<?php
/**
 * This file will overwrite the TYPO3 settings in the LocalConfiguration.php !!
 * In difference to LocalConfiguration.php this file is not touched by TYPO3.
 * System: TYPO3 8.7 LTS
 */

use TYPO3\CMS\Core\Core\Environment;

return [
    'SYS' => [
        'displayErrors' => 1,
        'systemLogLevel' => 0,
        'sitename' => getenv('WEBSITE_TITLE') . ' ' . Environment::getContext(),
        'errorHandlerErrors' => 4469,
        'belogErrorReporting' => 4469,
        'exceptionalErrors' => 12290,
        'trustedHostsPattern' => getenv('TRUSTED_HOST_PATTERN'),
        'UTF8filesystem' => true,
        'systemLocale' => 'de_DE.UTF-8',
        'ddmmyy' => 'Y-m-d',
        'hhmm' => 'H:i',
        'phpTimeZone' => 'UTC'
    ],
    'FE' => [
        'debug' => false,
        'pageNotFoundOnCHashError' => '0',
        'compressionLevel' => 9
    ],
    'BE' => [
        'debug' => true,
        'lockSSL' => true,
        'compressionLevel' => 9
    ],
];