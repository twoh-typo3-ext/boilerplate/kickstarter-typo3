<?php

return [
    'DB' => [
        'Connections' => [
            'Default' => [
                'driver' => 'mysqli',
                'host' => getenv('MYSQL_HOST'),
                'dbname' => getenv('MYSQL_DATABASE'),
                'user' => getenv('MYSQL_USER'),
                'password' => getenv('MYSQL_PASSWORD'),
                'charset' => 'utf8mb4',
                'port' => 3306,
                'tableoptions' => [
                    'charset' => 'utf8mb4',
                    'collate' => 'utf8mb4_unicode_ci',
                ],
            ],
        ],
    ],
    'EXTENSIONS' => [
        'backend' => [
            'loginBackgroundImage' => '',
            'loginFootnote' => '',
            'loginHighlightColor' => getenv('BACKEND_LOGIN_HIGHLIGHTING_COLOR'),
            'loginLogo' => getenv('LOGO'),
            'loginLogoAlt' => '',
        ],
    ],
    'MAIL' => [
        'defaultMailFromAddress' => getenv('DEFAULT_MAIL_ADRESS'),
        'defaultMailFromName' => getenv('DEFAULT_MAIL_FROM'),
        'transport' => getenv('TRANSPORT'),
        'transport_sendmail_command' => getenv('SENDMAIL_COMMAND'),
        'transport_smtp_encrypt' => getenv('SMTP_ENCRYPT'),
        'transport_smtp_password' => getenv('SMTP_PASSWORD'),
        'transport_smtp_server' => getenv('SMTP_SERVER'),
        'transport_smtp_username' => getenv('SMTP_USER'),
    ],
];
