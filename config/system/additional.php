<?php

use TWOH\TwohBase\Configuration\Loader;
use TYPO3\CMS\Core\Core\Environment;

$additionalConfiguration = new Loader(
    Environment::getContext(),
    dirname(__DIR__, 2)
);

$additionalConfiguration->load();