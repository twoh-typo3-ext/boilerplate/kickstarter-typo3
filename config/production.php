<?php
/**
 * This file will overwrite the TYPO3 settings in the LocalConfiguration.php !!
 * In difference to LocalConfiguration.php this file is not touched by TYPO3.
 * System: TYPO3 8.7 LTS
 */
use TYPO3\CMS\Core\Core\Environment;

/**
 * configuration
 */
return [
    'SYS' => [
        'displayErrors' => 0,
        'systemLogLevel' => 3,
        'sitename' => getenv('WEBSITE_TITLE') . ' ' . Environment::getContext(),
        'systemLog' => '',
        'enable_DLOG' => 1,
        'enable_errorDLOG' => 1,
        'enable_exceptionDLOG' => 1,
        'errorHandlerErrors' => 4469,
        'syslogErrorReporting' => 4469,
        'belogErrorReporting' => 4469,
        'exceptionalErrors' => 0,
        'enableDeprecationLog' => false,
        'sqlDebug' => 0,
        'trustedHostsPattern' => '*\.silbitz-group\.com',
        'UTF8filesystem' => true,
        'systemLocale' => 'de_DE.UTF-8',
        'ddmmyy' => 'Y-m-d',
        'hhmm' => 'H:i',
        'phpTimeZone' => 'UTC',
    ],
    'FE' => [
        'debug' => false,
        'pageNotFoundOnCHashError' => '0',
        'cHashIncludePageId' => true,
        'compressionLevel' => 9
    ],
    'BE' => [
        'debug' => false,
        'lockSSL' => true,
        'compressionLevel' => 9,
        'requireMfa' => 1
    ],
];