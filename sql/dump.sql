-- MySQL dump 10.13  Distrib 8.0.34, for Linux (x86_64)
--
-- Host: localhost    Database: typo3_twoh
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `backend_layout`
--

DROP TABLE IF EXISTS `backend_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `backend_layout` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `t3_origuid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `config` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backend_layout`
--

LOCK TABLES `backend_layout` WRITE;
/*!40000 ALTER TABLE `backend_layout` DISABLE KEYS */;
/*!40000 ALTER TABLE `backend_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `be_dashboards`
--

DROP TABLE IF EXISTS `be_dashboards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `be_dashboards` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `identifier` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cruser_id` int unsigned NOT NULL DEFAULT '0',
  `title` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `widgets` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `identifier` (`identifier`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_dashboards`
--

LOCK TABLES `be_dashboards` WRITE;
/*!40000 ALTER TABLE `be_dashboards` DISABLE KEYS */;
INSERT INTO `be_dashboards` VALUES (1,0,1710605684,1710605684,0,0,0,0,'a18eba2bfff8a44defc128d5db5d24efac18911c',1,'My dashboard','{\"0faa3928b491f702804855da7eb849513273e233\":{\"identifier\":\"t3information\"},\"6503748fe777ee7c0720867b79d297241b6af58d\":{\"identifier\":\"docGettingStarted\"}}');
/*!40000 ALTER TABLE `be_dashboards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `be_groups`
--

DROP TABLE IF EXISTS `be_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `be_groups` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `non_exclude_fields` text COLLATE utf8mb4_unicode_ci,
  `explicit_allowdeny` text COLLATE utf8mb4_unicode_ci,
  `allowed_languages` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `custom_options` text COLLATE utf8mb4_unicode_ci,
  `db_mountpoints` text COLLATE utf8mb4_unicode_ci,
  `pagetypes_select` text COLLATE utf8mb4_unicode_ci,
  `tables_select` text COLLATE utf8mb4_unicode_ci,
  `tables_modify` text COLLATE utf8mb4_unicode_ci,
  `groupMods` text COLLATE utf8mb4_unicode_ci,
  `availableWidgets` text COLLATE utf8mb4_unicode_ci,
  `mfa_providers` text COLLATE utf8mb4_unicode_ci,
  `file_mountpoints` text COLLATE utf8mb4_unicode_ci,
  `file_permissions` text COLLATE utf8mb4_unicode_ci,
  `TSconfig` text COLLATE utf8mb4_unicode_ci,
  `subgroup` text COLLATE utf8mb4_unicode_ci,
  `workspace_perms` smallint NOT NULL DEFAULT '1',
  `category_perms` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_groups`
--

LOCK TABLES `be_groups` WRITE;
/*!40000 ALTER TABLE `be_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `be_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `be_sessions`
--

DROP TABLE IF EXISTS `be_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `be_sessions` (
  `ses_id` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ses_iplock` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ses_userid` int unsigned NOT NULL DEFAULT '0',
  `ses_tstamp` int unsigned NOT NULL DEFAULT '0',
  `ses_data` longblob,
  PRIMARY KEY (`ses_id`),
  KEY `ses_tstamp` (`ses_tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_sessions`
--

LOCK TABLES `be_sessions` WRITE;
/*!40000 ALTER TABLE `be_sessions` DISABLE KEYS */;
INSERT INTO `be_sessions` VALUES ('6c52d7eb8624c1aa72c6935d1af883a472887ecfafdba968f8bbf6bfb0f695fb','[DISABLED]',1,1710608287,_binary 'a:1:{s:26:\"formProtectionSessionToken\";s:64:\"b3b9d408e03475720880b19f1d88aa2bbf34448391b4135b002d9e9e5345292a\";}');
/*!40000 ALTER TABLE `be_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `be_users`
--

DROP TABLE IF EXISTS `be_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `be_users` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `disable` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `avatar` int unsigned NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `admin` smallint unsigned NOT NULL DEFAULT '0',
  `usergroup` text COLLATE utf8mb4_unicode_ci,
  `lang` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `db_mountpoints` text COLLATE utf8mb4_unicode_ci,
  `options` smallint unsigned NOT NULL DEFAULT '0',
  `realName` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `userMods` text COLLATE utf8mb4_unicode_ci,
  `allowed_languages` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `uc` mediumblob,
  `file_mountpoints` text COLLATE utf8mb4_unicode_ci,
  `file_permissions` text COLLATE utf8mb4_unicode_ci,
  `workspace_perms` smallint NOT NULL DEFAULT '0',
  `TSconfig` text COLLATE utf8mb4_unicode_ci,
  `workspace_id` int NOT NULL DEFAULT '0',
  `mfa` mediumblob,
  `category_perms` longtext COLLATE utf8mb4_unicode_ci,
  `lastlogin` int NOT NULL DEFAULT '0',
  `password_reset_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `username` (`username`),
  KEY `parent` (`pid`,`deleted`,`disable`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `be_users`
--

LOCK TABLES `be_users` WRITE;
/*!40000 ALTER TABLE `be_users` DISABLE KEYS */;
INSERT INTO `be_users` VALUES (1,0,1710605599,1710605599,0,0,0,0,NULL,'arl',0,'$argon2i$v=19$m=65536,t=16,p=1$Y0U3WmJZUDJJVUgwV1FvNA$O6IbiF7VP0FkY87/bSbB/71BLGhb4Tm01MAknGTS47c',1,NULL,'default','a.reichel91@outlook.com',NULL,0,'Andreas',NULL,'',_binary 'a:7:{s:10:\"moduleData\";a:8:{s:28:\"dashboard/current_dashboard/\";s:40:\"a18eba2bfff8a44defc128d5db5d24efac18911c\";s:6:\"web_ts\";a:1:{s:6:\"action\";s:25:\"web_typoscript_infomodify\";}s:57:\"TYPO3\\CMS\\Backend\\Utility\\BackendUtility::getUpdateSignal\";a:0:{}s:25:\"web_typoscript_infomodify\";a:1:{s:23:\"selectedTemplatePerPage\";a:1:{i:1;i:1;}}s:10:\"FormEngine\";a:2:{i:0;a:4:{s:32:\"86205c5935270b8ee413592ec1b62292\";a:5:{i:0;s:8:\"NEW SITE\";i:1;a:5:{s:4:\"edit\";a:1:{s:12:\"sys_template\";a:1:{i:1;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;}i:2;s:35:\"&edit%5Bsys_template%5D%5B1%5D=edit\";i:3;a:5:{s:5:\"table\";s:12:\"sys_template\";s:3:\"uid\";i:1;s:3:\"pid\";i:1;s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}i:4;s:89:\"/typo3/module/web/typoscript/overview?token=6b8f55f3f5d3308678b968f1c72f26043856fb3f&id=1\";}s:32:\"696addfecc296b326ff6e9f04c7ff3e1\";a:5:{i:0;s:4:\"Home\";i:1;a:5:{s:4:\"edit\";a:1:{s:5:\"pages\";a:1:{i:1;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;}i:2;s:28:\"&edit%5Bpages%5D%5B1%5D=edit\";i:3;a:5:{s:5:\"table\";s:5:\"pages\";s:3:\"uid\";i:1;s:3:\"pid\";i:0;s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}i:4;s:77:\"/typo3/module/web/layout?token=7d7d5b5a60dfc71a919d90f8e4b0e04167ce840f&id=1&\";}s:32:\"c312013d83c1a6ad7fec8b36a37ba3c8\";a:5:{i:0;s:34:\"Willkommen bei TYPO3 Kickstarter! \";i:1;a:5:{s:4:\"edit\";a:1:{s:10:\"tt_content\";a:1:{i:1;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;}i:2;s:33:\"&edit%5Btt_content%5D%5B1%5D=edit\";i:3;a:5:{s:5:\"table\";s:10:\"tt_content\";s:3:\"uid\";i:1;s:3:\"pid\";i:1;s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}i:4;s:77:\"/typo3/module/web/layout?token=7d7d5b5a60dfc71a919d90f8e4b0e04167ce840f&id=1&\";}s:32:\"494c59ed0b451cdb0042831766e2d4b1\";a:5:{i:0;s:0:\"\";i:1;a:5:{s:4:\"edit\";a:1:{s:10:\"tt_content\";a:1:{i:5;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;}i:2;s:33:\"&edit%5Btt_content%5D%5B5%5D=edit\";i:3;a:5:{s:5:\"table\";s:10:\"tt_content\";s:3:\"uid\";i:5;s:3:\"pid\";i:1;s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}i:4;s:97:\"/typo3/module/web/layout?token=7d7d5b5a60dfc71a919d90f8e4b0e04167ce840f&id=1#element-tt_content-5\";}}i:1;s:32:\"494c59ed0b451cdb0042831766e2d4b1\";}s:10:\"web_layout\";a:3:{s:8:\"function\";N;s:8:\"language\";N;s:19:\"constant_editor_cat\";N;}s:16:\"opendocs::recent\";a:2:{s:32:\"581106f297d9eed8dec1190ee4d6b04d\";a:5:{i:0;s:27:\"TYPO3 Kickstarter Anwendung\";i:1;a:5:{s:4:\"edit\";a:1:{s:10:\"tt_content\";a:1:{i:3;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;}i:2;s:33:\"&edit%5Btt_content%5D%5B3%5D=edit\";i:3;a:5:{s:5:\"table\";s:10:\"tt_content\";s:3:\"uid\";i:3;s:3:\"pid\";i:1;s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}i:4;s:97:\"/typo3/module/web/layout?token=7d7d5b5a60dfc71a919d90f8e4b0e04167ce840f&id=1#element-tt_content-3\";}s:32:\"deac478137dd48a97e299bd046412e21\";a:5:{i:0;s:0:\"\";i:1;a:5:{s:4:\"edit\";a:1:{s:10:\"tt_content\";a:1:{i:2;s:4:\"edit\";}}s:7:\"defVals\";N;s:12:\"overrideVals\";N;s:11:\"columnsOnly\";N;s:6:\"noView\";N;}i:2;s:33:\"&edit%5Btt_content%5D%5B2%5D=edit\";i:3;a:5:{s:5:\"table\";s:10:\"tt_content\";s:3:\"uid\";i:2;s:3:\"pid\";i:1;s:3:\"cmd\";s:4:\"edit\";s:12:\"deleteAccess\";b:1;}i:4;s:97:\"/typo3/module/web/layout?token=7d7d5b5a60dfc71a919d90f8e4b0e04167ce840f&id=1#element-tt_content-2\";}}s:16:\"browse_links.php\";a:1:{s:12:\"expandFolder\";s:20:\"1:/form_definitions/\";}}s:14:\"emailMeAtLogin\";i:0;s:8:\"titleLen\";i:50;s:20:\"edit_docModuleUpload\";s:1:\"1\";s:15:\"moduleSessionID\";a:8:{s:28:\"dashboard/current_dashboard/\";s:40:\"59fc9331ad54ab4f9b0dbfd75500fd2e245e04a0\";s:6:\"web_ts\";s:40:\"59fc9331ad54ab4f9b0dbfd75500fd2e245e04a0\";s:57:\"TYPO3\\CMS\\Backend\\Utility\\BackendUtility::getUpdateSignal\";s:40:\"59fc9331ad54ab4f9b0dbfd75500fd2e245e04a0\";s:25:\"web_typoscript_infomodify\";s:40:\"59fc9331ad54ab4f9b0dbfd75500fd2e245e04a0\";s:10:\"FormEngine\";s:40:\"59fc9331ad54ab4f9b0dbfd75500fd2e245e04a0\";s:10:\"web_layout\";s:40:\"59fc9331ad54ab4f9b0dbfd75500fd2e245e04a0\";s:16:\"opendocs::recent\";s:40:\"59fc9331ad54ab4f9b0dbfd75500fd2e245e04a0\";s:16:\"browse_links.php\";s:40:\"59fc9331ad54ab4f9b0dbfd75500fd2e245e04a0\";}s:17:\"BackendComponents\";a:1:{s:6:\"States\";a:2:{s:8:\"Pagetree\";a:1:{s:9:\"stateHash\";a:2:{s:3:\"0_0\";s:1:\"1\";s:3:\"0_1\";s:1:\"1\";}}s:15:\"FileStorageTree\";a:1:{s:9:\"stateHash\";a:2:{s:10:\"1_59663721\";s:1:\"1\";s:11:\"1_130865399\";s:1:\"1\";}}}}s:10:\"inlineView\";s:47:\"{\"tt_content\":{\"3\":{\"sys_file_reference\":[1]}}}\";}',NULL,NULL,0,NULL,0,NULL,NULL,1710605682,'');
/*!40000 ALTER TABLE `be_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_adminpanel_requestcache`
--

DROP TABLE IF EXISTS `cache_adminpanel_requestcache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_adminpanel_requestcache` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `expires` int unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_adminpanel_requestcache`
--

LOCK TABLES `cache_adminpanel_requestcache` WRITE;
/*!40000 ALTER TABLE `cache_adminpanel_requestcache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_adminpanel_requestcache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_adminpanel_requestcache_tags`
--

DROP TABLE IF EXISTS `cache_adminpanel_requestcache_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_adminpanel_requestcache_tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_adminpanel_requestcache_tags`
--

LOCK TABLES `cache_adminpanel_requestcache_tags` WRITE;
/*!40000 ALTER TABLE `cache_adminpanel_requestcache_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_adminpanel_requestcache_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_news_category`
--

DROP TABLE IF EXISTS `cache_news_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_news_category` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `expires` int unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_news_category`
--

LOCK TABLES `cache_news_category` WRITE;
/*!40000 ALTER TABLE `cache_news_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_news_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_news_category_tags`
--

DROP TABLE IF EXISTS `cache_news_category_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_news_category_tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_news_category_tags`
--

LOCK TABLES `cache_news_category_tags` WRITE;
/*!40000 ALTER TABLE `cache_news_category_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_news_category_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_staticfilecache`
--

DROP TABLE IF EXISTS `cache_staticfilecache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_staticfilecache` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `expires` int unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_staticfilecache`
--

LOCK TABLES `cache_staticfilecache` WRITE;
/*!40000 ALTER TABLE `cache_staticfilecache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_staticfilecache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_staticfilecache_tags`
--

DROP TABLE IF EXISTS `cache_staticfilecache_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_staticfilecache_tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_staticfilecache_tags`
--

LOCK TABLES `cache_staticfilecache_tags` WRITE;
/*!40000 ALTER TABLE `cache_staticfilecache_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_staticfilecache_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_treelist`
--

DROP TABLE IF EXISTS `cache_treelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_treelist` (
  `md5hash` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `pid` int NOT NULL DEFAULT '0',
  `treelist` mediumtext COLLATE utf8mb4_unicode_ci,
  `tstamp` int NOT NULL DEFAULT '0',
  `expires` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`md5hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_treelist`
--

LOCK TABLES `cache_treelist` WRITE;
/*!40000 ALTER TABLE `cache_treelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_treelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_tx_solr`
--

DROP TABLE IF EXISTS `cache_tx_solr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_tx_solr` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `expires` int unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_tx_solr`
--

LOCK TABLES `cache_tx_solr` WRITE;
/*!40000 ALTER TABLE `cache_tx_solr` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_tx_solr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_tx_solr_configuration`
--

DROP TABLE IF EXISTS `cache_tx_solr_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_tx_solr_configuration` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `expires` int unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_tx_solr_configuration`
--

LOCK TABLES `cache_tx_solr_configuration` WRITE;
/*!40000 ALTER TABLE `cache_tx_solr_configuration` DISABLE KEYS */;
INSERT INTO `cache_tx_solr_configuration` VALUES (1,'bd1cc8da0ba33d084f0e3f63e065cf0a',1710694221,_binary 'a:15:{s:7:\"config.\";a:11:{s:19:\"pageTitleProviders.\";a:4:{s:7:\"record.\";a:1:{s:8:\"provider\";s:48:\"TYPO3\\CMS\\Core\\PageTitle\\RecordPageTitleProvider\";}s:4:\"seo.\";a:2:{s:8:\"provider\";s:49:\"TYPO3\\CMS\\Seo\\PageTitle\\SeoTitlePageTitleProvider\";s:6:\"before\";s:6:\"record\";}s:5:\"news.\";a:2:{s:8:\"provider\";s:38:\"GeorgRinger\\News\\Seo\\NewsTitleProvider\";s:6:\"before\";s:23:\"altPageTitle,record,seo\";}s:12:\"yoastRecord.\";a:2:{s:8:\"provider\";s:59:\"YoastSeoForTypo3\\YoastSeo\\PageTitle\\RecordPageTitleProvider\";s:6:\"before\";s:10:\"record,seo\";}}s:10:\"yoast_seo.\";a:1:{s:7:\"enabled\";s:1:\"1\";}s:15:\"structuredData.\";a:1:{s:10:\"providers.\";a:2:{s:11:\"breadcrumb.\";a:3:{s:8:\"provider\";s:73:\"YoastSeoForTypo3\\YoastSeo\\StructuredData\\BreadcrumbStructuredDataProvider\";s:5:\"after\";s:4:\"site\";s:16:\"excludedDoktypes\";s:0:\"\";}s:5:\"site.\";a:1:{s:8:\"provider\";s:67:\"YoastSeoForTypo3\\YoastSeo\\StructuredData\\SiteStructuredDataProvider\";}}}s:10:\"sourceopt.\";a:8:{s:7:\"enabled\";s:1:\"1\";s:13:\"headerComment\";s:0:\"\";s:10:\"formatHtml\";s:1:\"2\";s:11:\"formatHtml.\";a:2:{s:7:\"tabSize\";s:0:\"\";s:12:\"debugComment\";s:1:\"0\";}s:18:\"dropEmptySpaceChar\";s:1:\"0\";s:15:\"removeGenerator\";s:1:\"1\";s:14:\"removeComments\";s:1:\"1\";s:15:\"removeComments.\";a:1:{s:5:\"keep.\";a:11:{i:87;s:37:\"/This website is powered by TYPO3/usi\";i:88;s:10:\"/^\\[if/usi\";i:89;s:15:\"/^<!\\[endif/usi\";i:96;s:18:\"/^INT_SCRIPT\\./usi\";i:97;s:10:\"/^TDS_/usi\";i:98;s:9:\"/^HD_/usi\";i:99;s:16:\"/INTsubpart_/usi\";i:101;s:16:\"/^googleoff_/usi\";i:102;s:15:\"/^googleon_/usi\";i:104;s:14:\"/^gps(e|s)/usi\";i:10;s:18:\"/^TYPO3SEARCH_/usi\";}}}s:9:\"svgstore.\";a:2:{s:7:\"enabled\";s:1:\"1\";s:8:\"fileSize\";s:5:\"50000\";}s:11:\"tx_extbase.\";a:3:{s:12:\"persistence.\";a:3:{s:8:\"classes.\";a:4:{s:35:\"GeorgRinger\\News\\Domain\\Model\\News.\";a:1:{s:11:\"subclasses.\";a:3:{i:0;s:41:\"GeorgRinger\\News\\Domain\\Model\\NewsDefault\";i:1;s:42:\"GeorgRinger\\News\\Domain\\Model\\NewsInternal\";i:2;s:42:\"GeorgRinger\\News\\Domain\\Model\\NewsExternal\";}}s:42:\"GeorgRinger\\News\\Domain\\Model\\NewsDefault.\";a:1:{s:8:\"mapping.\";a:2:{s:10:\"recordType\";s:1:\"0\";s:9:\"tableName\";s:25:\"tx_news_domain_model_news\";}}s:43:\"GeorgRinger\\News\\Domain\\Model\\NewsInternal.\";a:1:{s:8:\"mapping.\";a:2:{s:10:\"recordType\";s:1:\"1\";s:9:\"tableName\";s:25:\"tx_news_domain_model_news\";}}s:43:\"GeorgRinger\\News\\Domain\\Model\\NewsExternal.\";a:1:{s:8:\"mapping.\";a:2:{s:10:\"recordType\";s:1:\"2\";s:9:\"tableName\";s:25:\"tx_news_domain_model_news\";}}}s:28:\"enableAutomaticCacheClearing\";s:1:\"1\";s:20:\"updateReferenceIndex\";s:1:\"0\";}s:4:\"mvc.\";a:1:{s:48:\"throwPageNotFoundExceptionIfActionCantBeResolved\";s:1:\"0\";}s:9:\"features.\";a:3:{s:20:\"skipDefaultArguments\";s:1:\"0\";s:25:\"ignoreAllEnableFieldsInBe\";s:1:\"0\";s:35:\"enableNamespacedArgumentsForBackend\";s:1:\"0\";}}s:24:\"moveJsFromHeaderToFooter\";s:1:\"1\";s:14:\"concatenateCss\";s:1:\"1\";s:13:\"concatenateJs\";s:1:\"1\";s:12:\"index_enable\";s:1:\"1\";s:12:\"recordLinks.\";a:1:{s:8:\"tx_news.\";a:2:{s:9:\"typolink.\";a:8:{s:9:\"parameter\";s:3:\"272\";s:17:\"additionalParams.\";a:2:{s:4:\"data\";s:9:\"field:uid\";s:4:\"wrap\";s:76:\"&tx_news_pi1[controller]=News&tx_news_pi1[action]=detail&tx_news_pi1[news]=|\";}s:12:\"useCacheHash\";s:1:\"1\";s:11:\"ATagParams.\";a:1:{s:4:\"data\";s:20:\"parameters:allParams\";}s:7:\"target.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}s:6:\"title.\";a:1:{s:4:\"data\";s:16:\"parameters:title\";}s:9:\"extTarget\";s:6:\"_blank\";s:10:\"extTarget.\";a:1:{s:9:\"override.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}}}s:9:\"forceLink\";s:1:\"1\";}}}s:7:\"styles.\";a:1:{s:8:\"content.\";a:2:{s:3:\"get\";s:7:\"CONTENT\";s:4:\"get.\";a:2:{s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:2:{s:7:\"orderBy\";s:7:\"sorting\";s:5:\"where\";s:11:\"{#colPos}=0\";}}}}s:10:\"tt_content\";s:4:\"CASE\";s:11:\"tt_content.\";a:145:{s:4:\"key.\";a:1:{s:5:\"field\";s:5:\"CType\";}s:7:\"default\";s:4:\"TEXT\";s:8:\"default.\";a:4:{s:5:\"field\";s:5:\"CType\";s:16:\"htmlSpecialChars\";s:1:\"1\";s:4:\"wrap\";s:165:\"<p style=\"background-color: yellow; padding: 0.5em 1em;\"><strong>ERROR:</strong> Content Element with uid \"{field:uid}\" and type \"|\" has no rendering definition!</p>\";s:5:\"wrap.\";a:1:{s:10:\"insertData\";s:1:\"1\";}}s:19:\"twoh_galerieelement\";s:18:\"< lib.contentBlock\";s:20:\"twoh_galerieelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:77:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/galerie-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:86:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/galerie-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:85:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/galerie-element/Source/Layouts/\";}}s:19:\"twoh_networkelement\";s:18:\"< lib.contentBlock\";s:20:\"twoh_networkelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:77:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/network-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:86:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/network-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:85:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/network-element/Source/Layouts/\";}}s:17:\"twoh_videoelement\";s:18:\"< lib.contentBlock\";s:18:\"twoh_videoelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:75:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/video-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:84:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/video-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:83:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/video-element/Source/Layouts/\";}}s:19:\"twoh_contactelement\";s:18:\"< lib.contentBlock\";s:20:\"twoh_contactelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:77:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/contact-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:86:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/contact-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:85:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/contact-element/Source/Layouts/\";}}s:19:\"twoh_partnerelement\";s:18:\"< lib.contentBlock\";s:20:\"twoh_partnerelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:77:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/partner-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:86:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/partner-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:85:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/partner-element/Source/Layouts/\";}}s:23:\"twoh_sliderimageelement\";s:18:\"< lib.contentBlock\";s:24:\"twoh_sliderimageelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:81:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/sliderimage-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:90:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/sliderimage-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:89:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/sliderimage-element/Source/Layouts/\";}}s:18:\"twoh_personelement\";s:18:\"< lib.contentBlock\";s:19:\"twoh_personelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:76:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/person-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:85:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/person-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:84:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/person-element/Source/Layouts/\";}}s:17:\"twoh_quoteelement\";s:18:\"< lib.contentBlock\";s:18:\"twoh_quoteelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:75:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/quote-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:84:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/quote-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:83:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/quote-element/Source/Layouts/\";}}s:19:\"twoh_sidebarelement\";s:18:\"< lib.contentBlock\";s:20:\"twoh_sidebarelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:77:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/sidebar-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:86:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/sidebar-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:85:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/sidebar-element/Source/Layouts/\";}}s:17:\"twoh_imageelement\";s:18:\"< lib.contentBlock\";s:18:\"twoh_imageelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:75:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/image-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:84:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/image-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:83:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/image-element/Source/Layouts/\";}}s:20:\"twoh_iconcardelement\";s:18:\"< lib.contentBlock\";s:21:\"twoh_iconcardelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:83:\"EXT:twoh_template_override_x/ContentBlocks/ContentElements/iconcard-element/Source/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:92:\"EXT:twoh_template_override_x/ContentBlocks/ContentElements/iconcard-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:91:\"EXT:twoh_template_override_x/ContentBlocks/ContentElements/iconcard-element/Source/Layouts/\";}}s:7:\"bullets\";s:20:\"< lib.contentElement\";s:8:\"bullets.\";a:2:{s:12:\"templateName\";s:7:\"Bullets\";s:15:\"dataProcessing.\";a:4:{i:10;s:5:\"split\";s:3:\"10.\";a:4:{s:3:\"if.\";a:2:{s:5:\"value\";s:1:\"2\";s:11:\"isLessThan.\";a:1:{s:5:\"field\";s:12:\"bullets_type\";}}s:9:\"fieldName\";s:8:\"bodytext\";s:18:\"removeEmptyEntries\";s:1:\"1\";s:2:\"as\";s:7:\"bullets\";}i:20;s:21:\"comma-separated-value\";s:3:\"20.\";a:4:{s:9:\"fieldName\";s:8:\"bodytext\";s:3:\"if.\";a:2:{s:5:\"value\";s:1:\"2\";s:7:\"equals.\";a:1:{s:5:\"field\";s:12:\"bullets_type\";}}s:14:\"fieldDelimiter\";s:1:\"|\";s:2:\"as\";s:7:\"bullets\";}}}s:3:\"div\";s:20:\"< lib.contentElement\";s:4:\"div.\";a:1:{s:12:\"templateName\";s:3:\"Div\";}s:6:\"header\";s:20:\"< lib.contentElement\";s:7:\"header.\";a:1:{s:12:\"templateName\";s:6:\"Header\";}s:4:\"html\";s:20:\"< lib.contentElement\";s:5:\"html.\";a:1:{s:12:\"templateName\";s:4:\"Html\";}s:5:\"image\";s:20:\"< lib.contentElement\";s:6:\"image.\";a:2:{s:12:\"templateName\";s:5:\"Image\";s:15:\"dataProcessing.\";a:4:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"image\";}}i:20;s:7:\"gallery\";s:3:\"20.\";a:5:{s:15:\"maxGalleryWidth\";s:3:\"600\";s:21:\"maxGalleryWidthInText\";s:3:\"300\";s:13:\"columnSpacing\";s:2:\"10\";s:11:\"borderWidth\";s:1:\"2\";s:13:\"borderPadding\";s:1:\"0\";}}}s:4:\"list\";s:20:\"< lib.contentElement\";s:5:\"list.\";a:2:{s:12:\"templateName\";s:4:\"List\";s:3:\"20.\";a:8:{s:15:\"solr_pi_results\";s:13:\"EXTBASEPLUGIN\";s:16:\"solr_pi_results.\";a:2:{s:13:\"extensionName\";s:4:\"Solr\";s:10:\"pluginName\";s:10:\"pi_results\";}s:14:\"solr_pi_search\";s:13:\"EXTBASEPLUGIN\";s:15:\"solr_pi_search.\";a:2:{s:13:\"extensionName\";s:4:\"Solr\";s:10:\"pluginName\";s:9:\"pi_search\";}s:26:\"solr_pi_frequentlysearched\";s:13:\"EXTBASEPLUGIN\";s:27:\"solr_pi_frequentlysearched.\";a:2:{s:13:\"extensionName\";s:4:\"Solr\";s:10:\"pluginName\";s:21:\"pi_frequentlySearched\";}s:15:\"solr_pi_suggest\";s:13:\"EXTBASEPLUGIN\";s:16:\"solr_pi_suggest.\";a:2:{s:13:\"extensionName\";s:4:\"Solr\";s:10:\"pluginName\";s:10:\"pi_suggest\";}}}s:8:\"shortcut\";s:20:\"< lib.contentElement\";s:9:\"shortcut.\";a:3:{s:12:\"templateName\";s:8:\"Shortcut\";s:10:\"variables.\";a:2:{s:9:\"shortcuts\";s:7:\"RECORDS\";s:10:\"shortcuts.\";a:3:{s:7:\"source.\";a:1:{s:5:\"field\";s:7:\"records\";}s:6:\"tables\";s:36:\"tt_content,tx_news_domain_model_news\";s:5:\"conf.\";a:2:{s:25:\"tx_news_domain_model_news\";s:4:\"USER\";s:26:\"tx_news_domain_model_news.\";a:6:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:10:\"NewsDetail\";s:10:\"vendorName\";s:11:\"GeorgRinger\";s:8:\"settings\";s:25:\"< plugin.tx_news.settings\";s:9:\"settings.\";a:4:{s:11:\"singleNews.\";a:1:{s:5:\"field\";s:3:\"uid\";}s:10:\"useStdWrap\";s:10:\"singleNews\";s:12:\"insertRecord\";s:1:\"1\";s:10:\"isShortcut\";s:1:\"1\";}}}}}s:3:\"20.\";a:2:{s:6:\"tables\";s:25:\"tx_news_domain_model_news\";s:5:\"conf.\";a:2:{s:25:\"tx_news_domain_model_news\";s:4:\"USER\";s:26:\"tx_news_domain_model_news.\";a:6:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:10:\"NewsDetail\";s:10:\"vendorName\";s:11:\"GeorgRinger\";s:8:\"settings\";s:25:\"< plugin.tx_news.settings\";s:9:\"settings.\";a:4:{s:11:\"singleNews.\";a:1:{s:5:\"field\";s:3:\"uid\";}s:10:\"useStdWrap\";s:10:\"singleNews\";s:12:\"insertRecord\";s:1:\"1\";s:10:\"isShortcut\";s:1:\"1\";}}}}}s:5:\"table\";s:20:\"< lib.contentElement\";s:6:\"table.\";a:2:{s:12:\"templateName\";s:5:\"Table\";s:15:\"dataProcessing.\";a:2:{i:10;s:21:\"comma-separated-value\";s:3:\"10.\";a:5:{s:9:\"fieldName\";s:8:\"bodytext\";s:15:\"fieldDelimiter.\";a:1:{s:5:\"char.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"field\";s:15:\"table_delimiter\";}}}s:15:\"fieldEnclosure.\";a:2:{s:5:\"char.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:1:{s:5:\"field\";s:15:\"table_enclosure\";}}s:3:\"if.\";a:3:{s:5:\"value\";s:1:\"0\";s:7:\"equals.\";a:1:{s:5:\"field\";s:15:\"table_enclosure\";}s:6:\"negate\";s:1:\"1\";}}s:15:\"maximumColumns.\";a:1:{s:5:\"field\";s:4:\"cols\";}s:2:\"as\";s:5:\"table\";}}}s:4:\"text\";s:20:\"< lib.contentElement\";s:5:\"text.\";a:1:{s:12:\"templateName\";s:4:\"Text\";}s:9:\"textmedia\";s:20:\"< lib.contentElement\";s:10:\"textmedia.\";a:2:{s:12:\"templateName\";s:9:\"Textmedia\";s:15:\"dataProcessing.\";a:4:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:6:\"assets\";}}i:20;s:7:\"gallery\";s:3:\"20.\";a:5:{s:15:\"maxGalleryWidth\";s:3:\"600\";s:21:\"maxGalleryWidthInText\";s:3:\"300\";s:13:\"columnSpacing\";s:2:\"10\";s:11:\"borderWidth\";s:1:\"2\";s:13:\"borderPadding\";s:1:\"0\";}}}s:7:\"textpic\";s:20:\"< lib.contentElement\";s:8:\"textpic.\";a:2:{s:12:\"templateName\";s:7:\"Textpic\";s:15:\"dataProcessing.\";a:4:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"image\";}}i:20;s:7:\"gallery\";s:3:\"20.\";a:5:{s:15:\"maxGalleryWidth\";s:3:\"600\";s:21:\"maxGalleryWidthInText\";s:3:\"300\";s:13:\"columnSpacing\";s:2:\"10\";s:11:\"borderWidth\";s:1:\"2\";s:13:\"borderPadding\";s:1:\"0\";}}}s:7:\"uploads\";s:20:\"< lib.contentElement\";s:8:\"uploads.\";a:2:{s:12:\"templateName\";s:7:\"Uploads\";s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:3:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}s:12:\"collections.\";a:1:{s:5:\"field\";s:16:\"file_collections\";}s:8:\"sorting.\";a:2:{s:5:\"field\";s:16:\"filelink_sorting\";s:10:\"direction.\";a:1:{s:5:\"field\";s:26:\"filelink_sorting_direction\";}}}}}s:13:\"menu_abstract\";s:20:\"< lib.contentElement\";s:14:\"menu_abstract.\";a:2:{s:12:\"templateName\";s:12:\"MenuAbstract\";s:15:\"dataProcessing.\";a:2:{i:10;s:4:\"menu\";s:3:\"10.\";a:3:{s:7:\"special\";s:9:\"directory\";s:8:\"special.\";a:1:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}}s:24:\"menu_categorized_content\";s:20:\"< lib.contentElement\";s:25:\"menu_categorized_content.\";a:2:{s:12:\"templateName\";s:22:\"MenuCategorizedContent\";s:15:\"dataProcessing.\";a:2:{i:10;s:14:\"database-query\";s:3:\"10.\";a:10:{s:5:\"table\";s:10:\"tt_content\";s:12:\"selectFields\";s:15:\"{#tt_content}.*\";s:7:\"groupBy\";s:3:\"uid\";s:10:\"pidInList.\";a:1:{s:4:\"data\";s:12:\"leveluid : 0\";}s:9:\"recursive\";s:2:\"99\";s:5:\"join.\";a:2:{s:4:\"data\";s:25:\"field:selected_categories\";s:4:\"wrap\";s:124:\"{#sys_category_record_mm} ON uid = {#sys_category_record_mm}.{#uid_foreign} AND {#sys_category_record_mm}.{#uid_local} IN(|)\";}s:6:\"where.\";a:2:{s:4:\"data\";s:20:\"field:category_field\";s:4:\"wrap\";s:47:\"{#tablenames}=\'tt_content\' and {#fieldname}=\'|\'\";}s:7:\"orderBy\";s:18:\"tt_content.sorting\";s:2:\"as\";s:7:\"content\";s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"image\";}}}}}}s:22:\"menu_categorized_pages\";s:20:\"< lib.contentElement\";s:23:\"menu_categorized_pages.\";a:2:{s:12:\"templateName\";s:20:\"MenuCategorizedPages\";s:15:\"dataProcessing.\";a:2:{i:10;s:4:\"menu\";s:3:\"10.\";a:3:{s:7:\"special\";s:10:\"categories\";s:8:\"special.\";a:4:{s:6:\"value.\";a:1:{s:5:\"field\";s:19:\"selected_categories\";}s:9:\"relation.\";a:1:{s:5:\"field\";s:14:\"category_field\";}s:7:\"sorting\";s:5:\"title\";s:5:\"order\";s:3:\"asc\";}s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}}s:10:\"menu_pages\";s:20:\"< lib.contentElement\";s:11:\"menu_pages.\";a:2:{s:12:\"templateName\";s:9:\"MenuPages\";s:15:\"dataProcessing.\";a:2:{i:10;s:4:\"menu\";s:3:\"10.\";a:3:{s:7:\"special\";s:4:\"list\";s:8:\"special.\";a:1:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}}s:13:\"menu_subpages\";s:20:\"< lib.contentElement\";s:14:\"menu_subpages.\";a:2:{s:12:\"templateName\";s:12:\"MenuSubpages\";s:15:\"dataProcessing.\";a:2:{i:10;s:4:\"menu\";s:3:\"10.\";a:3:{s:7:\"special\";s:9:\"directory\";s:8:\"special.\";a:1:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}}s:12:\"menu_section\";s:20:\"< lib.contentElement\";s:13:\"menu_section.\";a:2:{s:12:\"templateName\";s:11:\"MenuSection\";s:15:\"dataProcessing.\";a:2:{i:10;s:4:\"menu\";s:3:\"10.\";a:4:{s:17:\"includeNotInMenu.\";a:2:{s:8:\"override\";s:1:\"1\";s:9:\"override.\";a:1:{s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:5:\"field\";s:5:\"pages\";}}}}s:7:\"special\";s:4:\"list\";s:8:\"special.\";a:1:{s:6:\"value.\";a:2:{s:5:\"field\";s:5:\"pages\";s:9:\"override.\";a:3:{s:4:\"data\";s:8:\"page:uid\";s:3:\"if.\";a:1:{s:8:\"isFalse.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:9:\"override.\";a:2:{s:4:\"data\";s:21:\"page:content_from_pid\";s:3:\"if.\";a:1:{s:7:\"isTrue.\";a:1:{s:4:\"data\";s:21:\"page:content_from_pid\";}}}}}}s:15:\"dataProcessing.\";a:4:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}i:20;s:14:\"database-query\";s:3:\"20.\";a:6:{s:5:\"table\";s:10:\"tt_content\";s:10:\"pidInList.\";a:1:{s:5:\"field\";s:3:\"uid\";}s:2:\"as\";s:7:\"content\";s:5:\"where\";s:19:\"{#sectionIndex} = 1\";s:7:\"orderBy\";s:7:\"sorting\";s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"image\";}}}}}}}}s:18:\"menu_section_pages\";s:20:\"< lib.contentElement\";s:19:\"menu_section_pages.\";a:2:{s:12:\"templateName\";s:16:\"MenuSectionPages\";s:15:\"dataProcessing.\";a:2:{i:10;s:4:\"menu\";s:3:\"10.\";a:3:{s:7:\"special\";s:9:\"directory\";s:8:\"special.\";a:1:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:15:\"dataProcessing.\";a:4:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}i:20;s:14:\"database-query\";s:3:\"20.\";a:6:{s:5:\"table\";s:10:\"tt_content\";s:10:\"pidInList.\";a:1:{s:5:\"field\";s:3:\"uid\";}s:5:\"where\";s:19:\"{#sectionIndex} = 1\";s:7:\"orderBy\";s:7:\"sorting\";s:2:\"as\";s:7:\"content\";s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"image\";}}}}}}}}s:21:\"menu_recently_updated\";s:20:\"< lib.contentElement\";s:22:\"menu_recently_updated.\";a:2:{s:12:\"templateName\";s:19:\"MenuRecentlyUpdated\";s:15:\"dataProcessing.\";a:2:{i:10;s:4:\"menu\";s:3:\"10.\";a:3:{s:7:\"special\";s:7:\"updated\";s:8:\"special.\";a:3:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}s:6:\"maxAge\";s:9:\"3600*24*7\";s:20:\"excludeNoSearchPages\";s:1:\"1\";}s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}}s:18:\"menu_related_pages\";s:20:\"< lib.contentElement\";s:19:\"menu_related_pages.\";a:2:{s:12:\"templateName\";s:16:\"MenuRelatedPages\";s:15:\"dataProcessing.\";a:2:{i:10;s:4:\"menu\";s:3:\"10.\";a:4:{s:7:\"special\";s:8:\"keywords\";s:8:\"special.\";a:2:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}s:20:\"excludeNoSearchPages\";s:1:\"1\";}s:23:\"alternativeSortingField\";s:5:\"title\";s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}}s:12:\"menu_sitemap\";s:20:\"< lib.contentElement\";s:13:\"menu_sitemap.\";a:2:{s:12:\"templateName\";s:11:\"MenuSitemap\";s:15:\"dataProcessing.\";a:2:{i:10;s:4:\"menu\";s:3:\"10.\";a:2:{s:6:\"levels\";s:1:\"7\";s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}}s:18:\"menu_sitemap_pages\";s:20:\"< lib.contentElement\";s:19:\"menu_sitemap_pages.\";a:2:{s:12:\"templateName\";s:16:\"MenuSitemapPages\";s:15:\"dataProcessing.\";a:2:{i:10;s:4:\"menu\";s:3:\"10.\";a:4:{s:7:\"special\";s:9:\"directory\";s:8:\"special.\";a:1:{s:6:\"value.\";a:1:{s:5:\"field\";s:5:\"pages\";}}s:6:\"levels\";s:1:\"7\";s:15:\"dataProcessing.\";a:2:{i:10;s:5:\"files\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}}}s:18:\"form_formframework\";s:20:\"< lib.contentElement\";s:19:\"form_formframework.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:4:\"Form\";s:10:\"pluginName\";s:13:\"Formframework\";}}s:13:\"felogin_login\";s:20:\"< lib.contentElement\";s:14:\"felogin_login.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:7:\"Felogin\";s:10:\"pluginName\";s:5:\"Login\";}}s:8:\"news_pi1\";s:20:\"< lib.contentElement\";s:9:\"news_pi1.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:3:\"Pi1\";}}s:19:\"news_newsliststicky\";s:20:\"< lib.contentElement\";s:20:\"news_newsliststicky.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:14:\"NewsListSticky\";}}s:15:\"news_newsdetail\";s:20:\"< lib.contentElement\";s:16:\"news_newsdetail.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:10:\"NewsDetail\";}}s:21:\"news_newsselectedlist\";s:20:\"< lib.contentElement\";s:22:\"news_newsselectedlist.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:16:\"NewsSelectedList\";}}s:17:\"news_newsdatemenu\";s:20:\"< lib.contentElement\";s:18:\"news_newsdatemenu.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:12:\"NewsDateMenu\";}}s:19:\"news_newssearchform\";s:20:\"< lib.contentElement\";s:20:\"news_newssearchform.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:14:\"NewsSearchForm\";}}s:21:\"news_newssearchresult\";s:20:\"< lib.contentElement\";s:22:\"news_newssearchresult.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:16:\"NewsSearchResult\";}}s:17:\"news_categorylist\";s:20:\"< lib.contentElement\";s:18:\"news_categorylist.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:12:\"CategoryList\";}}s:12:\"news_taglist\";s:20:\"< lib.contentElement\";s:13:\"news_taglist.\";a:3:{s:12:\"templateName\";s:7:\"Generic\";i:20;s:13:\"EXTBASEPLUGIN\";s:3:\"20.\";a:2:{s:13:\"extensionName\";s:4:\"News\";s:10:\"pluginName\";s:7:\"TagList\";}}s:23:\"twoh_dataprocbreadcrumb\";s:20:\"< lib.contentElement\";s:24:\"twoh_dataprocbreadcrumb.\";a:4:{s:12:\"templateName\";s:18:\"DataProcBreadcrumb\";s:18:\"templateRootPaths.\";a:1:{i:0;s:64:\"EXT:twoh_kickstarter/Resources/Private/Templates/ContentElements\";}s:17:\"partialRootPaths.\";a:1:{i:0;s:60:\"EXT:twoh_kickstarter/Resources/Private/Partials/AtomElements\";}s:15:\"dataProcessing.\";a:2:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:5:{s:2:\"as\";s:10:\"breadcrumb\";s:16:\"includeNotInMenu\";s:1:\"1\";s:10:\"titleField\";s:9:\"nav_title\";s:7:\"special\";s:8:\"rootline\";s:8:\"special.\";a:1:{s:5:\"range\";s:4:\"0|-1\";}}}}s:8:\"threecol\";s:13:\"FLUIDTEMPLATE\";s:9:\"threecol.\";a:6:{s:12:\"templateName\";s:8:\"Threecol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:69:\"EXT:twoh_kickstarter/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:67:\"EXT:twoh_kickstarter/Resources/Private/Layouts/Extensions/Container\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:7:\"fourcol\";s:13:\"FLUIDTEMPLATE\";s:8:\"fourcol.\";a:6:{s:12:\"templateName\";s:7:\"Fourcol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:69:\"EXT:twoh_kickstarter/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:67:\"EXT:twoh_kickstarter/Resources/Private/Layouts/Extensions/Container\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:12:\"foureightcol\";s:13:\"FLUIDTEMPLATE\";s:13:\"foureightcol.\";a:6:{s:12:\"templateName\";s:12:\"Foureightcol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:69:\"EXT:twoh_kickstarter/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:67:\"EXT:twoh_kickstarter/Resources/Private/Layouts/Extensions/Container\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:12:\"eightfourcol\";s:13:\"FLUIDTEMPLATE\";s:13:\"eightfourcol.\";a:6:{s:12:\"templateName\";s:12:\"Eightfourcol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:69:\"EXT:twoh_kickstarter/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:67:\"EXT:twoh_kickstarter/Resources/Private/Layouts/Extensions/Container\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:12:\"ninethreecol\";s:13:\"FLUIDTEMPLATE\";s:13:\"ninethreecol.\";a:6:{s:12:\"templateName\";s:12:\"Ninethreecol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:69:\"EXT:twoh_kickstarter/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:67:\"EXT:twoh_kickstarter/Resources/Private/Layouts/Extensions/Container\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:12:\"threeninecol\";s:13:\"FLUIDTEMPLATE\";s:13:\"threeninecol.\";a:6:{s:12:\"templateName\";s:12:\"Threeninecol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:69:\"EXT:twoh_kickstarter/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:67:\"EXT:twoh_kickstarter/Resources/Private/Layouts/Extensions/Container\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:11:\"slickslider\";s:13:\"FLUIDTEMPLATE\";s:12:\"slickslider.\";a:6:{s:12:\"templateName\";s:11:\"Slickslider\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:69:\"EXT:twoh_kickstarter/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:67:\"EXT:twoh_kickstarter/Resources/Private/Layouts/Extensions/Container\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:9:\"accordion\";s:13:\"FLUIDTEMPLATE\";s:10:\"accordion.\";a:6:{s:12:\"templateName\";s:9:\"Accordion\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:69:\"EXT:twoh_kickstarter/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:67:\"EXT:twoh_kickstarter/Resources/Private/Layouts/Extensions/Container\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:4:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:102;s:62:\"TWOH\\TwohKickstarter\\DataProcessing\\AccordionFlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:13:\"partnerslider\";s:13:\"FLUIDTEMPLATE\";s:14:\"partnerslider.\";a:6:{s:12:\"templateName\";s:13:\"Partnerslider\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:69:\"EXT:twoh_kickstarter/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:67:\"EXT:twoh_kickstarter/Resources/Private/Layouts/Extensions/Container\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:18:\"twoh_buttonelement\";s:18:\"< lib.contentBlock\";s:19:\"twoh_buttonelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:119:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/ContentBlocks/ContentElements/button-element/Source\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:85:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/button-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:84:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/button-element/Source/Layouts/\";}}s:20:\"twoh_headlineelement\";s:18:\"< lib.contentBlock\";s:21:\"twoh_headlineelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:121:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/ContentBlocks/ContentElements/headline-element/Source\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:87:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/headline-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:86:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/headline-element/Source/Layouts/\";}}s:16:\"twoh_textelement\";s:18:\"< lib.contentBlock\";s:17:\"twoh_textelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:117:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/ContentBlocks/ContentElements/text-element/Source\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:83:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/text-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:82:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/text-element/Source/Layouts/\";}}s:19:\"twoh_counterelement\";s:18:\"< lib.contentBlock\";s:20:\"twoh_counterelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:120:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/ContentBlocks/ContentElements/counter-element/Source\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:86:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/counter-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:85:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/counter-element/Source/Layouts/\";}}s:17:\"twoh_topicelement\";s:18:\"< lib.contentBlock\";s:18:\"twoh_topicelement.\";a:4:{s:12:\"templateName\";s:8:\"Frontend\";s:18:\"templateRootPaths.\";a:1:{i:20;s:118:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/ContentBlocks/ContentElements/topic-element/Source\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:84:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/topic-element/Source/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:20;s:83:\"EXT:twoh_content_blocks/ContentBlocks/ContentElements/topic-element/Source/Layouts/\";}}s:17:\"twoh_dataprocmenu\";s:20:\"< lib.contentElement\";s:18:\"twoh_dataprocmenu.\";a:4:{s:12:\"templateName\";s:12:\"DataProcMenu\";s:18:\"templateRootPaths.\";a:1:{i:0;s:72:\"EXT:twoh_template_override_x/Resources/Private/Templates/ContentElements\";}s:17:\"partialRootPaths.\";a:1:{i:0;s:73:\"EXT:twoh_template_override_x/Resources/Private/Partials/Typo3CoreElements\";}s:15:\"dataProcessing.\";a:3:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:8:{s:6:\"levels\";s:1:\"3\";s:2:\"as\";s:10:\"headerMenu\";s:9:\"expandAll\";s:1:\"1\";s:13:\"includeSpacer\";s:1:\"1\";s:10:\"titleField\";s:9:\"nav_title\";s:7:\"special\";s:9:\"directory\";s:8:\"special.\";a:1:{s:5:\"value\";s:1:\"1\";}s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}i:20;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:21:\"twoh_dataproclangmenu\";s:20:\"< lib.contentElement\";s:22:\"twoh_dataproclangmenu.\";a:4:{s:12:\"templateName\";s:16:\"DataProcLangMenu\";s:18:\"templateRootPaths.\";a:1:{i:0;s:72:\"EXT:twoh_template_override_x/Resources/Private/Templates/ContentElements\";}s:17:\"partialRootPaths.\";a:1:{i:0;s:68:\"EXT:twoh_template_override_x/Resources/Private/Partials/AtomElements\";}s:15:\"dataProcessing.\";a:2:{i:10;s:55:\"TYPO3\\CMS\\Frontend\\DataProcessing\\LanguageMenuProcessor\";s:3:\"10.\";a:2:{s:9:\"languages\";s:4:\"auto\";s:2:\"as\";s:18:\"languageNavigation\";}}}s:23:\"twoh_dataprocfootermenu\";s:20:\"< lib.contentElement\";s:24:\"twoh_dataprocfootermenu.\";a:4:{s:12:\"templateName\";s:18:\"DataProcFooterMenu\";s:18:\"templateRootPaths.\";a:1:{i:0;s:72:\"EXT:twoh_template_override_x/Resources/Private/Templates/ContentElements\";}s:17:\"partialRootPaths.\";a:1:{i:0;s:73:\"EXT:twoh_template_override_x/Resources/Private/Partials/Typo3CoreElements\";}s:15:\"dataProcessing.\";a:3:{i:10;s:47:\"TYPO3\\CMS\\Frontend\\DataProcessing\\MenuProcessor\";s:3:\"10.\";a:8:{s:6:\"levels\";s:1:\"2\";s:2:\"as\";s:10:\"footerMenu\";s:9:\"expandAll\";s:1:\"1\";s:13:\"includeSpacer\";s:1:\"1\";s:10:\"titleField\";s:9:\"nav_title\";s:7:\"special\";s:9:\"directory\";s:8:\"special.\";a:1:{s:5:\"value\";s:1:\"8\";}s:15:\"dataProcessing.\";a:2:{i:10;s:48:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FilesProcessor\";s:3:\"10.\";a:1:{s:11:\"references.\";a:1:{s:9:\"fieldName\";s:5:\"media\";}}}}i:20;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:16:\"countercontainer\";s:13:\"FLUIDTEMPLATE\";s:17:\"countercontainer.\";a:6:{s:12:\"templateName\";s:16:\"CounterContainer\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:77:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:73:\"EXT:twoh_template_override_x/Resources/Private/Partials/Typo3CoreElements\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:75:\"EXT:twoh_template_override_x/Resources/Private/Layouts/Extensions/Container\";i:10;s:0:\"\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:10:\"homeslider\";s:13:\"FLUIDTEMPLATE\";s:11:\"homeslider.\";a:6:{s:12:\"templateName\";s:10:\"Homeslider\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:77:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:75:\"EXT:twoh_template_override_x/Resources/Private/Layouts/Extensions/Container\";i:10;s:0:\"\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:6:\"onecol\";s:13:\"FLUIDTEMPLATE\";s:7:\"onecol.\";a:6:{s:12:\"templateName\";s:6:\"Onecol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:77:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:75:\"EXT:twoh_template_override_x/Resources/Private/Layouts/Extensions/Container\";i:10;s:0:\"\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:12:\"oneelevencol\";s:13:\"FLUIDTEMPLATE\";s:13:\"oneelevencol.\";a:6:{s:12:\"templateName\";s:12:\"Oneelevencol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:77:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:75:\"EXT:twoh_template_override_x/Resources/Private/Layouts/Extensions/Container\";i:10;s:0:\"\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:9:\"twotencol\";s:13:\"FLUIDTEMPLATE\";s:10:\"twotencol.\";a:6:{s:12:\"templateName\";s:9:\"Twotencol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:77:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:75:\"EXT:twoh_template_override_x/Resources/Private/Layouts/Extensions/Container\";i:10;s:0:\"\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:6:\"twocol\";s:13:\"FLUIDTEMPLATE\";s:7:\"twocol.\";a:6:{s:12:\"templateName\";s:6:\"Twocol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:77:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:75:\"EXT:twoh_template_override_x/Resources/Private/Layouts/Extensions/Container\";i:10;s:0:\"\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:16:\"oneeightthreecol\";s:13:\"FLUIDTEMPLATE\";s:17:\"oneeightthreecol.\";a:6:{s:12:\"templateName\";s:16:\"Oneeightthreecol\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:77:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:75:\"EXT:twoh_template_override_x/Resources/Private/Layouts/Extensions/Container\";i:10;s:0:\"\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}s:14:\"topiccontainer\";s:13:\"FLUIDTEMPLATE\";s:15:\"topiccontainer.\";a:6:{s:12:\"templateName\";s:14:\"Topiccontainer\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:77:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/Container\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:75:\"EXT:twoh_template_override_x/Resources/Private/Layouts/Extensions/Container\";i:10;s:0:\"\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}s:15:\"dataProcessing.\";a:3:{i:100;s:47:\"B13\\Container\\DataProcessing\\ContainerProcessor\";i:101;s:51:\"TYPO3\\CMS\\Frontend\\DataProcessing\\FlexFormProcessor\";i:103;s:53:\"TWOH\\TwohKickstarter\\DataProcessing\\FlexFormProcessor\";}}}s:7:\"module.\";a:5:{s:8:\"tx_form.\";a:1:{s:9:\"settings.\";a:1:{s:19:\"yamlConfigurations.\";a:1:{i:10;s:42:\"EXT:form/Configuration/Yaml/FormSetup.yaml\";}}}s:12:\"tx_yoastseo.\";a:2:{s:5:\"view.\";a:1:{s:18:\"templateRootPaths.\";a:1:{i:10;s:42:\"EXT:yoast_seo/Resources/Private/Templates/\";}}s:9:\"settings.\";a:1:{s:12:\"itemsPerPage\";s:2:\"20\";}}s:13:\"tx_dashboard.\";a:1:{s:5:\"view.\";a:3:{s:18:\"templateRootPaths.\";a:1:{i:1589480298;s:42:\"EXT:yoast_seo/Resources/Private/Templates/\";}s:17:\"partialRootPaths.\";a:1:{i:1589480298;s:41:\"EXT:yoast_seo/Resources/Private/Partials/\";}s:16:\"layoutRootPaths.\";a:1:{i:1589480298;s:40:\"EXT:yoast_seo/Resources/Private/Layouts/\";}}}s:8:\"tx_news.\";a:3:{s:4:\"mvc.\";a:1:{s:39:\"callDefaultActionIfActionCantBeResolved\";s:1:\"1\";}s:5:\"view.\";a:3:{s:18:\"templateRootPaths.\";a:2:{i:0;s:37:\"EXT:news/Resources/Private/Templates/\";i:1;s:37:\"EXT:news/Resources/Private/Templates/\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:36:\"EXT:news/Resources/Private/Partials/\";i:1;s:36:\"EXT:news/Resources/Private/Partials/\";}s:16:\"layoutRootPaths.\";a:2:{i:0;s:35:\"EXT:news/Resources/Private/Layouts/\";i:1;s:35:\"EXT:news/Resources/Private/Layouts/\";}}s:9:\"settings.\";a:22:{s:7:\"cssFile\";s:44:\"EXT:news/Resources/Public/Css/news-basic.css\";s:21:\"displayDummyIfNoMedia\";s:1:\"1\";s:6:\"format\";s:4:\"html\";s:31:\"overrideFlexformSettingsIfEmpty\";s:199:\"cropMaxCharacters,dateField,timeRestriction,timeRestrictionHigh,archiveRestriction,orderBy,orderDirection,backPid,listPid,startingpoint,recursive,list.paginate.itemsPerPage,list.paginate.templatePath\";s:35:\"allowEmptyStringsForOverwriteDemand\";s:1:\"0\";s:20:\"includeSubCategories\";s:1:\"0\";s:10:\"analytics.\";a:1:{s:7:\"social.\";a:3:{s:12:\"facebookLike\";s:1:\"1\";s:13:\"facebookShare\";s:1:\"1\";s:7:\"twitter\";s:1:\"1\";}}s:22:\"detailPidDetermination\";s:29:\"flexform, categories, default\";s:16:\"defaultDetailPid\";s:1:\"0\";s:9:\"dateField\";s:8:\"datetime\";s:5:\"link.\";a:3:{s:23:\"typesOpeningInNewWindow\";s:1:\"2\";s:6:\"hrDate\";s:1:\"0\";s:7:\"hrDate.\";a:3:{s:3:\"day\";s:1:\"j\";s:5:\"month\";s:1:\"n\";s:4:\"year\";s:1:\"Y\";}}s:17:\"cropMaxCharacters\";s:3:\"150\";s:7:\"orderBy\";s:8:\"datetime\";s:14:\"orderDirection\";s:4:\"desc\";s:12:\"topNewsFirst\";s:1:\"0\";s:14:\"orderByAllowed\";s:78:\"sorting,author,uid,title,teaser,author,tstamp,crdate,datetime,categories.title\";s:14:\"facebookLocale\";s:5:\"en_US\";s:11:\"demandClass\";s:0:\"\";s:7:\"search.\";a:3:{s:6:\"fields\";s:21:\"teaser,title,bodytext\";s:15:\"splitSearchWord\";s:1:\"0\";s:9:\"paginate.\";a:5:{s:5:\"class\";s:49:\"GeorgRinger\\NumberedPagination\\NumberedPagination\";s:12:\"itemsPerPage\";s:2:\"10\";s:11:\"insertAbove\";s:1:\"1\";s:11:\"insertBelow\";s:1:\"1\";s:20:\"maximumNumberOfLinks\";s:1:\"3\";}}s:7:\"detail.\";a:9:{s:13:\"errorHandling\";s:88:\"showStandaloneTemplate,EXT:news/Resources/Private/Templates/News/DetailNotFound.html,404\";s:20:\"checkPidOfNewsRecord\";s:1:\"0\";s:18:\"registerProperties\";s:14:\"keywords,title\";s:12:\"showPrevNext\";s:1:\"0\";s:22:\"showSocialShareButtons\";s:1:\"1\";s:12:\"showMetaTags\";s:1:\"1\";s:6:\"media.\";a:2:{s:6:\"image.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:8:\"maxWidth\";s:3:\"282\";s:9:\"maxHeight\";s:0:\"\";s:9:\"lightbox.\";a:5:{s:7:\"enabled\";s:1:\"0\";s:5:\"class\";s:8:\"lightbox\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:3:\"rel\";s:20:\"lightbox[myImageSet]\";}}s:6:\"video.\";a:2:{s:5:\"width\";s:3:\"282\";s:6:\"height\";s:3:\"159\";}}s:9:\"pageTitle\";s:1:\"1\";s:10:\"pageTitle.\";a:2:{s:8:\"provider\";s:38:\"GeorgRinger\\News\\Seo\\NewsTitleProvider\";s:10:\"properties\";s:22:\"alternativeTitle,title\";}}s:5:\"list.\";a:3:{s:6:\"media.\";a:2:{s:6:\"image.\";a:3:{s:11:\"lazyLoading\";s:4:\"lazy\";s:8:\"maxWidth\";s:3:\"100\";s:9:\"maxHeight\";s:3:\"100\";}s:10:\"dummyImage\";s:56:\"EXT:news/Resources/Public/Images/dummy-preview-image.png\";}s:9:\"paginate.\";a:5:{s:5:\"class\";s:49:\"GeorgRinger\\NumberedPagination\\NumberedPagination\";s:12:\"itemsPerPage\";s:2:\"25\";s:11:\"insertAbove\";s:1:\"1\";s:11:\"insertBelow\";s:1:\"1\";s:20:\"maximumNumberOfLinks\";s:1:\"3\";}s:4:\"rss.\";a:1:{s:8:\"channel.\";a:8:{s:5:\"title\";s:11:\"Dummy Title\";s:11:\"description\";s:0:\"\";s:8:\"language\";s:5:\"en-gb\";s:9:\"copyright\";s:10:\"TYPO3 News\";s:9:\"generator\";s:14:\"TYPO3 EXT:news\";s:4:\"link\";s:18:\"http://example.com\";s:7:\"typeNum\";s:4:\"9818\";s:3:\"ttl\";s:0:\"\";}}}s:10:\"opengraph.\";a:5:{s:9:\"site_name\";s:0:\"\";s:4:\"type\";s:7:\"article\";s:6:\"admins\";s:0:\"\";s:6:\"locale\";s:0:\"\";s:8:\"twitter.\";a:3:{s:4:\"card\";s:7:\"summary\";s:4:\"site\";s:0:\"\";s:7:\"creator\";s:0:\"\";}}}}s:11:\"tx_aisuite.\";a:1:{s:5:\"view.\";a:1:{s:16:\"layoutRootPaths.\";a:1:{i:20;s:39:\"EXT:ai_suite/Resources/Private/Layouts/\";}}}}s:7:\"plugin.\";a:21:{s:17:\"tx_felogin_login.\";a:3:{s:5:\"view.\";a:3:{s:18:\"templateRootPaths.\";a:1:{i:10;s:68:\"EXT:twoh_kickstarter/Resources/Private/Templates/Extensions/FeLogin/\";}s:17:\"partialRootPaths.\";a:1:{i:10;s:67:\"EXT:twoh_kickstarter/Resources/Private/Partials/Extensions/FeLogin/\";}s:16:\"layoutRootPaths.\";a:1:{i:10;s:0:\"\";}}s:29:\"ignoreFlexFormSettingsIfEmpty\";s:183:\"showForgotPassword,showPermaLogin,showLogoutFormAfterLogin,pages,recursive,redirectMode,redirectFirstMethod,redirectPageLogin,redirectPageLoginError,redirectPageLogout,redirectDisable\";s:9:\"settings.\";a:21:{s:5:\"pages\";s:1:\"0\";s:9:\"recursive\";s:1:\"0\";s:10:\"dateFormat\";s:9:\"Y-m-d H:i\";s:18:\"showForgotPassword\";s:1:\"0\";s:22:\"showForgotPasswordLink\";s:1:\"0\";s:14:\"showPermaLogin\";s:1:\"0\";s:24:\"showLogoutFormAfterLogin\";s:1:\"0\";s:10:\"email_from\";s:0:\"\";s:14:\"email_fromName\";s:0:\"\";s:6:\"email.\";a:4:{s:12:\"templateName\";s:16:\"PasswordRecovery\";s:16:\"layoutRootPaths.\";a:1:{i:20;s:0:\"\";}s:18:\"templateRootPaths.\";a:1:{i:20;s:46:\"EXT:felogin/Resources/Private/Email/Templates/\";}s:17:\"partialRootPaths.\";a:1:{i:20;s:0:\"\";}}s:12:\"redirectMode\";s:0:\"\";s:19:\"redirectFirstMethod\";s:1:\"0\";s:17:\"redirectPageLogin\";s:1:\"0\";s:22:\"redirectPageLoginError\";s:1:\"0\";s:18:\"redirectPageLogout\";s:1:\"0\";s:15:\"redirectDisable\";s:1:\"0\";s:43:\"exposeNonexistentUserInForgotPasswordDialog\";s:1:\"0\";s:23:\"forgotLinkHashValidTime\";s:2:\"12\";s:7:\"domains\";s:0:\"\";s:19:\"passwordValidators.\";a:2:{i:10;s:56:\"TYPO3\\CMS\\Extbase\\Validation\\Validator\\NotEmptyValidator\";s:3:\"20.\";a:2:{s:9:\"className\";s:60:\"TYPO3\\CMS\\Extbase\\Validation\\Validator\\StringLengthValidator\";s:8:\"options.\";a:1:{s:7:\"minimum\";s:1:\"6\";}}}s:20:\"newPasswordMinLength\";s:1:\"6\";}}s:23:\"tx_news_newsliststicky.\";a:1:{s:5:\"view.\";a:1:{s:15:\"pluginNamespace\";s:11:\"tx_news_pi1\";}}s:19:\"tx_news_newsdetail.\";a:1:{s:5:\"view.\";a:1:{s:15:\"pluginNamespace\";s:11:\"tx_news_pi1\";}}s:25:\"tx_news_newsselectedlist.\";a:1:{s:5:\"view.\";a:1:{s:15:\"pluginNamespace\";s:11:\"tx_news_pi1\";}}s:21:\"tx_news_newsdatemenu.\";a:1:{s:5:\"view.\";a:1:{s:15:\"pluginNamespace\";s:11:\"tx_news_pi1\";}}s:21:\"tx_news_categorylist.\";a:1:{s:5:\"view.\";a:1:{s:15:\"pluginNamespace\";s:11:\"tx_news_pi1\";}}s:23:\"tx_news_newssearchform.\";a:1:{s:5:\"view.\";a:1:{s:15:\"pluginNamespace\";s:11:\"tx_news_pi1\";}}s:25:\"tx_news_newssearchresult.\";a:1:{s:5:\"view.\";a:1:{s:15:\"pluginNamespace\";s:11:\"tx_news_pi1\";}}s:16:\"tx_news_taglist.\";a:1:{s:5:\"view.\";a:1:{s:15:\"pluginNamespace\";s:11:\"tx_news_pi1\";}}s:7:\"tx_seo.\";a:2:{s:5:\"view.\";a:3:{s:18:\"templateRootPaths.\";a:2:{i:0;s:46:\"EXT:seo/Resources/Private/Templates/XmlSitemap\";i:10;s:36:\"EXT:seo/Resources/Private/Templates/\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:45:\"EXT:seo/Resources/Private/Partials/XmlSitemap\";i:10;s:35:\"EXT:seo/Resources/Private/Partials/\";}s:16:\"layoutRootPaths.\";a:2:{i:0;s:44:\"EXT:seo/Resources/Private/Layouts/XmlSitemap\";i:10;s:34:\"EXT:seo/Resources/Private/Layouts/\";}}s:7:\"config.\";a:1:{s:11:\"xmlSitemap.\";a:1:{s:9:\"sitemaps.\";a:2:{s:6:\"pages.\";a:2:{s:8:\"provider\";s:52:\"TYPO3\\CMS\\Seo\\XmlSitemap\\PagesXmlSitemapDataProvider\";s:7:\"config.\";a:4:{s:16:\"excludedDoktypes\";s:16:\"6, 254, 255, 199\";s:21:\"excludePagesRecursive\";s:0:\"\";s:15:\"additionalWhere\";s:35:\"AND (no_index = 0 OR no_follow = 0)\";s:8:\"rootPage\";s:1:\"1\";}}s:5:\"news.\";a:2:{s:8:\"provider\";s:47:\"GeorgRinger\\News\\Seo\\NewsXmlSitemapDataProvider\";s:7:\"config.\";a:7:{s:13:\"excludedTypes\";s:3:\"1,2\";s:15:\"additionalWhere\";s:0:\"\";s:9:\"sortField\";s:8:\"datetime\";s:17:\"lastModifiedField\";s:6:\"tstamp\";s:3:\"pid\";s:2:\"98\";s:9:\"recursive\";s:1:\"2\";s:4:\"url.\";a:7:{s:6:\"pageId\";s:3:\"140\";s:20:\"useCategorySinglePid\";s:1:\"1\";s:6:\"hrDate\";s:1:\"0\";s:7:\"hrDate.\";a:3:{s:3:\"day\";s:1:\"j\";s:5:\"month\";s:1:\"n\";s:4:\"year\";s:1:\"Y\";}s:20:\"fieldToParameterMap.\";a:1:{s:3:\"uid\";s:17:\"tx_news_pi1[news]\";}s:24:\"additionalGetParameters.\";a:1:{s:12:\"tx_news_pi1.\";a:2:{s:10:\"controller\";s:4:\"News\";s:6:\"action\";s:6:\"detail\";}}s:12:\"useCacheHash\";s:1:\"1\";}}}}}}}s:15:\"tx_twohtinypng.\";a:1:{s:9:\"settings.\";a:4:{s:6:\"apiKey\";s:0:\"\";s:5:\"width\";s:0:\"\";s:3:\"pid\";s:0:\"\";s:24:\"ignoreImagesByFolderName\";s:0:\"\";}}s:8:\"tx_solr.\";a:14:{s:7:\"enabled\";s:1:\"1\";s:15:\"enableDebugMode\";s:1:\"0\";s:8:\"general.\";a:1:{s:11:\"dateFormat.\";a:1:{s:4:\"date\";s:9:\"d.m.Y H:i\";}}s:6:\"index.\";a:2:{s:28:\"fieldProcessingInstructions.\";a:7:{s:7:\"changed\";s:18:\"timestampToIsoDate\";s:7:\"created\";s:18:\"timestampToIsoDate\";s:7:\"endtime\";s:21:\"timestampToUtcIsoDate\";s:8:\"rootline\";s:18:\"pageUidToHierarchy\";s:8:\"datetime\";s:18:\"timestampToIsoDate\";s:16:\"category_stringM\";s:9:\"serialize\";s:21:\"pageHierarchy_stringM\";s:15:\"pathToHierarchy\";}s:6:\"queue.\";a:4:{s:5:\"pages\";s:1:\"1\";s:6:\"pages.\";a:7:{s:14:\"initialization\";s:51:\"ApacheSolrForTypo3\\Solr\\IndexQueue\\Initializer\\Page\";s:16:\"allowedPageTypes\";s:3:\"1,7\";s:16:\"indexingPriority\";s:1:\"0\";s:7:\"indexer\";s:46:\"ApacheSolrForTypo3\\Solr\\IndexQueue\\PageIndexer\";s:21:\"additionalWhereClause\";s:65:\"(doktype = 1 OR (doktype=7 AND mount_pid_ol=0)) AND no_search = 0\";s:21:\"excludeContentByClass\";s:20:\"typo3-search-exclude\";s:7:\"fields.\";a:3:{s:20:\"sortSubTitle_stringS\";s:8:\"subtitle\";s:7:\"content\";s:7:\"CONTENT\";s:8:\"content.\";a:3:{s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:1:{s:8:\"andWhere\";s:8:\"colPos=0\";}s:8:\"stdWrap.\";a:1:{s:9:\"stripHtml\";s:1:\"1\";}}}}s:4:\"news\";s:1:\"1\";s:5:\"news.\";a:2:{s:5:\"table\";s:25:\"tx_news_domain_model_news\";s:7:\"fields.\";a:6:{s:21:\"additionalWhereClause\";s:12:\"pid IN (100)\";s:5:\"title\";s:5:\"title\";s:7:\"content\";s:12:\"SOLR_CONTENT\";s:8:\"content.\";a:1:{s:5:\"field\";s:8:\"bodytext\";}s:3:\"url\";s:4:\"TEXT\";s:4:\"url.\";a:1:{s:9:\"typolink.\";a:5:{s:9:\"parameter\";s:3:\"140\";s:16:\"additionalParams\";s:86:\"&tx_news_pi1[controller]=News&tx_news_pi1[action]=detail&tx_news_pi1[news]={field:uid}\";s:17:\"additionalParams.\";a:1:{s:10:\"insertData\";s:1:\"1\";}s:12:\"useCacheHash\";s:1:\"1\";s:10:\"returnLast\";s:3:\"url\";}}}}}}s:7:\"search.\";a:27:{s:13:\"trustedFields\";s:3:\"url\";s:10:\"targetPage\";s:1:\"0\";s:24:\"initializeWithEmptyQuery\";s:1:\"0\";s:30:\"showResultsOfInitialEmptyQuery\";s:1:\"0\";s:19:\"initializeWithQuery\";s:0:\"\";s:25:\"showResultsOfInitialQuery\";s:1:\"0\";s:36:\"keepExistingParametersForNewSearches\";s:1:\"1\";s:22:\"ignoreGlobalQParameter\";s:1:\"0\";s:6:\"query.\";a:15:{s:15:\"allowEmptyQuery\";s:1:\"0\";s:12:\"allowedSites\";s:19:\"__solr_current_site\";s:11:\"queryFields\";s:166:\"content^40.0, title^5.0, keywords^2.0, tagsH1^5.0, tagsH2H3^3.0, tagsH4H5H6^2.0, tagsInline^1.0, description^4.0, abstract^1.0, subtitle^1.0, navtitle^1.0, author^1.0\";s:12:\"returnFields\";s:8:\"*, score\";s:12:\"minimumMatch\";s:0:\"\";s:13:\"boostFunction\";s:0:\"\";s:10:\"boostQuery\";s:0:\"\";s:12:\"tieParameter\";s:0:\"\";s:6:\"sortBy\";s:0:\"\";s:6:\"phrase\";s:1:\"0\";s:7:\"phrase.\";a:3:{s:6:\"fields\";s:149:\"content^10.0, title^10.0, tagsH1^10.0, tagsH2H3^10.0, tagsH4H5H6^10.0, tagsInline^10.0, description^10.0, abstract^10.0, subtitle^10.0, navtitle^10.0\";s:4:\"slop\";s:1:\"0\";s:9:\"querySlop\";s:1:\"0\";}s:12:\"bigramPhrase\";s:1:\"0\";s:13:\"bigramPhrase.\";a:2:{s:6:\"fields\";s:149:\"content^10.0, title^10.0, tagsH1^10.0, tagsH2H3^10.0, tagsH4H5H6^10.0, tagsInline^10.0, description^10.0, abstract^10.0, subtitle^10.0, navtitle^10.0\";s:4:\"slop\";s:1:\"0\";}s:13:\"trigramPhrase\";s:1:\"0\";s:14:\"trigramPhrase.\";a:2:{s:6:\"fields\";s:149:\"content^10.0, title^10.0, tagsH1^10.0, tagsH2H3^10.0, tagsH4H5H6^10.0, tagsInline^10.0, description^10.0, abstract^10.0, subtitle^10.0, navtitle^10.0\";s:4:\"slop\";s:1:\"0\";}}s:8:\"grouping\";s:1:\"0\";s:9:\"grouping.\";a:2:{s:14:\"numberOfGroups\";s:1:\"5\";s:23:\"numberOfResultsPerGroup\";s:1:\"3\";}s:8:\"results.\";a:8:{s:19:\"resultsHighlighting\";s:1:\"0\";s:20:\"resultsHighlighting.\";a:4:{s:15:\"highlightFields\";s:7:\"content\";s:12:\"fragmentSize\";s:3:\"300\";s:17:\"fragmentSeparator\";s:5:\"[...]\";s:4:\"wrap\";s:39:\"<span class=\"label label-info\">|</span>\";}s:16:\"siteHighlighting\";s:1:\"0\";s:14:\"resultsPerPage\";s:2:\"10\";s:27:\"resultsPerPageSwitchOptions\";s:10:\"10, 25, 50\";s:17:\"maxPaginatorLinks\";s:1:\"0\";s:25:\"showDocumentScoreAnalysis\";s:1:\"0\";s:27:\"fieldRenderingInstructions.\";a:2:{s:7:\"content\";s:4:\"TEXT\";s:8:\"content.\";a:2:{s:5:\"field\";s:7:\"content\";s:4:\"crop\";s:13:\"300 | ... | 1\";}}}s:13:\"spellchecking\";s:1:\"0\";s:14:\"spellchecking.\";a:3:{s:4:\"wrap\";s:64:\"|<div class=\"spelling-suggestions\">###LLL:didYouMean### |</div>|\";s:33:\"searchUsingSpellCheckerSuggestion\";s:1:\"1\";s:24:\"numberOfSuggestionsToTry\";s:1:\"3\";}s:12:\"lastSearches\";s:1:\"0\";s:13:\"lastSearches.\";a:2:{s:5:\"limit\";s:2:\"10\";s:4:\"mode\";s:6:\"global\";}s:16:\"frequentSearches\";s:1:\"0\";s:17:\"frequentSearches.\";a:6:{s:20:\"useLowercaseKeywords\";s:1:\"1\";s:7:\"minSize\";s:2:\"14\";s:7:\"maxSize\";s:2:\"32\";s:5:\"limit\";s:2:\"20\";s:7:\"select.\";a:7:{s:6:\"SELECT\";s:41:\"keywords as search_term, count(*) as hits\";s:4:\"FROM\";s:18:\"tx_solr_statistics\";s:9:\"ADD_WHERE\";s:18:\"AND num_found != 0\";s:8:\"GROUP_BY\";s:8:\"keywords\";s:8:\"ORDER_BY\";s:26:\"hits DESC, search_term ASC\";s:15:\"checkRootPageId\";s:1:\"1\";s:13:\"checkLanguage\";s:1:\"1\";}s:13:\"cacheLifetime\";s:5:\"86400\";}s:7:\"sorting\";s:1:\"0\";s:8:\"sorting.\";a:2:{s:12:\"defaultOrder\";s:3:\"asc\";s:8:\"options.\";a:3:{s:8:\"created.\";a:3:{s:5:\"field\";s:7:\"created\";s:5:\"label\";s:13:\"Creation Date\";s:6:\"label.\";a:1:{s:5:\"lang.\";a:1:{s:2:\"de\";s:16:\"Erstellungsdatum\";}}}s:6:\"title.\";a:3:{s:5:\"field\";s:9:\"sortTitle\";s:5:\"label\";s:5:\"Title\";s:6:\"label.\";a:1:{s:5:\"lang.\";a:1:{s:2:\"de\";s:5:\"Titel\";}}}s:10:\"relevance.\";a:3:{s:5:\"field\";s:9:\"relevance\";s:5:\"label\";s:9:\"Relevance\";s:6:\"label.\";a:1:{s:5:\"lang.\";a:1:{s:2:\"de\";s:8:\"Relevanz\";}}}}}s:8:\"faceting\";s:1:\"1\";s:9:\"faceting.\";a:10:{s:12:\"minimumCount\";s:1:\"1\";s:6:\"sortBy\";s:5:\"count\";s:5:\"limit\";s:2:\"10\";s:15:\"showEmptyFacets\";s:1:\"0\";s:24:\"keepAllFacetsOnSelection\";s:1:\"0\";s:17:\"urlParameterStyle\";s:5:\"index\";s:16:\"urlParameterSort\";s:1:\"0\";s:22:\"facetLinkUrlParameters\";s:0:\"\";s:7:\"facets.\";a:2:{s:5:\"type.\";a:4:{s:5:\"label\";s:10:\"Inhaltstyp\";s:5:\"field\";s:4:\"type\";s:20:\"renderingInstruction\";s:4:\"CASE\";s:21:\"renderingInstruction.\";a:7:{s:4:\"key.\";a:1:{s:5:\"field\";s:11:\"optionValue\";}s:7:\"default\";s:4:\"TEXT\";s:8:\"default.\";a:1:{s:5:\"field\";s:11:\"optionValue\";}s:5:\"pages\";s:4:\"TEXT\";s:6:\"pages.\";a:2:{s:5:\"value\";s:5:\"Pages\";s:5:\"lang.\";a:1:{s:2:\"de\";s:6:\"Seiten\";}}s:25:\"tx_news_domain_model_news\";s:4:\"TEXT\";s:26:\"tx_news_domain_model_news.\";a:2:{s:5:\"value\";s:4:\"News\";s:5:\"lang.\";a:1:{s:2:\"de\";s:11:\"Nachrichten\";}}}}s:9:\"category.\";a:5:{s:5:\"field\";s:16:\"category_stringM\";s:5:\"label\";s:9:\"Kategorie\";s:13:\"requirements.\";a:1:{s:11:\"typeIsNews.\";a:2:{s:5:\"facet\";s:4:\"type\";s:6:\"values\";s:25:\"tx_news_domain_model_news\";}}s:20:\"renderingInstruction\";s:4:\"CASE\";s:21:\"renderingInstruction.\";a:3:{s:4:\"key.\";a:2:{s:5:\"field\";s:11:\"optionValue\";s:12:\"replacement.\";a:1:{s:3:\"10.\";a:3:{s:6:\"search\";s:4:\"/\\s/\";s:7:\"replace\";s:1:\"_\";s:9:\"useRegExp\";s:1:\"1\";}}}s:7:\"default\";s:4:\"TEXT\";s:8:\"default.\";a:1:{s:5:\"field\";s:11:\"optionValue\";}}}}s:12:\"showAllLink.\";a:1:{s:4:\"wrap\";s:10:\"<li>|</li>\";}}s:9:\"elevation\";s:1:\"0\";s:10:\"elevation.\";a:2:{s:19:\"markElevatedResults\";s:1:\"1\";s:14:\"forceElevation\";s:1:\"1\";}s:8:\"variants\";s:1:\"0\";s:9:\"variants.\";a:3:{s:6:\"expand\";s:1:\"1\";s:12:\"variantField\";s:9:\"variantId\";s:5:\"limit\";s:2:\"10\";}s:12:\"highlighting\";s:1:\"1\";}s:7:\"suggest\";s:1:\"1\";s:8:\"suggest.\";a:5:{s:19:\"numberOfSuggestions\";s:2:\"10\";s:12:\"suggestField\";s:5:\"spell\";s:10:\"forceHttps\";s:1:\"0\";s:14:\"showTopResults\";s:1:\"1\";s:18:\"numberOfTopResults\";s:1:\"5\";}s:10:\"statistics\";s:1:\"0\";s:11:\"statistics.\";a:1:{s:11:\"anonymizeIP\";s:1:\"1\";}s:5:\"view.\";a:4:{s:15:\"pluginNamespace\";s:7:\"tx_solr\";s:18:\"templateRootPaths.\";a:2:{i:0;s:37:\"EXT:solr/Resources/Private/Templates/\";i:10;s:73:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/Solr/\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:36:\"EXT:solr/Resources/Private/Partials/\";i:10;s:72:\"EXT:twoh_template_override_x/Resources/Private/Partials/Extensions/Solr/\";}s:16:\"layoutRootPaths.\";a:2:{i:0;s:35:\"EXT:solr/Resources/Private/Layouts/\";i:10;s:71:\"EXT:twoh_template_override_x/Resources/Private/Layouts/Extensions/Solr/\";}}s:8:\"logging.\";a:4:{s:10:\"exceptions\";s:1:\"1\";s:11:\"debugOutput\";s:1:\"0\";s:9:\"indexing.\";a:4:{s:24:\"indexQueueInitialization\";s:1:\"1\";s:25:\"missingTypo3SearchMarkers\";s:1:\"1\";s:11:\"pageIndexed\";s:1:\"1\";s:6:\"queue.\";a:2:{s:5:\"pages\";s:1:\"1\";s:4:\"news\";s:1:\"1\";}}s:6:\"query.\";a:6:{s:7:\"filters\";s:1:\"0\";s:11:\"searchWords\";s:1:\"0\";s:11:\"queryString\";s:1:\"0\";s:7:\"rawPost\";s:1:\"0\";s:6:\"rawGet\";s:1:\"0\";s:9:\"rawDelete\";s:1:\"0\";}}s:9:\"features.\";a:2:{s:38:\"requireCHashArgumentForActionArguments\";s:1:\"0\";s:15:\"useRawDocuments\";s:1:\"1\";}s:9:\"settings.\";a:1:{s:9:\"faceting.\";a:1:{s:5:\"limit\";s:2:\"10\";}}s:12:\"_LOCAL_LANG.\";a:2:{s:8:\"default.\";a:10:{s:17:\"searchPlaceholder\";s:11:\"BVCD-Search\";s:11:\"searchTitle\";s:34:\"Please enter your search term here\";s:5:\"pages\";s:4:\"Page\";s:4:\"news\";s:4:\"News\";s:7:\"created\";s:8:\"Created:\";s:8:\"modified\";s:9:\"Modified:\";s:9:\"published\";s:10:\"Published:\";s:4:\"date\";s:5:\"Date:\";s:4:\"type\";s:5:\"Type:\";s:8:\"category\";s:9:\"Category:\";}s:3:\"de.\";a:10:{s:17:\"searchPlaceholder\";s:12:\"DEHOGA-Suche\";s:11:\"searchTitle\";s:42:\"Bitte geben Sie hier Ihren Suchbegriff ein\";s:5:\"pages\";s:5:\"Seite\";s:4:\"news\";s:9:\"Nachricht\";s:7:\"created\";s:9:\"Erstellt:\";s:8:\"modified\";s:19:\"Zuletzt Verändert:\";s:9:\"published\";s:16:\"Veröffentlicht:\";s:4:\"date\";s:6:\"Datum:\";s:4:\"type\";s:11:\"Inhaltstyp:\";s:8:\"category\";s:10:\"Kategorie:\";}}}s:25:\"tx_solr_PiResults_Results\";s:8:\"USER_INT\";s:26:\"tx_solr_PiResults_Results.\";a:7:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:10:\"vendorName\";s:18:\"ApacheSolrForTypo3\";s:13:\"extensionName\";s:4:\"Solr\";s:9:\"settings.\";a:1:{s:9:\"faceting.\";a:1:{s:5:\"limit\";s:2:\"10\";}}s:5:\"view.\";a:4:{s:15:\"pluginNamespace\";s:7:\"tx_solr\";s:18:\"templateRootPaths.\";a:2:{i:0;s:37:\"EXT:solr/Resources/Private/Templates/\";i:10;s:0:\"\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:36:\"EXT:solr/Resources/Private/Partials/\";i:10;s:0:\"\";}s:16:\"layoutRootPaths.\";a:2:{i:0;s:35:\"EXT:solr/Resources/Private/Layouts/\";i:10;s:0:\"\";}}s:10:\"pluginName\";s:10:\"pi_results\";s:28:\"switchableControllerActions.\";a:1:{s:7:\"Search.\";a:2:{i:1;s:7:\"results\";i:2;s:4:\"form\";}}}s:23:\"tx_solr_PiSearch_Search\";s:4:\"USER\";s:24:\"tx_solr_PiSearch_Search.\";a:7:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:10:\"vendorName\";s:18:\"ApacheSolrForTypo3\";s:13:\"extensionName\";s:4:\"Solr\";s:9:\"settings.\";a:1:{s:9:\"faceting.\";a:1:{s:5:\"limit\";s:2:\"10\";}}s:5:\"view.\";a:4:{s:15:\"pluginNamespace\";s:7:\"tx_solr\";s:18:\"templateRootPaths.\";a:2:{i:0;s:37:\"EXT:solr/Resources/Private/Templates/\";i:10;s:0:\"\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:36:\"EXT:solr/Resources/Private/Partials/\";i:10;s:0:\"\";}s:16:\"layoutRootPaths.\";a:2:{i:0;s:35:\"EXT:solr/Resources/Private/Layouts/\";i:10;s:0:\"\";}}s:10:\"pluginName\";s:9:\"pi_search\";s:28:\"switchableControllerActions.\";a:1:{s:7:\"Search.\";a:1:{i:1;s:4:\"form\";}}}s:43:\"tx_solr_PiFrequentSearches_FrequentSearches\";s:4:\"USER\";s:44:\"tx_solr_PiFrequentSearches_FrequentSearches.\";a:7:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:10:\"vendorName\";s:18:\"ApacheSolrForTypo3\";s:13:\"extensionName\";s:4:\"Solr\";s:9:\"settings.\";a:1:{s:9:\"faceting.\";a:1:{s:5:\"limit\";s:2:\"10\";}}s:5:\"view.\";a:4:{s:15:\"pluginNamespace\";s:7:\"tx_solr\";s:18:\"templateRootPaths.\";a:2:{i:0;s:37:\"EXT:solr/Resources/Private/Templates/\";i:10;s:0:\"\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:36:\"EXT:solr/Resources/Private/Partials/\";i:10;s:0:\"\";}s:16:\"layoutRootPaths.\";a:2:{i:0;s:35:\"EXT:solr/Resources/Private/Layouts/\";i:10;s:0:\"\";}}s:10:\"pluginName\";s:21:\"pi_frequentlySearched\";s:28:\"switchableControllerActions.\";a:1:{s:7:\"Search.\";a:1:{i:1;s:18:\"frequentlySearched\";}}}s:8:\"tx_news.\";a:3:{s:4:\"mvc.\";a:1:{s:39:\"callDefaultActionIfActionCantBeResolved\";s:1:\"1\";}s:5:\"view.\";a:3:{s:16:\"layoutRootPaths.\";a:2:{i:0;s:35:\"EXT:news/Resources/Private/Layouts/\";i:1;s:35:\"EXT:news/Resources/Private/Layouts/\";}s:18:\"templateRootPaths.\";a:2:{i:0;s:37:\"EXT:news/Resources/Private/Templates/\";i:1;s:73:\"EXT:twoh_template_override_x/Resources/Private/Templates/Extensions/News/\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:36:\"EXT:news/Resources/Private/Partials/\";i:1;s:72:\"EXT:twoh_template_override_x/Resources/Private/Partials/Extensions/News/\";}}s:9:\"settings.\";a:22:{s:7:\"cssFile\";s:44:\"EXT:news/Resources/Public/Css/news-basic.css\";s:21:\"displayDummyIfNoMedia\";s:1:\"1\";s:6:\"format\";s:4:\"html\";s:31:\"overrideFlexformSettingsIfEmpty\";s:199:\"cropMaxCharacters,dateField,timeRestriction,timeRestrictionHigh,archiveRestriction,orderBy,orderDirection,backPid,listPid,startingpoint,recursive,list.paginate.itemsPerPage,list.paginate.templatePath\";s:35:\"allowEmptyStringsForOverwriteDemand\";s:1:\"0\";s:20:\"includeSubCategories\";s:1:\"0\";s:10:\"analytics.\";a:1:{s:7:\"social.\";a:3:{s:12:\"facebookLike\";s:1:\"1\";s:13:\"facebookShare\";s:1:\"1\";s:7:\"twitter\";s:1:\"1\";}}s:22:\"detailPidDetermination\";s:29:\"flexform, categories, default\";s:16:\"defaultDetailPid\";s:1:\"0\";s:9:\"dateField\";s:8:\"datetime\";s:5:\"link.\";a:3:{s:23:\"typesOpeningInNewWindow\";s:1:\"2\";s:6:\"hrDate\";s:1:\"0\";s:7:\"hrDate.\";a:3:{s:3:\"day\";s:1:\"j\";s:5:\"month\";s:1:\"n\";s:4:\"year\";s:1:\"Y\";}}s:17:\"cropMaxCharacters\";s:3:\"150\";s:7:\"orderBy\";s:8:\"datetime\";s:14:\"orderDirection\";s:4:\"desc\";s:12:\"topNewsFirst\";s:1:\"0\";s:14:\"orderByAllowed\";s:78:\"sorting,author,uid,title,teaser,author,tstamp,crdate,datetime,categories.title\";s:14:\"facebookLocale\";s:5:\"en_US\";s:11:\"demandClass\";s:0:\"\";s:7:\"search.\";a:3:{s:6:\"fields\";s:21:\"teaser,title,bodytext\";s:15:\"splitSearchWord\";s:1:\"0\";s:9:\"paginate.\";a:5:{s:5:\"class\";s:49:\"GeorgRinger\\NumberedPagination\\NumberedPagination\";s:12:\"itemsPerPage\";s:2:\"10\";s:11:\"insertAbove\";s:1:\"1\";s:11:\"insertBelow\";s:1:\"1\";s:20:\"maximumNumberOfLinks\";s:1:\"3\";}}s:7:\"detail.\";a:9:{s:13:\"errorHandling\";s:88:\"showStandaloneTemplate,EXT:news/Resources/Private/Templates/News/DetailNotFound.html,404\";s:20:\"checkPidOfNewsRecord\";s:1:\"0\";s:18:\"registerProperties\";s:14:\"keywords,title\";s:12:\"showPrevNext\";s:1:\"0\";s:22:\"showSocialShareButtons\";s:1:\"1\";s:12:\"showMetaTags\";s:1:\"1\";s:6:\"media.\";a:2:{s:6:\"image.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:8:\"maxWidth\";s:3:\"282\";s:9:\"maxHeight\";s:0:\"\";s:9:\"lightbox.\";a:5:{s:7:\"enabled\";s:1:\"0\";s:5:\"class\";s:8:\"lightbox\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:3:\"rel\";s:20:\"lightbox[myImageSet]\";}}s:6:\"video.\";a:2:{s:5:\"width\";s:3:\"282\";s:6:\"height\";s:3:\"159\";}}s:9:\"pageTitle\";s:1:\"1\";s:10:\"pageTitle.\";a:2:{s:8:\"provider\";s:38:\"GeorgRinger\\News\\Seo\\NewsTitleProvider\";s:10:\"properties\";s:22:\"alternativeTitle,title\";}}s:5:\"list.\";a:3:{s:6:\"media.\";a:2:{s:6:\"image.\";a:3:{s:11:\"lazyLoading\";s:4:\"lazy\";s:8:\"maxWidth\";s:3:\"100\";s:9:\"maxHeight\";s:3:\"100\";}s:10:\"dummyImage\";s:56:\"EXT:news/Resources/Public/Images/dummy-preview-image.png\";}s:9:\"paginate.\";a:5:{s:5:\"class\";s:49:\"GeorgRinger\\NumberedPagination\\NumberedPagination\";s:12:\"itemsPerPage\";s:2:\"10\";s:11:\"insertAbove\";s:1:\"1\";s:11:\"insertBelow\";s:1:\"1\";s:20:\"maximumNumberOfLinks\";s:1:\"3\";}s:4:\"rss.\";a:1:{s:8:\"channel.\";a:8:{s:5:\"title\";s:11:\"Dummy Title\";s:11:\"description\";s:0:\"\";s:8:\"language\";s:5:\"en-gb\";s:9:\"copyright\";s:10:\"TYPO3 News\";s:9:\"generator\";s:14:\"TYPO3 EXT:news\";s:4:\"link\";s:18:\"http://example.com\";s:7:\"typeNum\";s:4:\"9818\";s:3:\"ttl\";s:0:\"\";}}}s:10:\"opengraph.\";a:5:{s:9:\"site_name\";s:0:\"\";s:4:\"type\";s:7:\"article\";s:6:\"admins\";s:0:\"\";s:6:\"locale\";s:0:\"\";s:8:\"twitter.\";a:3:{s:4:\"card\";s:7:\"summary\";s:4:\"site\";s:0:\"\";s:7:\"creator\";s:0:\"\";}}}}s:12:\"tx_twohatom.\";a:1:{s:9:\"settings.\";a:10:{s:10:\"headerMenu\";s:1:\"1\";s:10:\"footerMenu\";s:1:\"8\";s:11:\"companyName\";s:14:\"schubwerk GmbH\";s:19:\"companyNameAppendix\";s:0:\"\";s:14:\"companyAddress\";s:33:\"Moritzstraße 49, 65185 Wiesbaden\";s:20:\"companyAddressStreet\";s:16:\"Moritzstraße 49\";s:21:\"companyAddressZipCity\";s:15:\"65185 Wiesbaden\";s:12:\"companyPhone\";s:19:\"+49 (0)611 94583570\";s:12:\"companyEmail\";s:17:\"info@schubwerk.de\";s:15:\"profileIconLink\";s:7:\"/login/\";}}s:8:\"tx_form.\";a:1:{s:9:\"settings.\";a:1:{s:19:\"yamlConfigurations.\";a:1:{i:100;s:77:\"EXT:twoh_template_override_x/Configuration/FormFramework/CustomFormSetup.yaml\";}}}}s:4:\"lib.\";a:24:{s:12:\"contentBlock\";s:13:\"FLUIDTEMPLATE\";s:13:\"contentBlock.\";a:1:{s:15:\"dataProcessing.\";a:1:{i:10;s:14:\"content-blocks\";}}s:14:\"contentElement\";s:13:\"FLUIDTEMPLATE\";s:15:\"contentElement.\";a:5:{s:12:\"templateName\";s:7:\"Default\";s:18:\"templateRootPaths.\";a:3:{i:0;s:53:\"EXT:fluid_styled_content/Resources/Private/Templates/\";i:10;s:0:\"\";i:200;s:66:\"EXT:twoh_kickstarter/Resources/Private/Templates/Typo3CoreElements\";}s:17:\"partialRootPaths.\";a:3:{i:0;s:52:\"EXT:fluid_styled_content/Resources/Private/Partials/\";i:10;s:0:\"\";i:200;s:65:\"EXT:twoh_kickstarter/Resources/Private/Partials/Typo3CoreElements\";}s:16:\"layoutRootPaths.\";a:3:{i:0;s:51:\"EXT:fluid_styled_content/Resources/Private/Layouts/\";i:10;s:0:\"\";i:200;s:63:\"EXT:twoh_kickstarter/Resources/Private/Layout/Typo3CoreElements\";}s:9:\"settings.\";a:2:{s:17:\"defaultHeaderType\";s:1:\"2\";s:6:\"media.\";a:4:{s:11:\"lazyLoading\";s:4:\"lazy\";s:13:\"imageDecoding\";s:0:\"\";s:6:\"popup.\";a:9:{s:7:\"bodyTag\";s:41:\"<body style=\"margin:0; background:#fff;\">\";s:4:\"wrap\";s:37:\"<a href=\"javascript:close();\"> | </a>\";s:5:\"width\";s:4:\"800m\";s:6:\"height\";s:4:\"600m\";s:5:\"crop.\";a:1:{s:4:\"data\";s:17:\"file:current:crop\";}s:8:\"JSwindow\";s:1:\"1\";s:9:\"JSwindow.\";a:2:{s:9:\"newWindow\";s:1:\"0\";s:3:\"if.\";a:1:{s:7:\"isFalse\";s:1:\"0\";}}s:15:\"directImageLink\";s:1:\"0\";s:11:\"linkParams.\";a:1:{s:11:\"ATagParams.\";a:1:{s:8:\"dataWrap\";s:44:\"class=\"lightbox\" rel=\"lightbox[{field:uid}]\"\";}}}s:17:\"additionalConfig.\";a:2:{s:9:\"no-cookie\";s:1:\"1\";s:3:\"api\";s:1:\"0\";}}}}s:10:\"parseFunc.\";a:8:{s:9:\"makelinks\";s:1:\"1\";s:10:\"makelinks.\";a:2:{s:5:\"http.\";a:2:{s:4:\"keep\";s:4:\"path\";s:9:\"extTarget\";s:6:\"_blank\";}s:7:\"mailto.\";a:1:{s:4:\"keep\";s:4:\"path\";}}s:5:\"tags.\";a:2:{s:1:\"a\";s:4:\"TEXT\";s:2:\"a.\";a:2:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:5:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:15:\"parameters:href\";}s:6:\"title.\";a:1:{s:4:\"data\";s:16:\"parameters:title\";}s:11:\"ATagParams.\";a:1:{s:4:\"data\";s:20:\"parameters:allParams\";}s:7:\"target.\";a:1:{s:8:\"ifEmpty.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}}s:10:\"extTarget.\";a:2:{s:8:\"ifEmpty.\";a:1:{s:8:\"override\";s:6:\"_blank\";}s:9:\"override.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}}}}}s:9:\"allowTags\";s:412:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, figcaption, figure, font, footer, header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, s, samp, sdfield, section, small, span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:8:\"denyTags\";s:1:\"*\";s:9:\"constants\";s:1:\"1\";s:18:\"nonTypoTagStdWrap.\";a:2:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"2\";}}s:12:\"htmlSanitize\";s:1:\"1\";}s:14:\"parseFunc_RTE.\";a:10:{s:9:\"makelinks\";s:1:\"1\";s:10:\"makelinks.\";a:2:{s:5:\"http.\";a:2:{s:4:\"keep\";s:4:\"path\";s:9:\"extTarget\";s:6:\"_blank\";}s:7:\"mailto.\";a:1:{s:4:\"keep\";s:4:\"path\";}}s:5:\"tags.\";a:2:{s:1:\"a\";s:4:\"TEXT\";s:2:\"a.\";a:2:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:5:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:15:\"parameters:href\";}s:6:\"title.\";a:1:{s:4:\"data\";s:16:\"parameters:title\";}s:11:\"ATagParams.\";a:1:{s:4:\"data\";s:20:\"parameters:allParams\";}s:7:\"target.\";a:1:{s:8:\"ifEmpty.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}}s:10:\"extTarget.\";a:2:{s:8:\"ifEmpty.\";a:1:{s:8:\"override\";s:6:\"_blank\";}s:9:\"override.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}}}}}s:9:\"allowTags\";s:412:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, figcaption, figure, font, footer, header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, s, samp, sdfield, section, small, span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:8:\"denyTags\";s:1:\"*\";s:9:\"constants\";s:1:\"1\";s:18:\"nonTypoTagStdWrap.\";a:3:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"2\";}s:12:\"encapsLines.\";a:4:{s:13:\"encapsTagList\";s:29:\"p,pre,h1,h2,h3,h4,h5,h6,hr,dt\";s:9:\"remapTag.\";a:1:{s:3:\"DIV\";s:1:\"P\";}s:13:\"nonWrappedTag\";s:1:\"P\";s:17:\"innerStdWrap_all.\";a:1:{s:7:\"ifBlank\";s:6:\"&nbsp;\";}}}s:12:\"htmlSanitize\";s:1:\"1\";s:14:\"externalBlocks\";s:109:\"article, aside, blockquote, div, dd, dl, footer, header, nav, ol, section, table, ul, pre, figure, figcaption\";s:15:\"externalBlocks.\";a:16:{s:3:\"ol.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:8:\"stdWrap.\";a:1:{s:9:\"parseFunc\";s:15:\"< lib.parseFunc\";}}s:3:\"ul.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:8:\"stdWrap.\";a:1:{s:9:\"parseFunc\";s:15:\"< lib.parseFunc\";}}s:4:\"pre.\";a:1:{s:8:\"stdWrap.\";a:1:{s:10:\"parseFunc.\";a:8:{s:9:\"makelinks\";s:1:\"1\";s:10:\"makelinks.\";a:2:{s:5:\"http.\";a:2:{s:4:\"keep\";s:4:\"path\";s:9:\"extTarget\";s:6:\"_blank\";}s:7:\"mailto.\";a:1:{s:4:\"keep\";s:4:\"path\";}}s:5:\"tags.\";a:2:{s:1:\"a\";s:4:\"TEXT\";s:2:\"a.\";a:2:{s:7:\"current\";s:1:\"1\";s:9:\"typolink.\";a:5:{s:10:\"parameter.\";a:1:{s:4:\"data\";s:15:\"parameters:href\";}s:6:\"title.\";a:1:{s:4:\"data\";s:16:\"parameters:title\";}s:11:\"ATagParams.\";a:1:{s:4:\"data\";s:20:\"parameters:allParams\";}s:7:\"target.\";a:1:{s:8:\"ifEmpty.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}}s:10:\"extTarget.\";a:2:{s:8:\"ifEmpty.\";a:1:{s:8:\"override\";s:6:\"_blank\";}s:9:\"override.\";a:1:{s:4:\"data\";s:17:\"parameters:target\";}}}}}s:9:\"allowTags\";s:412:\"a, abbr, acronym, address, article, aside, b, bdo, big, blockquote, br, caption, center, cite, code, col, colgroup, dd, del, dfn, dl, div, dt, em, figcaption, figure, font, footer, header, h1, h2, h3, h4, h5, h6, hr, i, img, ins, kbd, label, li, link, meta, nav, ol, p, pre, q, s, samp, sdfield, section, small, span, strike, strong, style, sub, sup, table, thead, tbody, tfoot, td, th, tr, title, tt, u, ul, var\";s:8:\"denyTags\";s:1:\"*\";s:9:\"constants\";s:1:\"1\";s:18:\"nonTypoTagStdWrap.\";a:2:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:18:\"keepNonMatchedTags\";s:1:\"1\";s:16:\"htmlSpecialChars\";s:1:\"2\";}}s:12:\"htmlSanitize\";s:1:\"1\";}}}s:6:\"table.\";a:4:{s:7:\"stripNL\";s:1:\"1\";s:8:\"stdWrap.\";a:2:{s:10:\"HTMLparser\";s:1:\"1\";s:11:\"HTMLparser.\";a:2:{s:5:\"tags.\";a:1:{s:6:\"table.\";a:1:{s:10:\"fixAttrib.\";a:1:{s:6:\"class.\";a:3:{s:7:\"default\";s:12:\"contenttable\";s:6:\"always\";s:1:\"1\";s:4:\"list\";s:12:\"contenttable\";}}}}s:18:\"keepNonMatchedTags\";s:1:\"1\";}}s:14:\"HTMLtableCells\";s:1:\"1\";s:15:\"HTMLtableCells.\";a:2:{s:8:\"default.\";a:1:{s:8:\"stdWrap.\";a:2:{s:9:\"parseFunc\";s:19:\"< lib.parseFunc_RTE\";s:10:\"parseFunc.\";a:1:{s:18:\"nonTypoTagStdWrap.\";a:1:{s:12:\"encapsLines.\";a:2:{s:13:\"nonWrappedTag\";s:0:\"\";s:17:\"innerStdWrap_all.\";a:1:{s:7:\"ifBlank\";s:0:\"\";}}}}}}s:25:\"addChr10BetweenParagraphs\";s:1:\"1\";}}s:4:\"div.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:8:\"article.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:6:\"aside.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:7:\"figure.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:11:\"figcaption.\";a:1:{s:7:\"stripNL\";s:1:\"1\";}s:11:\"blockquote.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:7:\"footer.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:7:\"header.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:4:\"nav.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:8:\"section.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:3:\"dl.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}s:3:\"dd.\";a:2:{s:7:\"stripNL\";s:1:\"1\";s:13:\"callRecursive\";s:1:\"1\";}}}s:22:\"solr_extbase_bootstrap\";s:4:\"USER\";s:23:\"solr_extbase_bootstrap.\";a:5:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:10:\"vendorName\";s:18:\"ApacheSolrForTypo3\";s:13:\"extensionName\";s:4:\"Solr\";s:9:\"settings.\";a:1:{s:9:\"faceting.\";a:1:{s:5:\"limit\";s:2:\"10\";}}s:5:\"view.\";a:4:{s:15:\"pluginNamespace\";s:7:\"tx_solr\";s:18:\"templateRootPaths.\";a:2:{i:0;s:37:\"EXT:solr/Resources/Private/Templates/\";i:10;s:0:\"\";}s:17:\"partialRootPaths.\";a:2:{i:0;s:36:\"EXT:solr/Resources/Private/Partials/\";i:10;s:0:\"\";}s:16:\"layoutRootPaths.\";a:2:{i:0;s:35:\"EXT:solr/Resources/Private/Layouts/\";i:10;s:0:\"\";}}}s:8:\"tx_news.\";a:2:{s:23:\"contentElementRendering\";s:7:\"RECORDS\";s:24:\"contentElementRendering.\";a:3:{s:6:\"tables\";s:10:\"tt_content\";s:7:\"source.\";a:1:{s:7:\"current\";s:1:\"1\";}s:12:\"dontCheckPid\";s:1:\"1\";}}s:8:\"content.\";a:2:{s:6:\"render\";s:7:\"CONTENT\";s:7:\"render.\";a:2:{s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:2:{s:7:\"orderBy\";s:7:\"sorting\";s:6:\"where.\";a:2:{s:7:\"cObject\";s:3:\"COA\";s:8:\"cObject.\";a:2:{i:10;s:4:\"TEXT\";s:3:\"10.\";a:4:{s:5:\"field\";s:6:\"colPos\";s:6:\"intval\";s:1:\"1\";s:7:\"ifEmpty\";s:1:\"0\";s:10:\"noTrimWrap\";s:15:\"| AND colPos=||\";}}}}}}s:16:\"contentForSearch\";s:7:\"CONTENT\";s:17:\"contentForSearch.\";a:2:{s:5:\"table\";s:10:\"tt_content\";s:7:\"select.\";a:1:{s:8:\"andWhere\";s:8:\"colPos=0\";}}s:9:\"copyright\";s:4:\"TEXT\";s:10:\"copyright.\";a:3:{s:4:\"data\";s:8:\"date : U\";s:8:\"strftime\";s:2:\"%Y\";s:10:\"noTrimWrap\";s:9:\"|&copy; |\";}s:12:\"websiteTitle\";s:4:\"TEXT\";s:13:\"websiteTitle.\";a:1:{s:4:\"data\";s:17:\"site:websiteTitle\";}s:11:\"websiteLogo\";s:4:\"TEXT\";s:12:\"websiteLogo.\";a:1:{s:4:\"data\";s:16:\"site:websiteLogo\";}s:11:\"companyName\";s:4:\"TEXT\";s:12:\"companyName.\";a:1:{s:4:\"data\";s:16:\"site:companyName\";}s:17:\"websiteRootPageId\";s:4:\"TEXT\";s:18:\"websiteRootPageId.\";a:1:{s:4:\"data\";s:15:\"site:rootPageId\";}s:8:\"pageInfo\";s:3:\"COA\";s:9:\"pageInfo.\";a:5:{i:5;s:13:\"LOAD_REGISTER\";s:2:\"5.\";a:2:{s:10:\"pageField.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:9:\"pageField\";s:8:\"ifEmpty.\";a:1:{s:4:\"data\";s:5:\"title\";}}}s:8:\"pageUid.\";a:2:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:2:{s:5:\"field\";s:7:\"pageUid\";s:8:\"ifEmpty.\";a:1:{s:4:\"data\";s:7:\"TSFE:id\";}}}}i:20;s:7:\"RECORDS\";s:3:\"20.\";a:4:{s:7:\"source.\";a:1:{s:4:\"data\";s:16:\"register:pageUid\";}s:6:\"tables\";s:5:\"pages\";s:12:\"dontCheckPid\";s:1:\"1\";s:5:\"conf.\";a:2:{s:5:\"pages\";s:4:\"TEXT\";s:6:\"pages.\";a:1:{s:6:\"field.\";a:1:{s:4:\"data\";s:18:\"register:pageField\";}}}}i:90;s:16:\"RESTORE_REGISTER\";}}s:11:\"seo_sitemap\";s:4:\"PAGE\";s:12:\"seo_sitemap.\";a:4:{s:7:\"typeNum\";s:10:\"1533906435\";s:7:\"config.\";a:7:{s:12:\"cache_period\";s:3:\"900\";s:20:\"disableAllHeaderCode\";s:1:\"1\";s:8:\"admPanel\";s:1:\"0\";s:15:\"removeDefaultJS\";s:1:\"1\";s:16:\"removeDefaultCss\";s:1:\"1\";s:13:\"removePageCss\";s:1:\"1\";s:18:\"additionalHeaders.\";a:2:{s:3:\"10.\";a:1:{s:6:\"header\";s:42:\"Content-Type:application/xml;charset=utf-8\";}s:3:\"20.\";a:1:{s:6:\"header\";s:20:\"X-Robots-Tag:noindex\";}}}i:10;s:4:\"USER\";s:3:\"10.\";a:1:{s:8:\"userFunc\";s:51:\"TYPO3\\CMS\\Seo\\XmlSitemap\\XmlSitemapRenderer->render\";}}s:4:\"page\";s:4:\"PAGE\";s:5:\"page.\";a:7:{s:11:\"includeCSS.\";a:4:{s:6:\"search\";s:58:\"EXT:solr/Resources/Public/StyleSheets/Frontend/results.css\";s:11:\"solr-loader\";s:57:\"EXT:solr/Resources/Public/StyleSheets/Frontend/loader.css\";s:12:\"solr-suggest\";s:58:\"EXT:solr/Resources/Public/StyleSheets/Frontend/suggest.css\";s:7:\"atomCss\";s:20:\"theme/css/styles.css\";}s:20:\"includeJSFooterlibs.\";a:10:{s:11:\"solr-jquery\";s:57:\"EXT:solr/Resources/Public/JavaScript/JQuery/jquery.min.js\";s:17:\"solr-autocomplete\";s:70:\"EXT:solr/Resources/Public/JavaScript/JQuery/jquery.autocomplete.min.js\";s:12:\"solr-suggest\";s:58:\"EXT:solr/Resources/Public/JavaScript/suggest_controller.js\";s:6:\"jQuery\";s:28:\"theme/js/jquery-3.7.0.min.js\";s:7:\"jQuery.\";a:1:{s:10:\"forceOnTop\";s:1:\"1\";}s:9:\"popperMin\";s:22:\"theme/js/popper.min.js\";s:12:\"bootstrapMin\";s:25:\"theme/js/bootstrap.min.js\";s:8:\"slickMin\";s:21:\"theme/js/slick.min.js\";s:19:\"purecounter_vanilla\";s:31:\"theme/js/purecounter_vanilla.js\";s:8:\"masonary\";s:20:\"theme/js/macy.min.js\";}s:7:\"typeNum\";s:1:\"0\";s:16:\"includeJSFooter.\";a:1:{s:6:\"atomJs\";s:18:\"theme/js/bundle.js\";}s:11:\"headerData.\";a:2:{i:1568907945;s:4:\"TEXT\";s:11:\"1568907945.\";a:1:{s:5:\"value\";s:140:\"              <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n              <meta name=\"theme-color\" content=\"#035073\">\";}}i:10;s:13:\"FLUIDTEMPLATE\";s:3:\"10.\";a:4:{s:12:\"templateName\";s:4:\"TEXT\";s:13:\"templateName.\";a:3:{s:7:\"cObject\";s:4:\"TEXT\";s:8:\"cObject.\";a:4:{s:4:\"data\";s:10:\"pagelayout\";s:8:\"required\";s:1:\"1\";s:4:\"case\";s:14:\"uppercamelcase\";s:6:\"split.\";a:3:{s:5:\"token\";s:8:\"pagets__\";s:7:\"cObjNum\";s:1:\"1\";s:2:\"1.\";a:1:{s:7:\"current\";s:1:\"1\";}}}s:7:\"ifEmpty\";s:7:\"Default\";}s:18:\"templateRootPaths.\";a:1:{i:0;s:62:\"EXT:twoh_template_override_x/Resources/Private/Templates/Page/\";}s:17:\"partialRootPaths.\";a:1:{i:0;s:61:\"EXT:twoh_template_override_x/Resources/Private/Partials/Page/\";}}}s:15:\"tx_solr_suggest\";s:4:\"PAGE\";s:16:\"tx_solr_suggest.\";a:4:{s:7:\"typeNum\";s:4:\"7384\";s:7:\"config.\";a:6:{s:20:\"disableAllHeaderCode\";s:1:\"1\";s:14:\"xhtml_cleaning\";s:1:\"0\";s:8:\"admPanel\";s:1:\"0\";s:18:\"additionalHeaders.\";a:1:{s:3:\"10.\";a:1:{s:6:\"header\";s:30:\"Content-type: application/json\";}}s:8:\"no_cache\";s:1:\"0\";s:5:\"debug\";s:1:\"0\";}i:10;s:4:\"USER\";s:3:\"10.\";a:6:{s:8:\"userFunc\";s:37:\"TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run\";s:13:\"extensionName\";s:4:\"Solr\";s:10:\"pluginName\";s:10:\"pi_suggest\";s:10:\"vendorName\";s:18:\"ApacheSolrForTypo3\";s:10:\"controller\";s:7:\"Suggest\";s:6:\"action\";s:7:\"suggest\";}}s:4:\"tmp.\";a:1:{s:8:\"tx_solr.\";a:1:{s:7:\"search.\";a:1:{s:8:\"sorting.\";a:1:{s:8:\"options.\";a:3:{s:8:\"created.\";a:3:{s:5:\"field\";s:7:\"created\";s:5:\"label\";s:13:\"Creation Date\";s:6:\"label.\";a:1:{s:5:\"lang.\";a:1:{s:2:\"de\";s:16:\"Erstellungsdatum\";}}}s:6:\"title.\";a:3:{s:5:\"field\";s:9:\"sortTitle\";s:5:\"label\";s:5:\"Title\";s:6:\"label.\";a:1:{s:5:\"lang.\";a:1:{s:2:\"de\";s:5:\"Titel\";}}}s:10:\"relevance.\";a:3:{s:5:\"field\";s:9:\"relevance\";s:5:\"label\";s:9:\"Relevance\";s:6:\"label.\";a:1:{s:5:\"lang.\";a:1:{s:2:\"de\";s:8:\"Relevanz\";}}}}}}}}s:6:\"types.\";a:3:{i:1533906435;s:11:\"seo_sitemap\";i:0;s:4:\"page\";i:7384;s:15:\"tx_solr_suggest\";}}');
/*!40000 ALTER TABLE `cache_tx_solr_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_tx_solr_configuration_tags`
--

DROP TABLE IF EXISTS `cache_tx_solr_configuration_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_tx_solr_configuration_tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_tx_solr_configuration_tags`
--

LOCK TABLES `cache_tx_solr_configuration_tags` WRITE;
/*!40000 ALTER TABLE `cache_tx_solr_configuration_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_tx_solr_configuration_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_tx_solr_tags`
--

DROP TABLE IF EXISTS `cache_tx_solr_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_tx_solr_tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_tx_solr_tags`
--

LOCK TABLES `cache_tx_solr_tags` WRITE;
/*!40000 ALTER TABLE `cache_tx_solr_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_tx_solr_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_vhs_main`
--

DROP TABLE IF EXISTS `cache_vhs_main`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_vhs_main` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `expires` int unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_vhs_main`
--

LOCK TABLES `cache_vhs_main` WRITE;
/*!40000 ALTER TABLE `cache_vhs_main` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_vhs_main` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_vhs_main_tags`
--

DROP TABLE IF EXISTS `cache_vhs_main_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_vhs_main_tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_vhs_main_tags`
--

LOCK TABLES `cache_vhs_main_tags` WRITE;
/*!40000 ALTER TABLE `cache_vhs_main_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_vhs_main_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_vhs_markdown`
--

DROP TABLE IF EXISTS `cache_vhs_markdown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_vhs_markdown` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `expires` int unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_vhs_markdown`
--

LOCK TABLES `cache_vhs_markdown` WRITE;
/*!40000 ALTER TABLE `cache_vhs_markdown` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_vhs_markdown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_vhs_markdown_tags`
--

DROP TABLE IF EXISTS `cache_vhs_markdown_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_vhs_markdown_tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_vhs_markdown_tags`
--

LOCK TABLES `cache_vhs_markdown_tags` WRITE;
/*!40000 ALTER TABLE `cache_vhs_markdown_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_vhs_markdown_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_workspaces_cache`
--

DROP TABLE IF EXISTS `cache_workspaces_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_workspaces_cache` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `expires` int unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_workspaces_cache`
--

LOCK TABLES `cache_workspaces_cache` WRITE;
/*!40000 ALTER TABLE `cache_workspaces_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_workspaces_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_workspaces_cache_tags`
--

DROP TABLE IF EXISTS `cache_workspaces_cache_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_workspaces_cache_tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_workspaces_cache_tags`
--

LOCK TABLES `cache_workspaces_cache_tags` WRITE;
/*!40000 ALTER TABLE `cache_workspaces_cache_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_workspaces_cache_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_yoastseo_recordcache`
--

DROP TABLE IF EXISTS `cache_yoastseo_recordcache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_yoastseo_recordcache` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `expires` int unsigned NOT NULL DEFAULT '0',
  `content` longblob,
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(180),`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_yoastseo_recordcache`
--

LOCK TABLES `cache_yoastseo_recordcache` WRITE;
/*!40000 ALTER TABLE `cache_yoastseo_recordcache` DISABLE KEYS */;
INSERT INTO `cache_yoastseo_recordcache` VALUES (1,'records',1710611142,_binary 'a:0:{}');
/*!40000 ALTER TABLE `cache_yoastseo_recordcache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache_yoastseo_recordcache_tags`
--

DROP TABLE IF EXISTS `cache_yoastseo_recordcache_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_yoastseo_recordcache_tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cache_id` (`identifier`(191)),
  KEY `cache_tag` (`tag`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cache_yoastseo_recordcache_tags`
--

LOCK TABLES `cache_yoastseo_recordcache_tags` WRITE;
/*!40000 ALTER TABLE `cache_yoastseo_recordcache_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache_yoastseo_recordcache_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fe_groups`
--

DROP TABLE IF EXISTS `fe_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fe_groups` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `tx_extbase_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `subgroup` tinytext COLLATE utf8mb4_unicode_ci,
  `TSconfig` text COLLATE utf8mb4_unicode_ci,
  `felogin_redirectPid` tinytext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fe_groups`
--

LOCK TABLES `fe_groups` WRITE;
/*!40000 ALTER TABLE `fe_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `fe_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fe_sessions`
--

DROP TABLE IF EXISTS `fe_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fe_sessions` (
  `ses_id` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ses_iplock` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ses_userid` int unsigned NOT NULL DEFAULT '0',
  `ses_tstamp` int unsigned NOT NULL DEFAULT '0',
  `ses_data` mediumblob,
  `ses_permanent` smallint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ses_id`),
  KEY `ses_tstamp` (`ses_tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fe_sessions`
--

LOCK TABLES `fe_sessions` WRITE;
/*!40000 ALTER TABLE `fe_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `fe_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fe_users`
--

DROP TABLE IF EXISTS `fe_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fe_users` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `disable` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `tx_extbase_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `lastlogin` int NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `usergroup` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(160) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `middle_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `telephone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `uc` blob,
  `title` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `www` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `company` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `image` tinytext COLLATE utf8mb4_unicode_ci,
  `TSconfig` text COLLATE utf8mb4_unicode_ci,
  `is_online` int unsigned NOT NULL DEFAULT '0',
  `mfa` mediumblob,
  `felogin_redirectPid` tinytext COLLATE utf8mb4_unicode_ci,
  `felogin_forgotHash` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`username`(100)),
  KEY `username` (`username`(100)),
  KEY `is_online` (`is_online`),
  KEY `felogin_forgotHash` (`felogin_forgotHash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fe_users`
--

LOCK TABLES `fe_users` WRITE;
/*!40000 ALTER TABLE `fe_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `fe_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pages` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `fe_group` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `rowDescription` text COLLATE utf8mb4_unicode_ci,
  `editlock` smallint unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int unsigned NOT NULL DEFAULT '0',
  `l10n_source` int unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `t3_origuid` int unsigned NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `perms_userid` int unsigned NOT NULL DEFAULT '0',
  `perms_groupid` int unsigned NOT NULL DEFAULT '0',
  `perms_user` smallint unsigned NOT NULL DEFAULT '0',
  `perms_group` smallint unsigned NOT NULL DEFAULT '0',
  `perms_everybody` smallint unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `doktype` int unsigned NOT NULL DEFAULT '0',
  `TSconfig` text COLLATE utf8mb4_unicode_ci,
  `is_siteroot` smallint NOT NULL DEFAULT '0',
  `php_tree_stop` smallint NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `shortcut` int unsigned NOT NULL DEFAULT '0',
  `shortcut_mode` int unsigned NOT NULL DEFAULT '0',
  `subtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `layout` int unsigned NOT NULL DEFAULT '0',
  `target` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `media` int unsigned NOT NULL DEFAULT '0',
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `cache_timeout` int unsigned NOT NULL DEFAULT '0',
  `cache_tags` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci,
  `no_search` smallint unsigned NOT NULL DEFAULT '0',
  `SYS_LASTCHANGED` int unsigned NOT NULL DEFAULT '0',
  `abstract` text COLLATE utf8mb4_unicode_ci,
  `module` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extendToSubpages` smallint unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `author_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `nav_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `nav_hide` smallint NOT NULL DEFAULT '0',
  `content_from_pid` int unsigned NOT NULL DEFAULT '0',
  `mount_pid` int unsigned NOT NULL DEFAULT '0',
  `mount_pid_ol` smallint NOT NULL DEFAULT '0',
  `l18n_cfg` smallint NOT NULL DEFAULT '0',
  `backend_layout` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `backend_layout_next_level` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tsconfig_includes` text COLLATE utf8mb4_unicode_ci,
  `categories` int unsigned NOT NULL DEFAULT '0',
  `lastUpdated` int NOT NULL DEFAULT '0',
  `newUntil` int NOT NULL DEFAULT '0',
  `slug` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tx_impexp_origuid` int NOT NULL DEFAULT '0',
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `no_index` smallint NOT NULL DEFAULT '0',
  `no_follow` smallint NOT NULL DEFAULT '0',
  `og_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `og_description` text COLLATE utf8mb4_unicode_ci,
  `og_image` int unsigned NOT NULL DEFAULT '0',
  `twitter_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `twitter_description` text COLLATE utf8mb4_unicode_ci,
  `twitter_image` int unsigned NOT NULL DEFAULT '0',
  `twitter_card` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `canonical_link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sitemap_priority` decimal(2,1) NOT NULL DEFAULT '0.5',
  `sitemap_changefreq` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tx_yoastseo_focuskeyword` tinytext COLLATE utf8mb4_unicode_ci,
  `tx_yoastseo_focuskeyword_synonyms` tinytext COLLATE utf8mb4_unicode_ci,
  `tx_yoastseo_focuskeyword_related` int NOT NULL DEFAULT '0',
  `tx_yoastseo_hide_snippet_preview` smallint NOT NULL DEFAULT '0',
  `tx_yoastseo_cornerstone` smallint NOT NULL DEFAULT '0',
  `tx_yoastseo_score_readability` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tx_yoastseo_score_seo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tx_yoastseo_robots_noimageindex` smallint NOT NULL DEFAULT '0',
  `tx_yoastseo_robots_noarchive` smallint NOT NULL DEFAULT '0',
  `tx_yoastseo_robots_nosnippet` smallint NOT NULL DEFAULT '0',
  `no_search_sub_entries` smallint unsigned NOT NULL DEFAULT '0',
  `tx_staticfilecache_cache` smallint DEFAULT '1',
  `tx_staticfilecache_cache_force` smallint DEFAULT '0',
  `tx_staticfilecache_cache_offline` smallint DEFAULT '0',
  `tx_staticfilecache_cache_priority` int NOT NULL DEFAULT '0',
  `link_by_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `gradient` int NOT NULL DEFAULT '0',
  `do_not_link` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `determineSiteRoot` (`is_siteroot`),
  KEY `language_identifier` (`l10n_parent`,`sys_language_uid`),
  KEY `slug` (`slug`(127)),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `translation_source` (`l10n_source`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `tx_yoastseo_cornerstone` (`tx_yoastseo_cornerstone`),
  KEY `content_from_pid_deleted` (`content_from_pid`,`deleted`),
  KEY `doktype_no_search_deleted` (`doktype`,`no_search`,`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,0,1710605878,1710605699,0,0,0,0,'',256,NULL,0,0,0,0,NULL,0,_binary '{\"doktype\":\"\",\"title\":\"\",\"slug\":\"\",\"nav_title\":\"\",\"subtitle\":\"\",\"gradient\":\"\",\"do_not_link\":\"\",\"seo_title\":\"\",\"tx_yoastseo_cornerstone\":\"\",\"description\":\"\",\"tx_yoastseo_focuskeyword\":\"\",\"tx_yoastseo_focuskeyword_synonyms\":\"\",\"tx_yoastseo_focuskeyword_related\":\"\",\"no_index\":\"\",\"no_follow\":\"\",\"tx_yoastseo_robots_noimageindex\":\"\",\"tx_yoastseo_robots_noarchive\":\"\",\"tx_yoastseo_robots_nosnippet\":\"\",\"canonical_link\":\"\",\"sitemap_changefreq\":\"\",\"sitemap_priority\":\"\",\"og_title\":\"\",\"og_description\":\"\",\"og_image\":\"\",\"twitter_title\":\"\",\"twitter_description\":\"\",\"twitter_image\":\"\",\"twitter_card\":\"\",\"tx_yoastseo_hide_snippet_preview\":\"\",\"abstract\":\"\",\"keywords\":\"\",\"author\":\"\",\"author_email\":\"\",\"lastUpdated\":\"\",\"layout\":\"\",\"newUntil\":\"\",\"backend_layout\":\"\",\"backend_layout_next_level\":\"\",\"content_from_pid\":\"\",\"target\":\"\",\"cache_timeout\":\"\",\"cache_tags\":\"\",\"tx_staticfilecache_cache\":\"\",\"tx_staticfilecache_cache_force\":\"\",\"tx_staticfilecache_cache_offline\":\"\",\"tx_staticfilecache_cache_priority\":\"\",\"is_siteroot\":\"\",\"no_search\":\"\",\"no_search_sub_entries\":\"\",\"php_tree_stop\":\"\",\"module\":\"\",\"media\":\"\",\"tsconfig_includes\":\"\",\"TSconfig\":\"\",\"l18n_cfg\":\"\",\"hidden\":\"\",\"nav_hide\":\"\",\"starttime\":\"\",\"endtime\":\"\",\"extendToSubpages\":\"\",\"fe_group\":\"\",\"editlock\":\"\",\"categories\":\"\",\"rowDescription\":\"\"}',0,0,0,0,1,0,31,27,0,'Home',1,NULL,1,0,'',0,0,'',0,'',0,NULL,0,'',NULL,0,1710608286,NULL,'',0,'','','',0,0,0,0,0,'pagets__atom_base','pagets__atom_subpages',NULL,0,0,0,'/',0,'',0,0,'',NULL,0,'',NULL,0,'summary','',0.5,'',NULL,NULL,0,0,0,'bad','bad',0,0,0,0,1,0,0,0,'',0,0),(2,1,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'About Us',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/about-us',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0),(3,2,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'Our Team',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/about-us/our-team',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0),(4,2,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'Mission and Vision',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/about-us/mission-and-vision',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0),(5,1,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'Services',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/services',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0),(6,5,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'Web Development',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/services/web-development',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0),(7,5,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'SEO Optimization',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/services/seo-optimization',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0),(8,5,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'Mobile App Development',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/services/mobile-app-development',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0),(9,1,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'Portfolio',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/portfolio',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0),(10,9,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'Project 1',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/portfolio/project-1',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0),(11,9,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'Project 2',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/portfolio/project-2',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0),(12,9,1710607630,0,0,0,0,0,'0',0,NULL,0,0,0,0,NULL,0,NULL,0,0,0,0,1,0,31,27,0,'Project 3',1,NULL,0,0,'',0,0,'',0,'',0,NULL,0,'','',0,0,NULL,'',0,'','','',0,0,0,0,0,'','',NULL,0,0,0,'/portfolio/project-3',0,'',0,0,'',NULL,0,'',NULL,0,'','',0.5,'',NULL,NULL,0,0,0,'','',0,0,0,0,1,0,0,0,'',0,0);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_be_shortcuts`
--

DROP TABLE IF EXISTS `sys_be_shortcuts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_be_shortcuts` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `userid` int unsigned NOT NULL DEFAULT '0',
  `route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `arguments` text COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sorting` int NOT NULL DEFAULT '0',
  `sc_group` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_be_shortcuts`
--

LOCK TABLES `sys_be_shortcuts` WRITE;
/*!40000 ALTER TABLE `sys_be_shortcuts` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_be_shortcuts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_category`
--

DROP TABLE IF EXISTS `sys_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_category` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `t3_origuid` int unsigned NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `items` int NOT NULL DEFAULT '0',
  `parent` int unsigned NOT NULL DEFAULT '0',
  `fe_group` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `images` int unsigned DEFAULT '0',
  `single_pid` int unsigned NOT NULL DEFAULT '0',
  `shortcut` int NOT NULL DEFAULT '0',
  `import_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `import_source` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_headline` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `seo_text` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `category_parent` (`parent`),
  KEY `category_list` (`pid`,`deleted`,`sys_language_uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `import` (`import_id`,`import_source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_category`
--

LOCK TABLES `sys_category` WRITE;
/*!40000 ALTER TABLE `sys_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_category_record_mm`
--

DROP TABLE IF EXISTS `sys_category_record_mm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_category_record_mm` (
  `uid_local` int unsigned NOT NULL DEFAULT '0',
  `uid_foreign` int unsigned NOT NULL DEFAULT '0',
  `tablenames` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fieldname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sorting` int unsigned NOT NULL DEFAULT '0',
  `sorting_foreign` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid_local`,`uid_foreign`,`tablenames`,`fieldname`),
  KEY `uid_local` (`uid_local`),
  KEY `uid_foreign` (`uid_foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_category_record_mm`
--

LOCK TABLES `sys_category_record_mm` WRITE;
/*!40000 ALTER TABLE `sys_category_record_mm` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_category_record_mm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_csp_resolution`
--

DROP TABLE IF EXISTS `sys_csp_resolution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_csp_resolution` (
  `summary` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` int unsigned NOT NULL,
  `scope` varchar(264) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mutation_identifier` text COLLATE utf8mb4_unicode_ci,
  `mutation_collection` mediumtext COLLATE utf8mb4_unicode_ci,
  `meta` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`summary`),
  KEY `created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_csp_resolution`
--

LOCK TABLES `sys_csp_resolution` WRITE;
/*!40000 ALTER TABLE `sys_csp_resolution` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_csp_resolution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file`
--

DROP TABLE IF EXISTS `sys_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_file` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `last_indexed` int NOT NULL DEFAULT '0',
  `missing` smallint NOT NULL DEFAULT '0',
  `storage` int NOT NULL DEFAULT '0',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `metadata` int NOT NULL DEFAULT '0',
  `identifier` text COLLATE utf8mb4_unicode_ci,
  `identifier_hash` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `folder_hash` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` tinytext COLLATE utf8mb4_unicode_ci,
  `sha1` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `size` bigint unsigned NOT NULL DEFAULT '0',
  `creation_date` int NOT NULL DEFAULT '0',
  `modification_date` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `sel01` (`storage`,`identifier_hash`),
  KEY `folder` (`storage`,`folder_hash`),
  KEY `tstamp` (`tstamp`),
  KEY `lastindex` (`last_indexed`),
  KEY `sha1` (`sha1`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file`
--

LOCK TABLES `sys_file` WRITE;
/*!40000 ALTER TABLE `sys_file` DISABLE KEYS */;
INSERT INTO `sys_file` VALUES (1,0,1710608000,1710608000,0,1,'1',0,'/user_upload/_temp_/importexport/index.html','68614dc2826769e93d8a8ead62af30ac99aaa83a','0795cf796b4fc959be0ec00b183c0f47609dd9a5','html','text/html','index.html','86f257eae12059ed5daf12ce6e5508f5f893012a',116,1710605419,1710605419),(2,0,1710608002,1710608002,0,1,'5',0,'/user_upload/index.html','c25533f303185517ca3e1e24b215d53aa74076d2','19669f1e02c2f16705ec7587044c66443be70725','html','application/x-empty','index.html','da39a3ee5e6b4b0d3255bfef95601890afd80709',0,1710605419,1710605419),(3,0,1710608006,1710608006,0,1,'1',0,'/form_definitions/kontakt.form.yaml','f2a71771996e8c284f189dd2ef66b053becf993d','c62e3e70a526a59f0f0b7687864947eab72d7d3f','yaml','text/plain','kontakt.form.yaml','988a90a7369889bdbcb7761604559c82518649d7',1798,1710603158,1710603158),(4,0,1710608091,1710608091,0,1,'2',0,'/user_upload/ai-images/typo3-kickstarter-build-your-website-fast.jpg','84ad4847461a2d883b7dd0d07dc9313b655ef273','e3f01781bb4eaeb44120a81d41ee4663fe91fc08','jpg','image/png','typo3-kickstarter-build-your-website-fast.jpg','d49d2a8b2bd2d69321f42d6cdeef9b09d0fcf528',3162696,1710608091,1710608091);
/*!40000 ALTER TABLE `sys_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_collection`
--

DROP TABLE IF EXISTS `sys_file_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_file_collection` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `t3_origuid` int unsigned NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8mb4_unicode_ci,
  `type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'static',
  `files` int NOT NULL DEFAULT '0',
  `folder_identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `recursive` smallint NOT NULL DEFAULT '0',
  `category` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_collection`
--

LOCK TABLES `sys_file_collection` WRITE;
/*!40000 ALTER TABLE `sys_file_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_file_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_metadata`
--

DROP TABLE IF EXISTS `sys_file_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_file_metadata` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `t3_origuid` int unsigned NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `file` int NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8mb4_unicode_ci,
  `width` int NOT NULL DEFAULT '0',
  `height` int NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `alternative` text COLLATE utf8mb4_unicode_ci,
  `categories` int unsigned NOT NULL DEFAULT '0',
  `content_creation_date` int NOT NULL DEFAULT '0',
  `content_modification_date` int NOT NULL DEFAULT '0',
  `visible` int unsigned DEFAULT '1',
  `status` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `caption` text COLLATE utf8mb4_unicode_ci,
  `creator_tool` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `download_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `creator` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `publisher` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `copyright` text COLLATE utf8mb4_unicode_ci,
  `location_country` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `location_region` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `location_city` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `latitude` decimal(24,14) DEFAULT '0.00000000000000',
  `longitude` decimal(24,14) DEFAULT '0.00000000000000',
  `ranking` int unsigned DEFAULT '0',
  `note` text COLLATE utf8mb4_unicode_ci,
  `unit` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `duration` double DEFAULT '0',
  `color_space` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `pages` int unsigned DEFAULT '0',
  `language` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `fe_groups` tinytext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `file` (`file`),
  KEY `fal_filelist` (`l10n_parent`,`sys_language_uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_metadata`
--

LOCK TABLES `sys_file_metadata` WRITE;
/*!40000 ALTER TABLE `sys_file_metadata` DISABLE KEYS */;
INSERT INTO `sys_file_metadata` VALUES (1,0,1710608000,1710608000,0,0,NULL,0,'',0,0,0,0,1,NULL,0,0,NULL,NULL,0,0,0,1,'',NULL,NULL,'','','','','',NULL,'','','',0.00000000000000,0.00000000000000,0,NULL,'',0,'',0,'',NULL),(2,0,1710608001,1710608001,0,0,NULL,0,'',0,0,0,0,2,NULL,0,0,NULL,NULL,0,0,0,1,'',NULL,NULL,'','','','','',NULL,'','','',0.00000000000000,0.00000000000000,0,NULL,'',0,'',0,'',NULL),(3,0,1710608006,1710608006,0,0,NULL,0,'',0,0,0,0,3,NULL,0,0,NULL,NULL,0,0,0,1,'',NULL,NULL,'','','','','',NULL,'','','',0.00000000000000,0.00000000000000,0,NULL,'',0,'',0,'',NULL),(4,0,1710608088,1710608088,0,0,NULL,0,'',0,0,0,0,4,NULL,1024,1024,NULL,NULL,0,0,0,1,'',NULL,NULL,'','','','','',NULL,'','','',0.00000000000000,0.00000000000000,0,NULL,'',0,'',0,'',NULL);
/*!40000 ALTER TABLE `sys_file_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_processedfile`
--

DROP TABLE IF EXISTS `sys_file_processedfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_file_processedfile` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `tstamp` int NOT NULL DEFAULT '0',
  `crdate` int NOT NULL DEFAULT '0',
  `storage` int NOT NULL DEFAULT '0',
  `original` int NOT NULL DEFAULT '0',
  `identifier` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` tinytext COLLATE utf8mb4_unicode_ci,
  `processing_url` text COLLATE utf8mb4_unicode_ci,
  `configuration` blob,
  `configurationsha1` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `originalfilesha1` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `task_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `checksum` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `width` int DEFAULT '0',
  `height` int DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `combined_1` (`original`,`task_type`(100),`configurationsha1`),
  KEY `identifier` (`storage`,`identifier`(180))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_processedfile`
--

LOCK TABLES `sys_file_processedfile` WRITE;
/*!40000 ALTER TABLE `sys_file_processedfile` DISABLE KEYS */;
INSERT INTO `sys_file_processedfile` VALUES (1,1710608097,1710608091,1,4,'/_processed_/7/d/csm_typo3-kickstarter-build-your-website-fast_6380f4626c.jpg','csm_typo3-kickstarter-build-your-website-fast_6380f4626c.jpg','',_binary 'a:7:{s:5:\"width\";N;s:6:\"height\";N;s:8:\"minWidth\";N;s:9:\"minHeight\";N;s:8:\"maxWidth\";N;s:9:\"maxHeight\";i:150;s:4:\"crop\";N;}','cfe93cbb07d0cfbe7800b799777ad2e70305dbab','d49d2a8b2bd2d69321f42d6cdeef9b09d0fcf528','Image.CropScaleMask','6380f4626c',150,150),(2,1710608092,1710608091,1,4,'/_processed_/7/d/csm_typo3-kickstarter-build-your-website-fast_bf5794004a.jpg','csm_typo3-kickstarter-build-your-website-fast_bf5794004a.jpg','',_binary 'a:3:{s:8:\"maxWidth\";i:145;s:9:\"maxHeight\";i:45;s:6:\"height\";s:3:\"45m\";}','99641ee1e111db8018e526d3105ad5631f8f88a4','d49d2a8b2bd2d69321f42d6cdeef9b09d0fcf528','Image.CropScaleMask','bf5794004a',45,45),(3,1710608101,1710608101,1,4,'',NULL,'',_binary 'a:7:{s:5:\"width\";N;s:6:\"height\";N;s:8:\"minWidth\";N;s:9:\"minHeight\";N;s:8:\"maxWidth\";N;s:9:\"maxHeight\";N;s:4:\"crop\";N;}','24f48d5b4de7d99b7144e6559156976855e74b5d','d49d2a8b2bd2d69321f42d6cdeef9b09d0fcf528','Image.CropScaleMask','5075322532',0,0);
/*!40000 ALTER TABLE `sys_file_processedfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_reference`
--

DROP TABLE IF EXISTS `sys_file_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_file_reference` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `uid_local` int NOT NULL DEFAULT '0',
  `uid_foreign` int NOT NULL DEFAULT '0',
  `tablenames` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fieldname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sorting_foreign` int NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `alternative` text COLLATE utf8mb4_unicode_ci,
  `link` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `crop` varchar(4000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `autoplay` smallint NOT NULL DEFAULT '0',
  `showinpreview` smallint NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `tablenames_fieldname` (`tablenames`(32),`fieldname`(12)),
  KEY `deleted` (`deleted`),
  KEY `uid_local` (`uid_local`),
  KEY `uid_foreign` (`uid_foreign`),
  KEY `combined_1` (`l10n_parent`,`t3ver_oid`,`t3ver_wsid`,`t3ver_state`,`deleted`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_reference`
--

LOCK TABLES `sys_file_reference` WRITE;
/*!40000 ALTER TABLE `sys_file_reference` DISABLE KEYS */;
INSERT INTO `sys_file_reference` VALUES (1,1,1710608096,1710608096,0,0,0,0,NULL,'',0,0,0,0,4,3,'tt_content','media',1,NULL,NULL,NULL,'','{\"desktop\":{\"cropArea\":{\"x\":0,\"y\":0,\"width\":1,\"height\":1},\"selectedRatio\":\"NaN\",\"focusArea\":null},\"tablet\":{\"cropArea\":{\"x\":0,\"y\":0,\"width\":1,\"height\":1},\"selectedRatio\":\"NaN\",\"focusArea\":null},\"mobile\":{\"cropArea\":{\"x\":0,\"y\":0,\"width\":1,\"height\":1},\"selectedRatio\":\"NaN\",\"focusArea\":null}}',0,0);
/*!40000 ALTER TABLE `sys_file_reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_file_storage`
--

DROP TABLE IF EXISTS `sys_file_storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_file_storage` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `driver` tinytext COLLATE utf8mb4_unicode_ci,
  `configuration` text COLLATE utf8mb4_unicode_ci,
  `is_default` smallint NOT NULL DEFAULT '0',
  `is_browsable` smallint NOT NULL DEFAULT '0',
  `is_public` smallint NOT NULL DEFAULT '0',
  `is_writable` smallint NOT NULL DEFAULT '0',
  `is_online` smallint NOT NULL DEFAULT '1',
  `auto_extract_metadata` smallint NOT NULL DEFAULT '1',
  `processingfolder` tinytext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_file_storage`
--

LOCK TABLES `sys_file_storage` WRITE;
/*!40000 ALTER TABLE `sys_file_storage` DISABLE KEYS */;
INSERT INTO `sys_file_storage` VALUES (1,0,1710605392,1710605392,0,'This is the local fileadmin/ directory. This storage mount has been created automatically by TYPO3.','fileadmin','Local','<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\" ?>\n<T3FlexForms>\n    <data>\n        <sheet index=\"sDEF\">\n            <language index=\"lDEF\">\n                <field index=\"basePath\">\n                    <value index=\"vDEF\">fileadmin/</value>\n                </field>\n                <field index=\"pathType\">\n                    <value index=\"vDEF\">relative</value>\n                </field>\n                <field index=\"caseSensitive\">\n                    <value index=\"vDEF\">1</value>\n                </field>\n            </language>\n        </sheet>\n    </data>\n</T3FlexForms>',1,1,1,1,1,1,NULL);
/*!40000 ALTER TABLE `sys_file_storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_filemounts`
--

DROP TABLE IF EXISTS `sys_filemounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_filemounts` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `read_only` smallint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_filemounts`
--

LOCK TABLES `sys_filemounts` WRITE;
/*!40000 ALTER TABLE `sys_filemounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_filemounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_history`
--

DROP TABLE IF EXISTS `sys_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_history` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `actiontype` smallint NOT NULL DEFAULT '0',
  `usertype` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'BE',
  `userid` int unsigned DEFAULT NULL,
  `originaluserid` int unsigned DEFAULT NULL,
  `recuid` int NOT NULL DEFAULT '0',
  `tablename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `history_data` mediumtext COLLATE utf8mb4_unicode_ci,
  `workspace` int DEFAULT '0',
  `correlation_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `recordident_1` (`tablename`(100),`recuid`),
  KEY `recordident_2` (`tablename`(100),`tstamp`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_history`
--

LOCK TABLES `sys_history` WRITE;
/*!40000 ALTER TABLE `sys_history` DISABLE KEYS */;
INSERT INTO `sys_history` VALUES (1,1710605699,1,'BE',1,0,1,'pages','{\"uid\":1,\"pid\":0,\"tstamp\":1710605699,\"crdate\":1710605699,\"deleted\":0,\"hidden\":1,\"starttime\":0,\"endtime\":0,\"fe_group\":\"0\",\"sorting\":256,\"rowDescription\":null,\"editlock\":0,\"sys_language_uid\":0,\"l10n_parent\":0,\"l10n_source\":0,\"l10n_state\":null,\"t3_origuid\":0,\"l10n_diffsource\":\"\",\"t3ver_oid\":0,\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"perms_userid\":1,\"perms_groupid\":0,\"perms_user\":31,\"perms_group\":27,\"perms_everybody\":0,\"title\":\"Home\",\"doktype\":1,\"TSconfig\":null,\"is_siteroot\":0,\"php_tree_stop\":0,\"url\":\"\",\"shortcut\":0,\"shortcut_mode\":0,\"subtitle\":\"\",\"layout\":0,\"target\":\"\",\"media\":0,\"keywords\":null,\"cache_timeout\":0,\"cache_tags\":\"\",\"description\":null,\"no_search\":0,\"SYS_LASTCHANGED\":0,\"abstract\":null,\"module\":\"\",\"extendToSubpages\":0,\"author\":\"\",\"author_email\":\"\",\"nav_title\":\"\",\"nav_hide\":0,\"content_from_pid\":0,\"mount_pid\":0,\"mount_pid_ol\":0,\"l18n_cfg\":0,\"backend_layout\":\"\",\"backend_layout_next_level\":\"\",\"tsconfig_includes\":null,\"categories\":0,\"lastUpdated\":0,\"newUntil\":0,\"slug\":\"\\/\",\"tx_impexp_origuid\":0,\"seo_title\":\"\",\"no_index\":0,\"no_follow\":0,\"og_title\":\"\",\"og_description\":null,\"og_image\":0,\"twitter_title\":\"\",\"twitter_description\":null,\"twitter_image\":0,\"twitter_card\":\"summary\",\"canonical_link\":\"\",\"sitemap_priority\":\"0.5\",\"sitemap_changefreq\":\"\",\"tx_yoastseo_focuskeyword\":null,\"tx_yoastseo_focuskeyword_synonyms\":null,\"tx_yoastseo_focuskeyword_related\":0,\"tx_yoastseo_hide_snippet_preview\":0,\"tx_yoastseo_cornerstone\":0,\"tx_yoastseo_score_readability\":\"\",\"tx_yoastseo_score_seo\":\"\",\"tx_yoastseo_robots_noimageindex\":0,\"tx_yoastseo_robots_noarchive\":0,\"tx_yoastseo_robots_nosnippet\":0,\"no_search_sub_entries\":0,\"tx_staticfilecache_cache\":1,\"tx_staticfilecache_cache_force\":0,\"tx_staticfilecache_cache_offline\":0,\"tx_staticfilecache_cache_priority\":0,\"link_by_type\":\"\",\"gradient\":0,\"do_not_link\":0}',0,'0400$1178a9c55d5701a160d831034f69a170:e175f7045d7ccbfb26ffcf279422c2e5'),(2,1710605709,1,'BE',1,0,1,'sys_template','{\"uid\":1,\"pid\":1,\"tstamp\":1710605709,\"crdate\":1710605709,\"deleted\":0,\"hidden\":0,\"starttime\":0,\"endtime\":0,\"sorting\":256,\"description\":null,\"t3_origuid\":0,\"title\":\"NEW SITE\",\"root\":1,\"clear\":3,\"include_static_file\":null,\"constants\":null,\"config\":\"\\n# Default PAGE object:\\npage = PAGE\\npage.10 = TEXT\\npage.10.value = HELLO WORLD!\\n\",\"basedOn\":\"\",\"includeStaticAfterBasedOn\":0,\"static_file_mode\":0,\"tx_impexp_origuid\":0}',0,'0400$c87d742bfaeb26ec42f35bcea8112629:35af6288617af54964e77af08c30949a'),(3,1710605733,2,'BE',1,0,1,'sys_template','{\"oldRecord\":{\"title\":\"NEW SITE\",\"config\":\"\\n# Default PAGE object:\\npage = PAGE\\npage.10 = TEXT\\npage.10.value = HELLO WORLD!\\n\",\"include_static_file\":null},\"newRecord\":{\"title\":\"TWOH Kickstarter\",\"config\":\"\",\"include_static_file\":\"EXT:twoh_kickstarter\\/Configuration\\/TypoScript,EXT:twoh_template_override_x\\/Configuration\\/TypoScript\"}}',0,'0400$c2f2c546b2a13e986c872109cccdeb5b:35af6288617af54964e77af08c30949a'),(4,1710605757,2,'BE',1,0,1,'pages','{\"oldRecord\":{\"is_siteroot\":0,\"hidden\":1,\"fe_group\":\"0\",\"l10n_diffsource\":\"\"},\"newRecord\":{\"is_siteroot\":\"1\",\"hidden\":\"0\",\"fe_group\":\"\",\"l10n_diffsource\":\"{\\\"doktype\\\":\\\"\\\",\\\"title\\\":\\\"\\\",\\\"slug\\\":\\\"\\\",\\\"nav_title\\\":\\\"\\\",\\\"subtitle\\\":\\\"\\\",\\\"gradient\\\":\\\"\\\",\\\"do_not_link\\\":\\\"\\\",\\\"seo_title\\\":\\\"\\\",\\\"tx_yoastseo_cornerstone\\\":\\\"\\\",\\\"description\\\":\\\"\\\",\\\"tx_yoastseo_focuskeyword\\\":\\\"\\\",\\\"tx_yoastseo_focuskeyword_synonyms\\\":\\\"\\\",\\\"tx_yoastseo_focuskeyword_related\\\":\\\"\\\",\\\"no_index\\\":\\\"\\\",\\\"no_follow\\\":\\\"\\\",\\\"tx_yoastseo_robots_noimageindex\\\":\\\"\\\",\\\"tx_yoastseo_robots_noarchive\\\":\\\"\\\",\\\"tx_yoastseo_robots_nosnippet\\\":\\\"\\\",\\\"canonical_link\\\":\\\"\\\",\\\"sitemap_changefreq\\\":\\\"\\\",\\\"sitemap_priority\\\":\\\"\\\",\\\"og_title\\\":\\\"\\\",\\\"og_description\\\":\\\"\\\",\\\"og_image\\\":\\\"\\\",\\\"twitter_title\\\":\\\"\\\",\\\"twitter_description\\\":\\\"\\\",\\\"twitter_image\\\":\\\"\\\",\\\"twitter_card\\\":\\\"\\\",\\\"tx_yoastseo_hide_snippet_preview\\\":\\\"\\\",\\\"abstract\\\":\\\"\\\",\\\"keywords\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"author_email\\\":\\\"\\\",\\\"lastUpdated\\\":\\\"\\\",\\\"layout\\\":\\\"\\\",\\\"newUntil\\\":\\\"\\\",\\\"backend_layout\\\":\\\"\\\",\\\"backend_layout_next_level\\\":\\\"\\\",\\\"content_from_pid\\\":\\\"\\\",\\\"target\\\":\\\"\\\",\\\"cache_timeout\\\":\\\"\\\",\\\"cache_tags\\\":\\\"\\\",\\\"tx_staticfilecache_cache\\\":\\\"\\\",\\\"tx_staticfilecache_cache_force\\\":\\\"\\\",\\\"tx_staticfilecache_cache_offline\\\":\\\"\\\",\\\"tx_staticfilecache_cache_priority\\\":\\\"\\\",\\\"is_siteroot\\\":\\\"\\\",\\\"no_search\\\":\\\"\\\",\\\"no_search_sub_entries\\\":\\\"\\\",\\\"php_tree_stop\\\":\\\"\\\",\\\"module\\\":\\\"\\\",\\\"media\\\":\\\"\\\",\\\"tsconfig_includes\\\":\\\"\\\",\\\"TSconfig\\\":\\\"\\\",\\\"l18n_cfg\\\":\\\"\\\",\\\"hidden\\\":\\\"\\\",\\\"nav_hide\\\":\\\"\\\",\\\"starttime\\\":\\\"\\\",\\\"endtime\\\":\\\"\\\",\\\"extendToSubpages\\\":\\\"\\\",\\\"fe_group\\\":\\\"\\\",\\\"editlock\\\":\\\"\\\",\\\"categories\\\":\\\"\\\",\\\"rowDescription\\\":\\\"\\\"}\"}}',0,'0400$493c6b4163e783b5d46eb068a472e7df:e175f7045d7ccbfb26ffcf279422c2e5'),(5,1710605878,2,'BE',1,0,1,'pages','{\"oldRecord\":{\"backend_layout\":\"\",\"backend_layout_next_level\":\"\"},\"newRecord\":{\"backend_layout\":\"pagets__atom_base\",\"backend_layout_next_level\":\"pagets__atom_subpages\"}}',0,'0400$98da294622662d1c38dadbc1d7ff6e72:e175f7045d7ccbfb26ffcf279422c2e5'),(6,1710607821,1,'BE',1,0,1,'tt_content','{\"uid\":1,\"pid\":1,\"tstamp\":1710607821,\"crdate\":1710607821,\"deleted\":0,\"hidden\":0,\"starttime\":0,\"endtime\":0,\"fe_group\":\"\",\"sorting\":256,\"editlock\":0,\"sys_language_uid\":0,\"l18n_parent\":0,\"l10n_source\":0,\"l10n_state\":null,\"t3_origuid\":0,\"l18n_diffsource\":\"\",\"t3ver_oid\":0,\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"hideOnMobile\":0,\"categories\":0,\"selected_categories\":null,\"date\":0,\"marginPos\":\"0\",\"margin\":\"0\",\"url\":\"\",\"headlines\":\"\",\"textColor\":\"text-darkBlue\",\"iconColor\":\"\",\"icon\":\"\",\"boxed\":0,\"textAlignment\":\"\",\"upperCase\":0,\"aspectRatio\":\"\",\"copyright\":0,\"copyrightColor\":\"\",\"showImage\":0,\"hasHeader\":0,\"author\":\"\",\"linkTitle\":\"\",\"bgColor\":\"\",\"width\":\"w100\",\"disableHeader\":1,\"contact\":\"\",\"gradient\":\"\",\"position\":\"\",\"bottom\":0,\"amount\":\"\",\"autoplay\":0,\"loop\":0,\"textColor2\":\"\",\"rowDescription\":\"\",\"CType\":\"twoh_textelement\",\"header\":\"Willkommen bei TYPO3 Kickstarter! \",\"header_position\":\"\",\"bodytext\":\"<p>Willkommen bei TYPO3 Kickstarter! Unsere Anwendung bietet eine zeitsparende und zuverl\\u00e4ssige L\\u00f6sung zur Erstellung einer Website auf Basis von TYPO3. Dank unseres umfangreichen Boilerplates und einer Auswahl an hochwertigen Extensions von TWOh k\\u00f6nnen Sie in k\\u00fcrzester Zeit eine professionelle, sichere und stabile Webseite erstellen. Egal, ob Sie eine Unternehmenswebsite, einen Onlineshop oder eine pers\\u00f6nliche Blogseite ben\\u00f6tigen, TYPO3 Kickstarter macht den Prozess einfach und effizient. Starten Sie noch heute und verwirklichen Sie Ihre Webprojekte mit Leichtigkeit!<\\/p>\",\"bullets_type\":0,\"uploads_description\":0,\"uploads_type\":0,\"assets\":0,\"image\":0,\"imagewidth\":0,\"imageorient\":0,\"imagecols\":2,\"imageborder\":0,\"media\":0,\"layout\":0,\"frame_class\":\"default\",\"cols\":0,\"space_before_class\":\"\",\"space_after_class\":\"\",\"records\":null,\"pages\":null,\"colPos\":0,\"subheader\":\"\",\"header_link\":\"\",\"image_zoom\":0,\"header_layout\":\"0\",\"list_type\":\"\",\"sectionIndex\":1,\"linkToTop\":0,\"file_collections\":null,\"filelink_size\":0,\"filelink_sorting\":\"\",\"filelink_sorting_direction\":\"\",\"target\":\"\",\"recursive\":0,\"imageheight\":0,\"pi_flexform\":null,\"accessibility_title\":\"\",\"accessibility_bypass\":0,\"accessibility_bypass_text\":\"\",\"category_field\":\"\",\"table_class\":\"\",\"table_caption\":null,\"table_delimiter\":124,\"table_enclosure\":0,\"table_header_position\":0,\"table_tfoot\":0,\"tx_impexp_origuid\":0,\"tx_news_related_news\":0,\"tx_container_parent\":0}',0,'0400$a242bbf365f6ada28b1ecc91cb8f96c6:7fa2c035f26826fe83eeecaaeddc4d40'),(7,1710607838,1,'BE',1,0,2,'tt_content','{\"uid\":2,\"pid\":1,\"tstamp\":1710607838,\"crdate\":1710607838,\"deleted\":0,\"hidden\":0,\"starttime\":0,\"endtime\":0,\"fe_group\":\"0\",\"sorting\":128,\"editlock\":0,\"sys_language_uid\":0,\"l18n_parent\":0,\"l10n_source\":0,\"l10n_state\":null,\"t3_origuid\":0,\"l18n_diffsource\":\"\",\"t3ver_oid\":0,\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"hideOnMobile\":0,\"categories\":0,\"selected_categories\":null,\"date\":0,\"marginPos\":\"\",\"margin\":\"\",\"url\":\"\",\"headlines\":\"\",\"textColor\":\"\",\"iconColor\":\"\",\"icon\":\"\",\"boxed\":0,\"textAlignment\":\"\",\"upperCase\":0,\"aspectRatio\":\"\",\"copyright\":0,\"copyrightColor\":\"\",\"showImage\":0,\"hasHeader\":0,\"author\":\"\",\"linkTitle\":\"\",\"bgColor\":\"\",\"width\":\"\",\"disableHeader\":0,\"contact\":\"\",\"gradient\":\"\",\"position\":\"\",\"bottom\":0,\"amount\":\"\",\"autoplay\":0,\"loop\":0,\"textColor2\":\"\",\"rowDescription\":null,\"CType\":\"onecol\",\"header\":\"\",\"header_position\":\"\",\"bodytext\":null,\"bullets_type\":0,\"uploads_description\":0,\"uploads_type\":0,\"assets\":0,\"image\":0,\"imagewidth\":0,\"imageorient\":0,\"imagecols\":2,\"imageborder\":0,\"media\":0,\"layout\":0,\"frame_class\":\"default\",\"cols\":0,\"space_before_class\":\"\",\"space_after_class\":\"\",\"records\":null,\"pages\":null,\"colPos\":0,\"subheader\":\"\",\"header_link\":\"\",\"image_zoom\":0,\"header_layout\":\"0\",\"list_type\":\"\",\"sectionIndex\":1,\"linkToTop\":0,\"file_collections\":null,\"filelink_size\":0,\"filelink_sorting\":\"\",\"filelink_sorting_direction\":\"\",\"target\":\"\",\"recursive\":0,\"imageheight\":0,\"pi_flexform\":null,\"accessibility_title\":\"\",\"accessibility_bypass\":0,\"accessibility_bypass_text\":\"\",\"category_field\":\"\",\"table_class\":\"\",\"table_caption\":null,\"table_delimiter\":124,\"table_enclosure\":0,\"table_header_position\":0,\"table_tfoot\":0,\"tx_impexp_origuid\":0,\"tx_news_related_news\":0,\"tx_container_parent\":0}',0,'0400$b70a50d482de8680e2885c3a1d82a8d8:01dbc21fdb1263685b9147b3b1596ea8'),(8,1710607848,2,'BE',1,0,2,'tt_content','{\"oldRecord\":{\"pi_flexform\":null,\"fe_group\":\"0\",\"l18n_diffsource\":\"\"},\"newRecord\":{\"pi_flexform\":\"<?xml version=\\\"1.0\\\" encoding=\\\"utf-8\\\" standalone=\\\"yes\\\" ?>\\n<T3FlexForms>\\n    <data>\\n        <sheet index=\\\"sDEF\\\">\\n            <language index=\\\"lDEF\\\">\\n                <field index=\\\"boxed\\\">\\n                    <value index=\\\"vDEF\\\">1<\\/value>\\n                <\\/field>\\n                <field index=\\\"marginPos\\\">\\n                    <value index=\\\"vDEF\\\">y-<\\/value>\\n                <\\/field>\\n                <field index=\\\"bgcolor\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"disallowPadding\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"colGap\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n            <\\/language>\\n        <\\/sheet>\\n    <\\/data>\\n<\\/T3FlexForms>\",\"fe_group\":\"\",\"l18n_diffsource\":\"{\\\"CType\\\":\\\"\\\",\\\"colPos\\\":\\\"\\\",\\\"tx_container_parent\\\":\\\"\\\",\\\"header\\\":\\\"\\\",\\\"pi_flexform\\\":\\\"\\\",\\\"layout\\\":\\\"\\\",\\\"frame_class\\\":\\\"\\\",\\\"space_before_class\\\":\\\"\\\",\\\"space_after_class\\\":\\\"\\\",\\\"sectionIndex\\\":\\\"\\\",\\\"linkToTop\\\":\\\"\\\",\\\"sys_language_uid\\\":\\\"\\\",\\\"hidden\\\":\\\"\\\",\\\"starttime\\\":\\\"\\\",\\\"endtime\\\":\\\"\\\",\\\"fe_group\\\":\\\"\\\",\\\"editlock\\\":\\\"\\\",\\\"categories\\\":\\\"\\\",\\\"rowDescription\\\":\\\"\\\"}\"}}',0,'0400$9e34f61571e3dde096c6515a4e423f0f:01dbc21fdb1263685b9147b3b1596ea8'),(9,1710607856,2,'BE',1,0,2,'tt_content','{\"oldRecord\":{\"pi_flexform\":\"<?xml version=\\\"1.0\\\" encoding=\\\"utf-8\\\" standalone=\\\"yes\\\" ?>\\n<T3FlexForms>\\n    <data>\\n        <sheet index=\\\"sDEF\\\">\\n            <language index=\\\"lDEF\\\">\\n                <field index=\\\"boxed\\\">\\n                    <value index=\\\"vDEF\\\">1<\\/value>\\n                <\\/field>\\n                <field index=\\\"marginPos\\\">\\n                    <value index=\\\"vDEF\\\">y-<\\/value>\\n                <\\/field>\\n                <field index=\\\"bgcolor\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"disallowPadding\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"colGap\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n            <\\/language>\\n        <\\/sheet>\\n    <\\/data>\\n<\\/T3FlexForms>\"},\"newRecord\":{\"pi_flexform\":\"<?xml version=\\\"1.0\\\" encoding=\\\"utf-8\\\" standalone=\\\"yes\\\" ?>\\n<T3FlexForms>\\n    <data>\\n        <sheet index=\\\"sDEF\\\">\\n            <language index=\\\"lDEF\\\">\\n                <field index=\\\"boxed\\\">\\n                    <value index=\\\"vDEF\\\">1<\\/value>\\n                <\\/field>\\n                <field index=\\\"marginPos\\\">\\n                    <value index=\\\"vDEF\\\">y-<\\/value>\\n                <\\/field>\\n                <field index=\\\"bgcolor\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"disallowPadding\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"colGap\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"margin\\\">\\n                    <value index=\\\"vDEF\\\">3<\\/value>\\n                <\\/field>\\n            <\\/language>\\n        <\\/sheet>\\n    <\\/data>\\n<\\/T3FlexForms>\"}}',0,'0400$8580cb810daa2543f8d1c43a2c28c28f:01dbc21fdb1263685b9147b3b1596ea8'),(10,1710607865,3,'BE',1,0,1,'tt_content','{\"oldPageId\":1,\"newPageId\":1,\"oldData\":{\"header\":\"Willkommen bei TYPO3 Kickstarter! \",\"pid\":1,\"event_pid\":1,\"t3ver_state\":0},\"newData\":{\"tstamp\":1710607865,\"pid\":1,\"sorting\":192}}',0,'0400$4d60163161cf7137a2632aa11fafd6eb:7fa2c035f26826fe83eeecaaeddc4d40'),(11,1710607865,2,'BE',1,0,1,'tt_content','{\"oldRecord\":{\"colPos\":0,\"tx_container_parent\":0,\"l18n_diffsource\":\"\"},\"newRecord\":{\"colPos\":\"101\",\"tx_container_parent\":\"2\",\"l18n_diffsource\":\"{\\\"colPos\\\":\\\"\\\",\\\"sys_language_uid\\\":\\\"\\\",\\\"tx_container_parent\\\":\\\"\\\"}\"}}',0,'0400$9ee752c0e536e89787493d0ebe4c188e:7fa2c035f26826fe83eeecaaeddc4d40'),(12,1710607982,1,'BE',1,0,3,'tt_content','{\"uid\":3,\"pid\":1,\"tstamp\":1710607982,\"crdate\":1710607982,\"deleted\":0,\"hidden\":0,\"starttime\":0,\"endtime\":0,\"fe_group\":\"0\",\"sorting\":64,\"editlock\":0,\"sys_language_uid\":0,\"l18n_parent\":0,\"l10n_source\":0,\"l10n_state\":null,\"t3_origuid\":0,\"l18n_diffsource\":\"\",\"t3ver_oid\":0,\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"hideOnMobile\":0,\"categories\":0,\"selected_categories\":null,\"date\":0,\"marginPos\":\"\",\"margin\":\"\",\"url\":\"\",\"headlines\":\"\",\"textColor\":\"\",\"iconColor\":\"\",\"icon\":\"\",\"boxed\":0,\"textAlignment\":\"\",\"upperCase\":0,\"aspectRatio\":\"\",\"copyright\":0,\"copyrightColor\":\"\",\"showImage\":0,\"hasHeader\":0,\"author\":\"\",\"linkTitle\":\"\",\"bgColor\":\"\",\"width\":\"\",\"disableHeader\":0,\"contact\":\"\",\"gradient\":\"\",\"position\":\"\",\"bottom\":0,\"amount\":\"\",\"autoplay\":0,\"loop\":0,\"textColor2\":\"\",\"rowDescription\":null,\"CType\":\"twoh_imageelement\",\"header\":\"TYPO3 Kickstarter Anwendung\",\"header_position\":\"\",\"bodytext\":null,\"bullets_type\":0,\"uploads_description\":0,\"uploads_type\":0,\"assets\":0,\"image\":0,\"imagewidth\":0,\"imageorient\":0,\"imagecols\":2,\"imageborder\":0,\"media\":0,\"layout\":0,\"frame_class\":\"default\",\"cols\":0,\"space_before_class\":\"\",\"space_after_class\":\"\",\"records\":null,\"pages\":null,\"colPos\":0,\"subheader\":\"\",\"header_link\":\"\",\"image_zoom\":0,\"header_layout\":\"0\",\"list_type\":\"\",\"sectionIndex\":1,\"linkToTop\":0,\"file_collections\":null,\"filelink_size\":0,\"filelink_sorting\":\"\",\"filelink_sorting_direction\":\"\",\"target\":\"\",\"recursive\":0,\"imageheight\":0,\"pi_flexform\":null,\"accessibility_title\":\"\",\"accessibility_bypass\":0,\"accessibility_bypass_text\":\"\",\"category_field\":\"\",\"table_class\":\"\",\"table_caption\":null,\"table_delimiter\":124,\"table_enclosure\":0,\"table_header_position\":0,\"table_tfoot\":0,\"tx_impexp_origuid\":0,\"tx_news_related_news\":0,\"tx_container_parent\":0}',0,'0400$0391798898de2faea3dceccc21acdedf:b92300cfb5d1d3645c9cb212a7f56c1f'),(13,1710607982,3,'BE',1,0,3,'tt_content','{\"oldPageId\":1,\"newPageId\":1,\"oldData\":{\"header\":\"TYPO3 Kickstarter Anwendung\",\"pid\":1,\"event_pid\":1,\"t3ver_state\":0},\"newData\":{\"tstamp\":1710607982,\"pid\":1,\"sorting\":64}}',0,'0400$c6aaeec882dd98638848377a7b4ebdf2:b92300cfb5d1d3645c9cb212a7f56c1f'),(14,1710608096,2,'BE',1,0,3,'tt_content','{\"oldRecord\":{\"marginPos\":\"\",\"margin\":\"\",\"aspectRatio\":\"\",\"fe_group\":\"0\",\"l18n_diffsource\":\"\"},\"newRecord\":{\"marginPos\":\"0\",\"margin\":\"0\",\"aspectRatio\":\"0\",\"fe_group\":\"\",\"l18n_diffsource\":\"{\\\"CType\\\":\\\"\\\",\\\"colPos\\\":\\\"\\\",\\\"tx_container_parent\\\":\\\"\\\",\\\"hideOnMobile\\\":\\\"\\\",\\\"boxed\\\":\\\"\\\",\\\"marginPos\\\":\\\"\\\",\\\"margin\\\":\\\"\\\",\\\"header\\\":\\\"\\\",\\\"aspectRatio\\\":\\\"\\\",\\\"media\\\":\\\"\\\",\\\"copyright\\\":\\\"\\\",\\\"sys_language_uid\\\":\\\"\\\",\\\"hidden\\\":\\\"\\\",\\\"starttime\\\":\\\"\\\",\\\"endtime\\\":\\\"\\\",\\\"fe_group\\\":\\\"\\\",\\\"editlock\\\":\\\"\\\",\\\"rowDescription\\\":\\\"\\\"}\"}}',0,'0400$3cb86b6fdab74a4c84eb769bef80ea4d:b92300cfb5d1d3645c9cb212a7f56c1f'),(15,1710608096,1,'BE',1,0,1,'sys_file_reference','{\"uid\":1,\"pid\":1,\"tstamp\":1710608096,\"crdate\":1710608096,\"deleted\":0,\"hidden\":0,\"sys_language_uid\":0,\"l10n_parent\":0,\"l10n_state\":null,\"l10n_diffsource\":\"\",\"t3ver_oid\":0,\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"uid_local\":4,\"uid_foreign\":0,\"tablenames\":\"\",\"fieldname\":\"\",\"sorting_foreign\":0,\"title\":null,\"description\":null,\"alternative\":null,\"link\":\"\",\"crop\":\"{\\\"desktop\\\":{\\\"cropArea\\\":{\\\"x\\\":0,\\\"y\\\":0,\\\"width\\\":1,\\\"height\\\":1},\\\"selectedRatio\\\":\\\"NaN\\\",\\\"focusArea\\\":null},\\\"tablet\\\":{\\\"cropArea\\\":{\\\"x\\\":0,\\\"y\\\":0,\\\"width\\\":1,\\\"height\\\":1},\\\"selectedRatio\\\":\\\"NaN\\\",\\\"focusArea\\\":null},\\\"mobile\\\":{\\\"cropArea\\\":{\\\"x\\\":0,\\\"y\\\":0,\\\"width\\\":1,\\\"height\\\":1},\\\"selectedRatio\\\":\\\"NaN\\\",\\\"focusArea\\\":null}}\",\"autoplay\":0,\"showinpreview\":0}',0,'0400$3cb86b6fdab74a4c84eb769bef80ea4d:4cf496f597e7b095ce8b755e6cec3c0c'),(16,1710608096,2,'BE',1,0,3,'tt_content','{\"oldRecord\":{\"marginPos\":\"\",\"margin\":\"\",\"aspectRatio\":\"\",\"fe_group\":\"0\",\"l18n_diffsource\":\"\"},\"newRecord\":{\"marginPos\":\"0\",\"margin\":\"0\",\"aspectRatio\":\"0\",\"fe_group\":\"\",\"l18n_diffsource\":\"{\\\"CType\\\":\\\"\\\",\\\"colPos\\\":\\\"\\\",\\\"tx_container_parent\\\":\\\"\\\",\\\"hideOnMobile\\\":\\\"\\\",\\\"boxed\\\":\\\"\\\",\\\"marginPos\\\":\\\"\\\",\\\"margin\\\":\\\"\\\",\\\"header\\\":\\\"\\\",\\\"aspectRatio\\\":\\\"\\\",\\\"media\\\":\\\"\\\",\\\"copyright\\\":\\\"\\\",\\\"sys_language_uid\\\":\\\"\\\",\\\"hidden\\\":\\\"\\\",\\\"starttime\\\":\\\"\\\",\\\"endtime\\\":\\\"\\\",\\\"fe_group\\\":\\\"\\\",\\\"editlock\\\":\\\"\\\",\\\"rowDescription\\\":\\\"\\\"}\"}}',0,'0400$3cb86b6fdab74a4c84eb769bef80ea4d:b92300cfb5d1d3645c9cb212a7f56c1f'),(17,1710608118,1,'BE',1,0,4,'tt_content','{\"uid\":4,\"pid\":1,\"tstamp\":1710608118,\"crdate\":1710608118,\"deleted\":0,\"hidden\":0,\"starttime\":0,\"endtime\":0,\"fe_group\":\"0\",\"sorting\":32,\"editlock\":0,\"sys_language_uid\":0,\"l18n_parent\":0,\"l10n_source\":0,\"l10n_state\":null,\"t3_origuid\":0,\"l18n_diffsource\":\"\",\"t3ver_oid\":0,\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"hideOnMobile\":0,\"categories\":0,\"selected_categories\":null,\"date\":0,\"marginPos\":\"\",\"margin\":\"\",\"url\":\"\",\"headlines\":\"\",\"textColor\":\"\",\"iconColor\":\"\",\"icon\":\"\",\"boxed\":0,\"textAlignment\":\"\",\"upperCase\":0,\"aspectRatio\":\"\",\"copyright\":0,\"copyrightColor\":\"\",\"showImage\":0,\"hasHeader\":0,\"author\":\"\",\"linkTitle\":\"\",\"bgColor\":\"\",\"width\":\"\",\"disableHeader\":0,\"contact\":\"\",\"gradient\":\"\",\"position\":\"\",\"bottom\":0,\"amount\":\"\",\"autoplay\":0,\"loop\":0,\"textColor2\":\"\",\"rowDescription\":null,\"CType\":\"threecol\",\"header\":\"\",\"header_position\":\"\",\"bodytext\":null,\"bullets_type\":0,\"uploads_description\":0,\"uploads_type\":0,\"assets\":0,\"image\":0,\"imagewidth\":0,\"imageorient\":0,\"imagecols\":2,\"imageborder\":0,\"media\":0,\"layout\":0,\"frame_class\":\"default\",\"cols\":0,\"space_before_class\":\"\",\"space_after_class\":\"\",\"records\":null,\"pages\":null,\"colPos\":0,\"subheader\":\"\",\"header_link\":\"\",\"image_zoom\":0,\"header_layout\":\"0\",\"list_type\":\"\",\"sectionIndex\":1,\"linkToTop\":0,\"file_collections\":null,\"filelink_size\":0,\"filelink_sorting\":\"\",\"filelink_sorting_direction\":\"\",\"target\":\"\",\"recursive\":0,\"imageheight\":0,\"pi_flexform\":null,\"accessibility_title\":\"\",\"accessibility_bypass\":0,\"accessibility_bypass_text\":\"\",\"category_field\":\"\",\"table_class\":\"\",\"table_caption\":null,\"table_delimiter\":124,\"table_enclosure\":0,\"table_header_position\":0,\"table_tfoot\":0,\"tx_impexp_origuid\":0,\"tx_news_related_news\":0,\"tx_container_parent\":0}',0,'0400$c9340ef5abe0e5707854500318d6d576:4d391f5ef79b8d5d10dffa8a07ca167d'),(18,1710608125,3,'BE',1,0,3,'tt_content','{\"oldPageId\":1,\"newPageId\":1,\"oldData\":{\"header\":\"TYPO3 Kickstarter Anwendung\",\"pid\":1,\"event_pid\":1,\"t3ver_state\":0},\"newData\":{\"tstamp\":1710608125,\"pid\":1,\"sorting\":48}}',0,'0400$6c85e2e8cc8ca2c1e4382f0e7a543662:b92300cfb5d1d3645c9cb212a7f56c1f'),(19,1710608125,2,'BE',1,0,3,'tt_content','{\"oldRecord\":{\"colPos\":0,\"tx_container_parent\":0,\"l18n_diffsource\":\"{\\\"CType\\\":\\\"\\\",\\\"colPos\\\":\\\"\\\",\\\"tx_container_parent\\\":\\\"\\\",\\\"hideOnMobile\\\":\\\"\\\",\\\"boxed\\\":\\\"\\\",\\\"marginPos\\\":\\\"\\\",\\\"margin\\\":\\\"\\\",\\\"header\\\":\\\"\\\",\\\"aspectRatio\\\":\\\"\\\",\\\"media\\\":\\\"\\\",\\\"copyright\\\":\\\"\\\",\\\"sys_language_uid\\\":\\\"\\\",\\\"hidden\\\":\\\"\\\",\\\"starttime\\\":\\\"\\\",\\\"endtime\\\":\\\"\\\",\\\"fe_group\\\":\\\"\\\",\\\"editlock\\\":\\\"\\\",\\\"rowDescription\\\":\\\"\\\"}\"},\"newRecord\":{\"colPos\":\"102\",\"tx_container_parent\":\"4\",\"l18n_diffsource\":\"{\\\"colPos\\\":\\\"\\\",\\\"sys_language_uid\\\":\\\"\\\",\\\"tx_container_parent\\\":\\\"\\\"}\"}}',0,'0400$feab93837ccaa70ed931542987c9271e:b92300cfb5d1d3645c9cb212a7f56c1f'),(20,1710608233,1,'BE',1,0,5,'tt_content','{\"uid\":5,\"pid\":1,\"tstamp\":1710608233,\"crdate\":1710608233,\"deleted\":0,\"hidden\":0,\"starttime\":0,\"endtime\":0,\"fe_group\":\"0\",\"sorting\":16,\"editlock\":0,\"sys_language_uid\":0,\"l18n_parent\":0,\"l10n_source\":0,\"l10n_state\":null,\"t3_origuid\":0,\"l18n_diffsource\":\"\",\"t3ver_oid\":0,\"t3ver_wsid\":0,\"t3ver_state\":0,\"t3ver_stage\":0,\"hideOnMobile\":0,\"categories\":0,\"selected_categories\":null,\"date\":0,\"marginPos\":\"\",\"margin\":\"\",\"url\":\"\",\"headlines\":\"\",\"textColor\":\"\",\"iconColor\":\"\",\"icon\":\"\",\"boxed\":0,\"textAlignment\":\"\",\"upperCase\":0,\"aspectRatio\":\"\",\"copyright\":0,\"copyrightColor\":\"\",\"showImage\":0,\"hasHeader\":0,\"author\":\"\",\"linkTitle\":\"\",\"bgColor\":\"\",\"width\":\"\",\"disableHeader\":0,\"contact\":\"\",\"gradient\":\"\",\"position\":\"\",\"bottom\":0,\"amount\":\"\",\"autoplay\":0,\"loop\":0,\"textColor2\":\"\",\"rowDescription\":null,\"CType\":\"twocol\",\"header\":\"\",\"header_position\":\"\",\"bodytext\":null,\"bullets_type\":0,\"uploads_description\":0,\"uploads_type\":0,\"assets\":0,\"image\":0,\"imagewidth\":0,\"imageorient\":0,\"imagecols\":2,\"imageborder\":0,\"media\":0,\"layout\":0,\"frame_class\":\"default\",\"cols\":0,\"space_before_class\":\"\",\"space_after_class\":\"\",\"records\":null,\"pages\":null,\"colPos\":0,\"subheader\":\"\",\"header_link\":\"\",\"image_zoom\":0,\"header_layout\":\"0\",\"list_type\":\"\",\"sectionIndex\":1,\"linkToTop\":0,\"file_collections\":null,\"filelink_size\":0,\"filelink_sorting\":\"\",\"filelink_sorting_direction\":\"\",\"target\":\"\",\"recursive\":0,\"imageheight\":0,\"pi_flexform\":null,\"accessibility_title\":\"\",\"accessibility_bypass\":0,\"accessibility_bypass_text\":\"\",\"category_field\":\"\",\"table_class\":\"\",\"table_caption\":null,\"table_delimiter\":124,\"table_enclosure\":0,\"table_header_position\":0,\"table_tfoot\":0,\"tx_impexp_origuid\":0,\"tx_news_related_news\":0,\"tx_container_parent\":0}',0,'0400$6c0bd2b859b2b7edd6e2d58696bad153:c7626fc9bcba6f70beb6ebc085a400db'),(21,1710608239,3,'BE',1,0,3,'tt_content','{\"oldPageId\":1,\"newPageId\":1,\"oldData\":{\"header\":\"TYPO3 Kickstarter Anwendung\",\"pid\":1,\"event_pid\":1,\"t3ver_state\":0},\"newData\":{\"tstamp\":1710608239,\"pid\":1,\"sorting\":24}}',0,'0400$dda4dfecd9d35c1ce31a7f762f6f0b59:b92300cfb5d1d3645c9cb212a7f56c1f'),(22,1710608239,2,'BE',1,0,3,'tt_content','{\"oldRecord\":{\"tx_container_parent\":4},\"newRecord\":{\"tx_container_parent\":\"5\"}}',0,'0400$6509425b494ab6905983058042a98d29:b92300cfb5d1d3645c9cb212a7f56c1f'),(23,1710608243,4,'BE',1,0,4,'tt_content',NULL,0,'0400$0fb30354223f03c2a3438f6ba390e824:4d391f5ef79b8d5d10dffa8a07ca167d'),(24,1710608254,3,'BE',1,0,1,'tt_content','{\"oldPageId\":1,\"newPageId\":1,\"oldData\":{\"header\":\"Willkommen bei TYPO3 Kickstarter! \",\"pid\":1,\"event_pid\":1,\"t3ver_state\":0},\"newData\":{\"tstamp\":1710608254,\"pid\":1,\"sorting\":20}}',0,'0400$7b34feab9fb2346eaaa93a4114263317:7fa2c035f26826fe83eeecaaeddc4d40'),(25,1710608254,2,'BE',1,0,1,'tt_content','{\"oldRecord\":{\"tx_container_parent\":2},\"newRecord\":{\"tx_container_parent\":\"5\"}}',0,'0400$aa973905b03648e80772374d4cdb1a18:7fa2c035f26826fe83eeecaaeddc4d40'),(26,1710608258,4,'BE',1,0,2,'tt_content',NULL,0,'0400$99fbe154264a17d407211c847fc18d54:01dbc21fdb1263685b9147b3b1596ea8'),(27,1710608278,2,'BE',1,0,5,'tt_content','{\"oldRecord\":{\"pi_flexform\":null,\"fe_group\":\"0\",\"l18n_diffsource\":\"\"},\"newRecord\":{\"pi_flexform\":\"<?xml version=\\\"1.0\\\" encoding=\\\"utf-8\\\" standalone=\\\"yes\\\" ?>\\n<T3FlexForms>\\n    <data>\\n        <sheet index=\\\"sDEF\\\">\\n            <language index=\\\"lDEF\\\">\\n                <field index=\\\"boxed\\\">\\n                    <value index=\\\"vDEF\\\">1<\\/value>\\n                <\\/field>\\n                <field index=\\\"marginPos\\\">\\n                    <value index=\\\"vDEF\\\">y-<\\/value>\\n                <\\/field>\\n                <field index=\\\"bgcolor\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"disallowPadding\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"colGap\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n            <\\/language>\\n        <\\/sheet>\\n    <\\/data>\\n<\\/T3FlexForms>\",\"fe_group\":\"\",\"l18n_diffsource\":\"{\\\"CType\\\":\\\"\\\",\\\"colPos\\\":\\\"\\\",\\\"tx_container_parent\\\":\\\"\\\",\\\"header\\\":\\\"\\\",\\\"pi_flexform\\\":\\\"\\\",\\\"layout\\\":\\\"\\\",\\\"frame_class\\\":\\\"\\\",\\\"space_before_class\\\":\\\"\\\",\\\"space_after_class\\\":\\\"\\\",\\\"sectionIndex\\\":\\\"\\\",\\\"linkToTop\\\":\\\"\\\",\\\"sys_language_uid\\\":\\\"\\\",\\\"hidden\\\":\\\"\\\",\\\"starttime\\\":\\\"\\\",\\\"endtime\\\":\\\"\\\",\\\"fe_group\\\":\\\"\\\",\\\"editlock\\\":\\\"\\\",\\\"categories\\\":\\\"\\\",\\\"rowDescription\\\":\\\"\\\"}\"}}',0,'0400$a0e0eb21f3d7bc199906fbbc8444ed00:c7626fc9bcba6f70beb6ebc085a400db'),(28,1710608286,2,'BE',1,0,5,'tt_content','{\"oldRecord\":{\"pi_flexform\":\"<?xml version=\\\"1.0\\\" encoding=\\\"utf-8\\\" standalone=\\\"yes\\\" ?>\\n<T3FlexForms>\\n    <data>\\n        <sheet index=\\\"sDEF\\\">\\n            <language index=\\\"lDEF\\\">\\n                <field index=\\\"boxed\\\">\\n                    <value index=\\\"vDEF\\\">1<\\/value>\\n                <\\/field>\\n                <field index=\\\"marginPos\\\">\\n                    <value index=\\\"vDEF\\\">y-<\\/value>\\n                <\\/field>\\n                <field index=\\\"bgcolor\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"disallowPadding\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"colGap\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n            <\\/language>\\n        <\\/sheet>\\n    <\\/data>\\n<\\/T3FlexForms>\"},\"newRecord\":{\"pi_flexform\":\"<?xml version=\\\"1.0\\\" encoding=\\\"utf-8\\\" standalone=\\\"yes\\\" ?>\\n<T3FlexForms>\\n    <data>\\n        <sheet index=\\\"sDEF\\\">\\n            <language index=\\\"lDEF\\\">\\n                <field index=\\\"boxed\\\">\\n                    <value index=\\\"vDEF\\\">1<\\/value>\\n                <\\/field>\\n                <field index=\\\"marginPos\\\">\\n                    <value index=\\\"vDEF\\\">y-<\\/value>\\n                <\\/field>\\n                <field index=\\\"bgcolor\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"disallowPadding\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"colGap\\\">\\n                    <value index=\\\"vDEF\\\">0<\\/value>\\n                <\\/field>\\n                <field index=\\\"margin\\\">\\n                    <value index=\\\"vDEF\\\">3<\\/value>\\n                <\\/field>\\n            <\\/language>\\n        <\\/sheet>\\n    <\\/data>\\n<\\/T3FlexForms>\"}}',0,'0400$bdc35c88bc73c930f92cf74a82b35e21:c7626fc9bcba6f70beb6ebc085a400db');
/*!40000 ALTER TABLE `sys_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_http_report`
--

DROP TABLE IF EXISTS `sys_http_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_http_report` (
  `uuid` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint unsigned NOT NULL DEFAULT '0',
  `created` int unsigned NOT NULL,
  `changed` int unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scope` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_time` bigint unsigned NOT NULL,
  `meta` mediumtext COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `summary` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `type_scope` (`type`,`scope`),
  KEY `created` (`created`),
  KEY `changed` (`changed`),
  KEY `request_time` (`request_time`),
  KEY `summary_created` (`summary`,`created`),
  KEY `all_conditions` (`type`,`status`,`scope`,`summary`,`request_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_http_report`
--

LOCK TABLES `sys_http_report` WRITE;
/*!40000 ALTER TABLE `sys_http_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_http_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_lockedrecords`
--

DROP TABLE IF EXISTS `sys_lockedrecords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_lockedrecords` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `userid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `record_table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `record_uid` int NOT NULL DEFAULT '0',
  `record_pid` int NOT NULL DEFAULT '0',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `feuserid` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`,`tstamp`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_lockedrecords`
--

LOCK TABLES `sys_lockedrecords` WRITE;
/*!40000 ALTER TABLE `sys_lockedrecords` DISABLE KEYS */;
INSERT INTO `sys_lockedrecords` VALUES (15,1,1710608287,'tt_content',5,1,'arl',0);
/*!40000 ALTER TABLE `sys_lockedrecords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_log`
--

DROP TABLE IF EXISTS `sys_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_log` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `userid` int unsigned NOT NULL DEFAULT '0',
  `action` smallint unsigned NOT NULL DEFAULT '0',
  `recuid` int unsigned NOT NULL DEFAULT '0',
  `tablename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `recpid` int NOT NULL DEFAULT '0',
  `error` smallint unsigned NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` smallint unsigned NOT NULL DEFAULT '0',
  `channel` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `details_nr` smallint NOT NULL DEFAULT '0',
  `IP` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `log_data` text COLLATE utf8mb4_unicode_ci,
  `event_pid` int NOT NULL DEFAULT '-1',
  `workspace` int NOT NULL DEFAULT '0',
  `NEWid` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `request_id` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `time_micro` double NOT NULL DEFAULT '0',
  `component` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `level` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'info',
  `message` text COLLATE utf8mb4_unicode_ci,
  `data` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `event` (`userid`,`event_pid`),
  KEY `recuidIdx` (`recuid`),
  KEY `user_auth` (`type`,`action`,`tstamp`),
  KEY `request` (`request_id`),
  KEY `combined_1` (`tstamp`,`type`,`userid`),
  KEY `errorcount` (`tstamp`,`error`),
  KEY `channel` (`channel`),
  KEY `level` (`level`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_log`
--

LOCK TABLES `sys_log` WRITE;
/*!40000 ALTER TABLE `sys_log` DISABLE KEYS */;
INSERT INTO `sys_log` VALUES (1,0,1710605682,1,1,0,'',0,0,'User %s logged in from ###IP###',255,'user',1,'178.85.0.1','[\"arl\"]',-1,-99,'','',0,'','info',NULL,NULL),(2,0,1710605699,1,1,1,'pages',0,0,'Record \"{title}\" ({table}:{uid}) was inserted on page \"{pageTitle}\" ({pid})',1,'content',10,'178.85.0.1','{\"title\":\"Home\",\"table\":\"pages\",\"uid\":1,\"pageTitle\":\"[root-level]\",\"pid\":0}',0,0,'NEW_1','',0,'','info',NULL,NULL),(3,0,1710605709,1,1,1,'sys_template',0,0,'Record \"{title}\" ({table}:{uid}) was inserted on page \"{pageTitle}\" ({pid})',1,'content',10,'178.85.0.1','{\"title\":\"NEW SITE\",\"table\":\"sys_template\",\"uid\":1,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'NEW','',0,'','info',NULL,NULL),(4,0,1710605733,1,2,1,'sys_template',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"TWOH Kickstarter\",\"table\":\"sys_template\",\"uid\":1,\"history\":\"3\"}',1,0,'','',0,'','info',NULL,NULL),(5,0,1710605739,0,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1257246929: Tried resolving a template file for controller action \"Default->Default\" in format \".html\", but none of the paths contained the expected template file (Default/Default.html). The following paths were checked: /var/www/html/vendor/twoh/twoh_template_override_x/Resources/Private/Templates/Page/ | TYPO3Fluid\\Fluid\\View\\Exception\\InvalidTemplateResourceException thrown in file /var/www/html/vendor/typo3fluid/fluid/src/View/TemplatePaths.php in line 595. Requested URL: https://t3.kickstarter.local/',5,'php',0,'178.85.0.3','',-1,0,'','',0,'','error',NULL,NULL),(6,0,1710605741,0,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1257246929: Tried resolving a template file for controller action \"Default->Default\" in format \".html\", but none of the paths contained the expected template file (Default/Default.html). The following paths were checked: /var/www/html/vendor/twoh/twoh_template_override_x/Resources/Private/Templates/Page/ | TYPO3Fluid\\Fluid\\View\\Exception\\InvalidTemplateResourceException thrown in file /var/www/html/vendor/typo3fluid/fluid/src/View/TemplatePaths.php in line 595. Requested URL: https://t3.kickstarter.local/',5,'php',0,'178.85.0.3','',-1,0,'','',0,'','error',NULL,NULL),(7,0,1710605757,1,2,1,'pages',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"Home\",\"table\":\"pages\",\"uid\":1,\"history\":\"4\"}',1,0,'','',0,'','info',NULL,NULL),(8,0,1710605758,0,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1257246929: Tried resolving a template file for controller action \"Default->Default\" in format \".html\", but none of the paths contained the expected template file (Default/Default.html). The following paths were checked: /var/www/html/vendor/twoh/twoh_template_override_x/Resources/Private/Templates/Page/ | TYPO3Fluid\\Fluid\\View\\Exception\\InvalidTemplateResourceException thrown in file /var/www/html/vendor/typo3fluid/fluid/src/View/TemplatePaths.php in line 595. Requested URL: https://t3.kickstarter.local/',5,'php',0,'178.85.0.3','',-1,0,'','',0,'','error',NULL,NULL),(9,0,1710605761,1,0,0,'',0,2,'Core: Exception handler (WEB): Uncaught TYPO3 Exception: #1257246929: Tried resolving a template file for controller action \"Default->Default\" in format \".html\", but none of the paths contained the expected template file (Default/Default.html). The following paths were checked: /var/www/html/vendor/twoh/twoh_template_override_x/Resources/Private/Templates/Page/ | TYPO3Fluid\\Fluid\\View\\Exception\\InvalidTemplateResourceException thrown in file /var/www/html/vendor/typo3fluid/fluid/src/View/TemplatePaths.php in line 595. Requested URL: https://t3.kickstarter.local/',5,'php',0,'178.85.0.1','',-1,0,'','',0,'','error',NULL,NULL),(10,0,1710605878,1,2,1,'pages',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"Home\",\"table\":\"pages\",\"uid\":1,\"history\":\"5\"}',1,0,'','',0,'','info',NULL,NULL),(11,0,1710607821,1,1,1,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was inserted on page \"{pageTitle}\" ({pid})',1,'content',10,'178.85.0.1','{\"title\":\"Willkommen bei TYPO3 Kickstarter! \",\"table\":\"tt_content\",\"uid\":1,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'NEW65f5cd3135db4437485180','',0,'','info',NULL,NULL),(12,0,1710607838,1,1,2,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was inserted on page \"{pageTitle}\" ({pid})',1,'content',10,'178.85.0.1','{\"title\":\"[No title]\",\"table\":\"tt_content\",\"uid\":2,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'NEW65f5cddaab44b140700620','',0,'','info',NULL,NULL),(13,0,1710607848,1,2,2,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"[No title]\",\"table\":\"tt_content\",\"uid\":2,\"history\":\"8\"}',1,0,'','',0,'','info',NULL,NULL),(14,0,1710607856,1,2,2,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"[No title]\",\"table\":\"tt_content\",\"uid\":2,\"history\":\"9\"}',1,0,'','',0,'','info',NULL,NULL),(15,0,1710607865,1,4,1,'tt_content',0,0,'Moved record \"{title}\" ({table}:{uid}) on page \"{pageTitle}\" ({pid})',1,'content',4,'178.85.0.1','{\"title\":\"Willkommen bei TYPO3 Kickstarter! \",\"table\":\"tt_content\",\"uid\":1,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'','',0,'','info',NULL,NULL),(16,0,1710607865,1,2,1,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"Willkommen bei TYPO3 Kickstarter! \",\"table\":\"tt_content\",\"uid\":1,\"history\":\"11\"}',1,0,'','',0,'','info',NULL,NULL),(17,0,1710607982,1,1,3,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was inserted on page \"{pageTitle}\" ({pid})',1,'content',10,'178.85.0.1','{\"title\":\"TYPO3 Kickstarter Anwendung\",\"table\":\"tt_content\",\"uid\":3,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'NEWe4d55ccc38d1e5e2299727','',0,'','info',NULL,NULL),(18,0,1710607982,1,4,3,'tt_content',0,0,'Moved record \"{title}\" ({table}:{uid}) on page \"{pageTitle}\" ({pid})',1,'content',4,'178.85.0.1','{\"title\":\"TYPO3 Kickstarter Anwendung\",\"table\":\"tt_content\",\"uid\":3,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'','',0,'','info',NULL,NULL),(19,0,1710608096,1,2,3,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"TYPO3 Kickstarter Anwendung\",\"table\":\"tt_content\",\"uid\":3,\"history\":\"14\"}',1,0,'','',0,'','info',NULL,NULL),(20,0,1710608096,1,1,1,'sys_file_reference',0,0,'Record \"{title}\" ({table}:{uid}) was inserted on page \"{pageTitle}\" ({pid})',1,'content',10,'178.85.0.1','{\"title\":\"typo3-kickstarter-build-your-website-fast.jpg\",\"table\":\"sys_file_reference\",\"uid\":1,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'NEW65f5cedbaeb52754922076','',0,'','info',NULL,NULL),(21,0,1710608096,1,2,3,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"TYPO3 Kickstarter Anwendung\",\"table\":\"tt_content\",\"uid\":3,\"history\":\"16\"}',1,0,'','',0,'','info',NULL,NULL),(22,0,1710608118,1,1,4,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was inserted on page \"{pageTitle}\" ({pid})',1,'content',10,'178.85.0.1','{\"title\":\"[No title]\",\"table\":\"tt_content\",\"uid\":4,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'NEW65f5cef205cc7276178914','',0,'','info',NULL,NULL),(23,0,1710608125,1,4,3,'tt_content',0,0,'Moved record \"{title}\" ({table}:{uid}) on page \"{pageTitle}\" ({pid})',1,'content',4,'178.85.0.1','{\"title\":\"TYPO3 Kickstarter Anwendung\",\"table\":\"tt_content\",\"uid\":3,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'','',0,'','info',NULL,NULL),(24,0,1710608125,1,2,3,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"TYPO3 Kickstarter Anwendung\",\"table\":\"tt_content\",\"uid\":3,\"history\":\"19\"}',1,0,'','',0,'','info',NULL,NULL),(25,0,1710608233,1,1,5,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was inserted on page \"{pageTitle}\" ({pid})',1,'content',10,'178.85.0.1','{\"title\":\"[No title]\",\"table\":\"tt_content\",\"uid\":5,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'NEW65f5cf658a960670563063','',0,'','info',NULL,NULL),(26,0,1710608239,1,4,3,'tt_content',0,0,'Moved record \"{title}\" ({table}:{uid}) on page \"{pageTitle}\" ({pid})',1,'content',4,'178.85.0.1','{\"title\":\"TYPO3 Kickstarter Anwendung\",\"table\":\"tt_content\",\"uid\":3,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'','',0,'','info',NULL,NULL),(27,0,1710608239,1,2,3,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"TYPO3 Kickstarter Anwendung\",\"table\":\"tt_content\",\"uid\":3,\"history\":\"22\"}',1,0,'','',0,'','info',NULL,NULL),(28,0,1710608243,1,3,4,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was deleted from page \"{pageTitle}\" ({pid})',1,'content',0,'178.85.0.1','{\"title\":\"[No title]\",\"table\":\"tt_content\",\"uid\":4,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'','',0,'','info',NULL,NULL),(29,0,1710608254,1,4,1,'tt_content',0,0,'Moved record \"{title}\" ({table}:{uid}) on page \"{pageTitle}\" ({pid})',1,'content',4,'178.85.0.1','{\"title\":\"Willkommen bei TYPO3 Kickstarter! \",\"table\":\"tt_content\",\"uid\":1,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'','',0,'','info',NULL,NULL),(30,0,1710608254,1,2,1,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"Willkommen bei TYPO3 Kickstarter! \",\"table\":\"tt_content\",\"uid\":1,\"history\":\"25\"}',1,0,'','',0,'','info',NULL,NULL),(31,0,1710608258,1,3,2,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was deleted from page \"{pageTitle}\" ({pid})',1,'content',0,'178.85.0.1','{\"title\":\"[No title]\",\"table\":\"tt_content\",\"uid\":2,\"pageTitle\":\"Home\",\"pid\":1}',1,0,'','',0,'','info',NULL,NULL),(32,0,1710608278,1,2,5,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"[No title]\",\"table\":\"tt_content\",\"uid\":5,\"history\":\"27\"}',1,0,'','',0,'','info',NULL,NULL),(33,0,1710608286,1,2,5,'tt_content',0,0,'Record \"{title}\" ({table}:{uid}) was updated',1,'content',10,'178.85.0.1','{\"title\":\"[No title]\",\"table\":\"tt_content\",\"uid\":5,\"history\":\"28\"}',1,0,'','',0,'','info',NULL,NULL);
/*!40000 ALTER TABLE `sys_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_messenger_messages`
--

DROP TABLE IF EXISTS `sys_messenger_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_messenger_messages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `available_at` datetime NOT NULL,
  `delivered_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `queue_name` (`queue_name`),
  KEY `available_at` (`available_at`),
  KEY `delivered_at` (`delivered_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_messenger_messages`
--

LOCK TABLES `sys_messenger_messages` WRITE;
/*!40000 ALTER TABLE `sys_messenger_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_messenger_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_news`
--

DROP TABLE IF EXISTS `sys_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_news` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_news`
--

LOCK TABLES `sys_news` WRITE;
/*!40000 ALTER TABLE `sys_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_note`
--

DROP TABLE IF EXISTS `sys_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_note` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cruser` int unsigned NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci,
  `personal` smallint unsigned NOT NULL DEFAULT '0',
  `category` smallint unsigned NOT NULL DEFAULT '0',
  `position` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_note`
--

LOCK TABLES `sys_note` WRITE;
/*!40000 ALTER TABLE `sys_note` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_preview`
--

DROP TABLE IF EXISTS `sys_preview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_preview` (
  `keyword` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tstamp` int NOT NULL DEFAULT '0',
  `endtime` int NOT NULL DEFAULT '0',
  `config` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_preview`
--

LOCK TABLES `sys_preview` WRITE;
/*!40000 ALTER TABLE `sys_preview` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_preview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_redirect`
--

DROP TABLE IF EXISTS `sys_redirect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_redirect` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `updatedon` int unsigned NOT NULL DEFAULT '0',
  `createdon` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `disabled` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `source_host` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `source_path` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_regexp` smallint unsigned NOT NULL DEFAULT '0',
  `protected` smallint unsigned NOT NULL DEFAULT '0',
  `creation_type` int unsigned NOT NULL DEFAULT '0',
  `force_https` smallint unsigned NOT NULL DEFAULT '0',
  `respect_query_parameters` smallint unsigned NOT NULL DEFAULT '0',
  `keep_query_parameters` smallint unsigned NOT NULL DEFAULT '0',
  `target` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `target_statuscode` int NOT NULL DEFAULT '307',
  `hitcount` int NOT NULL DEFAULT '0',
  `disable_hitcount` smallint unsigned NOT NULL DEFAULT '0',
  `lasthiton` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `index_source` (`source_host`(80),`source_path`(80)),
  KEY `parent` (`pid`,`deleted`,`disabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_redirect`
--

LOCK TABLES `sys_redirect` WRITE;
/*!40000 ALTER TABLE `sys_redirect` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_redirect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_refindex`
--

DROP TABLE IF EXISTS `sys_refindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_refindex` (
  `hash` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tablename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `recuid` int NOT NULL DEFAULT '0',
  `field` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `flexpointer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `softref_key` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `softref_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sorting` int NOT NULL DEFAULT '0',
  `workspace` int NOT NULL DEFAULT '0',
  `ref_table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ref_uid` int NOT NULL DEFAULT '0',
  `ref_string` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`hash`),
  KEY `lookup_rec` (`tablename`(100),`recuid`),
  KEY `lookup_uid` (`ref_table`(100),`ref_uid`),
  KEY `lookup_string` (`ref_string`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_refindex`
--

LOCK TABLES `sys_refindex` WRITE;
/*!40000 ALTER TABLE `sys_refindex` DISABLE KEYS */;
INSERT INTO `sys_refindex` VALUES ('1c9b8784c1518ef7b22704c4fc698ca9','sys_file',2,'storage','','','',0,0,'sys_file_storage',1,''),('24d47b29aa969cf4db8635e76dd1c386','sys_file',3,'storage','','','',0,0,'sys_file_storage',1,''),('39433ea4a82060704109046e4828d3c8','sys_file',1,'storage','','','',0,0,'sys_file_storage',1,''),('3aca4d07b501ab193392d087c39c4dd9','tt_content',1,'tx_container_parent','','','',0,0,'tt_content',5,''),('9617f7aeacff3e2d566903ab174917c5','tt_content',3,'tx_container_parent','','','',0,0,'tt_content',5,''),('a3dcd1cdc39ffd54e49c4af9271fa820','tt_content',3,'media','','','',0,0,'sys_file_reference',1,''),('bab37143de5339e474516691bf0c5857','sys_file',4,'storage','','','',0,0,'sys_file_storage',1,''),('bbaf7772c492a53546a23c907ad3a5d7','sys_file_reference',1,'uid_local','','','',0,0,'sys_file',4,'');
/*!40000 ALTER TABLE `sys_refindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_registry`
--

DROP TABLE IF EXISTS `sys_registry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_registry` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `entry_namespace` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `entry_key` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `entry_value` mediumblob,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `entry_identifier` (`entry_namespace`,`entry_key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_registry`
--

LOCK TABLES `sys_registry` WRITE;
/*!40000 ALTER TABLE `sys_registry` DISABLE KEYS */;
INSERT INTO `sys_registry` VALUES (1,'languagePacks','de-ai_suite',_binary 'i:1710605404;'),(2,'languagePacks','de-container',_binary 'i:1710605404;'),(3,'languagePacks','de-content_blocks',_binary 'i:1710605404;'),(4,'languagePacks','de-solr',_binary 'i:1710605405;'),(5,'languagePacks','de-sourceopt',_binary 'i:1710605405;'),(6,'languagePacks','de-twoh_kickstarter',_binary 'i:1710605406;'),(7,'languagePacks','de-twoh_template_override_x',_binary 'i:1710605406;'),(8,'languagePacks','de-twoh_tiny_png',_binary 'i:1710605406;'),(9,'languagePacks','de',_binary 'i:1710605406;'),(10,'core','formProtectionSessionToken:1',_binary 's:64:\"b3b9d408e03475720880b19f1d88aa2bbf34448391b4135b002d9e9e5345292a\";');
/*!40000 ALTER TABLE `sys_registry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_template`
--

DROP TABLE IF EXISTS `sys_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_template` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `t3_origuid` int unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `root` smallint unsigned NOT NULL DEFAULT '0',
  `clear` smallint unsigned NOT NULL DEFAULT '0',
  `include_static_file` text COLLATE utf8mb4_unicode_ci,
  `constants` text COLLATE utf8mb4_unicode_ci,
  `config` text COLLATE utf8mb4_unicode_ci,
  `basedOn` tinytext COLLATE utf8mb4_unicode_ci,
  `includeStaticAfterBasedOn` smallint unsigned NOT NULL DEFAULT '0',
  `static_file_mode` smallint unsigned NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `roottemplate` (`deleted`,`hidden`,`root`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_template`
--

LOCK TABLES `sys_template` WRITE;
/*!40000 ALTER TABLE `sys_template` DISABLE KEYS */;
INSERT INTO `sys_template` VALUES (1,1,1710605733,1710605709,0,0,0,0,256,NULL,0,'TWOH Kickstarter',1,3,'EXT:twoh_kickstarter/Configuration/TypoScript,EXT:twoh_template_override_x/Configuration/TypoScript',NULL,'','',0,0,0);
/*!40000 ALTER TABLE `sys_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_workspace`
--

DROP TABLE IF EXISTS `sys_workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_workspace` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `adminusers` varchar(4000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `members` varchar(4000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `db_mountpoints` text COLLATE utf8mb4_unicode_ci,
  `file_mountpoints` text COLLATE utf8mb4_unicode_ci,
  `freeze` smallint NOT NULL DEFAULT '0',
  `live_edit` smallint NOT NULL DEFAULT '0',
  `publish_access` smallint NOT NULL DEFAULT '0',
  `previewlink_lifetime` int NOT NULL DEFAULT '0',
  `custom_stages` int NOT NULL DEFAULT '0',
  `stagechg_notification` smallint NOT NULL DEFAULT '0',
  `edit_notification_defaults` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `edit_notification_preselection` smallint NOT NULL DEFAULT '3',
  `edit_allow_notificaton_settings` smallint NOT NULL DEFAULT '0',
  `publish_notification_defaults` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `publish_notification_preselection` smallint NOT NULL DEFAULT '3',
  `publish_allow_notificaton_settings` smallint NOT NULL DEFAULT '0',
  `execute_notification_defaults` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `execute_notification_preselection` smallint NOT NULL DEFAULT '3',
  `execute_allow_notificaton_settings` smallint NOT NULL DEFAULT '0',
  `publish_time` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_workspace`
--

LOCK TABLES `sys_workspace` WRITE;
/*!40000 ALTER TABLE `sys_workspace` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_workspace_stage`
--

DROP TABLE IF EXISTS `sys_workspace_stage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_workspace_stage` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `responsible_persons` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `default_mailcomment` text COLLATE utf8mb4_unicode_ci,
  `parentid` int NOT NULL DEFAULT '0',
  `parenttable` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `notification_defaults` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `allow_notificaton_settings` smallint NOT NULL DEFAULT '0',
  `notification_preselection` smallint NOT NULL DEFAULT '8',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_workspace_stage`
--

LOCK TABLES `sys_workspace_stage` WRITE;
/*!40000 ALTER TABLE `sys_workspace_stage` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_workspace_stage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tt_content`
--

DROP TABLE IF EXISTS `tt_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tt_content` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `fe_group` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `editlock` smallint unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l18n_parent` int unsigned NOT NULL DEFAULT '0',
  `l10n_source` int unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `t3_origuid` int unsigned NOT NULL DEFAULT '0',
  `l18n_diffsource` mediumblob,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `hideOnMobile` int unsigned NOT NULL DEFAULT '0',
  `categories` int unsigned NOT NULL DEFAULT '0',
  `selected_categories` longtext COLLATE utf8mb4_unicode_ci,
  `date` int NOT NULL DEFAULT '0',
  `marginPos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `margin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `headlines` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `textColor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `iconColor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `boxed` int unsigned NOT NULL DEFAULT '0',
  `textAlignment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `upperCase` int unsigned NOT NULL DEFAULT '0',
  `aspectRatio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `copyright` int unsigned NOT NULL DEFAULT '0',
  `copyrightColor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `showImage` int unsigned NOT NULL DEFAULT '0',
  `hasHeader` int unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `linkTitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bgColor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `width` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `disableHeader` int unsigned NOT NULL DEFAULT '0',
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `gradient` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bottom` int unsigned NOT NULL DEFAULT '0',
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `autoplay` int unsigned NOT NULL DEFAULT '0',
  `loop` int unsigned NOT NULL DEFAULT '0',
  `textColor2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rowDescription` text COLLATE utf8mb4_unicode_ci,
  `CType` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `header_position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bodytext` mediumtext COLLATE utf8mb4_unicode_ci,
  `bullets_type` smallint unsigned NOT NULL DEFAULT '0',
  `uploads_description` smallint unsigned NOT NULL DEFAULT '0',
  `uploads_type` smallint unsigned NOT NULL DEFAULT '0',
  `assets` int unsigned NOT NULL DEFAULT '0',
  `image` int unsigned NOT NULL DEFAULT '0',
  `imagewidth` int unsigned NOT NULL DEFAULT '0',
  `imageorient` smallint unsigned NOT NULL DEFAULT '0',
  `imagecols` smallint unsigned NOT NULL DEFAULT '0',
  `imageborder` smallint unsigned NOT NULL DEFAULT '0',
  `media` int unsigned NOT NULL DEFAULT '0',
  `layout` int unsigned NOT NULL DEFAULT '0',
  `frame_class` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `cols` int unsigned NOT NULL DEFAULT '0',
  `space_before_class` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `space_after_class` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `records` text COLLATE utf8mb4_unicode_ci,
  `pages` text COLLATE utf8mb4_unicode_ci,
  `colPos` int unsigned NOT NULL DEFAULT '0',
  `subheader` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `header_link` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `image_zoom` smallint unsigned NOT NULL DEFAULT '0',
  `header_layout` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `list_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sectionIndex` smallint unsigned NOT NULL DEFAULT '0',
  `linkToTop` smallint unsigned NOT NULL DEFAULT '0',
  `file_collections` text COLLATE utf8mb4_unicode_ci,
  `filelink_size` smallint unsigned NOT NULL DEFAULT '0',
  `filelink_sorting` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `filelink_sorting_direction` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `target` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `recursive` smallint unsigned NOT NULL DEFAULT '0',
  `imageheight` int unsigned NOT NULL DEFAULT '0',
  `pi_flexform` mediumtext COLLATE utf8mb4_unicode_ci,
  `accessibility_title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `accessibility_bypass` smallint unsigned NOT NULL DEFAULT '0',
  `accessibility_bypass_text` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `category_field` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table_class` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table_caption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_delimiter` smallint unsigned NOT NULL DEFAULT '0',
  `table_enclosure` smallint unsigned NOT NULL DEFAULT '0',
  `table_header_position` smallint unsigned NOT NULL DEFAULT '0',
  `table_tfoot` smallint unsigned NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int NOT NULL DEFAULT '0',
  `tx_news_related_news` int NOT NULL DEFAULT '0',
  `tx_container_parent` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `translation_source` (`l10n_source`),
  KEY `parent` (`pid`,`sorting`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `language` (`l18n_parent`,`sys_language_uid`),
  KEY `index_newscontent` (`tx_news_related_news`),
  KEY `container_parent` (`tx_container_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tt_content`
--

LOCK TABLES `tt_content` WRITE;
/*!40000 ALTER TABLE `tt_content` DISABLE KEYS */;
INSERT INTO `tt_content` VALUES (1,1,1710608254,1710607821,0,0,0,0,'',20,0,0,0,0,NULL,0,_binary '{\"colPos\":\"\",\"sys_language_uid\":\"\",\"tx_container_parent\":\"\"}',0,0,0,0,0,0,NULL,0,'0','0','','','text-darkBlue','','',0,'',0,'',0,'',0,0,'','','','w100',1,'','','',0,'',0,0,'','','twoh_textelement','Willkommen bei TYPO3 Kickstarter! ','','<p>Willkommen bei TYPO3 Kickstarter! Unsere Anwendung bietet eine zeitsparende und zuverlässige Lösung zur Erstellung einer Website auf Basis von TYPO3. Dank unseres umfangreichen Boilerplates und einer Auswahl an hochwertigen Extensions von TWOh können Sie in kürzester Zeit eine professionelle, sichere und stabile Webseite erstellen. Egal, ob Sie eine Unternehmenswebsite, einen Onlineshop oder eine persönliche Blogseite benötigen, TYPO3 Kickstarter macht den Prozess einfach und effizient. Starten Sie noch heute und verwirklichen Sie Ihre Webprojekte mit Leichtigkeit!</p>',0,0,0,0,0,0,0,2,0,0,0,'default',0,'','',NULL,NULL,101,'','',0,'0','',1,0,NULL,0,'','','',0,0,NULL,'',0,'','','',NULL,124,0,0,0,0,0,5),(2,1,1710608258,1710607838,1,0,0,0,'',128,0,0,0,0,NULL,0,_binary '{\"CType\":\"\",\"colPos\":\"\",\"tx_container_parent\":\"\",\"header\":\"\",\"pi_flexform\":\"\",\"layout\":\"\",\"frame_class\":\"\",\"space_before_class\":\"\",\"space_after_class\":\"\",\"sectionIndex\":\"\",\"linkToTop\":\"\",\"sys_language_uid\":\"\",\"hidden\":\"\",\"starttime\":\"\",\"endtime\":\"\",\"fe_group\":\"\",\"editlock\":\"\",\"categories\":\"\",\"rowDescription\":\"\"}',0,0,0,0,0,0,NULL,0,'','','','','','','',0,'',0,'',0,'',0,0,'','','','',0,'','','',0,'',0,0,'',NULL,'onecol','','',NULL,0,0,0,0,0,0,0,2,0,0,0,'default',0,'','',NULL,NULL,0,'','',0,'0','',1,0,NULL,0,'','','',0,0,'<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\" ?>\n<T3FlexForms>\n    <data>\n        <sheet index=\"sDEF\">\n            <language index=\"lDEF\">\n                <field index=\"boxed\">\n                    <value index=\"vDEF\">1</value>\n                </field>\n                <field index=\"marginPos\">\n                    <value index=\"vDEF\">y-</value>\n                </field>\n                <field index=\"bgcolor\">\n                    <value index=\"vDEF\">0</value>\n                </field>\n                <field index=\"disallowPadding\">\n                    <value index=\"vDEF\">0</value>\n                </field>\n                <field index=\"colGap\">\n                    <value index=\"vDEF\">0</value>\n                </field>\n                <field index=\"margin\">\n                    <value index=\"vDEF\">3</value>\n                </field>\n            </language>\n        </sheet>\n    </data>\n</T3FlexForms>','',0,'','','',NULL,124,0,0,0,0,0,0),(3,1,1710608239,1710607982,0,0,0,0,'',24,0,0,0,0,NULL,0,_binary '{\"colPos\":\"\",\"sys_language_uid\":\"\",\"tx_container_parent\":\"\"}',0,0,0,0,0,0,NULL,0,'0','0','','','','','',0,'',0,'0',0,'',0,0,'','','','',0,'','','',0,'',0,0,'',NULL,'twoh_imageelement','TYPO3 Kickstarter Anwendung','',NULL,0,0,0,0,0,0,0,2,0,1,0,'default',0,'','',NULL,NULL,102,'','',0,'0','',1,0,NULL,0,'','','',0,0,NULL,'',0,'','','',NULL,124,0,0,0,0,0,5),(4,1,1710608243,1710608118,1,0,0,0,'0',32,0,0,0,0,NULL,0,'',0,0,0,0,0,0,NULL,0,'','','','','','','',0,'',0,'',0,'',0,0,'','','','',0,'','','',0,'',0,0,'',NULL,'threecol','','',NULL,0,0,0,0,0,0,0,2,0,0,0,'default',0,'','',NULL,NULL,0,'','',0,'0','',1,0,NULL,0,'','','',0,0,NULL,'',0,'','','',NULL,124,0,0,0,0,0,0),(5,1,1710608286,1710608233,0,0,0,0,'',16,0,0,0,0,NULL,0,_binary '{\"CType\":\"\",\"colPos\":\"\",\"tx_container_parent\":\"\",\"header\":\"\",\"pi_flexform\":\"\",\"layout\":\"\",\"frame_class\":\"\",\"space_before_class\":\"\",\"space_after_class\":\"\",\"sectionIndex\":\"\",\"linkToTop\":\"\",\"sys_language_uid\":\"\",\"hidden\":\"\",\"starttime\":\"\",\"endtime\":\"\",\"fe_group\":\"\",\"editlock\":\"\",\"categories\":\"\",\"rowDescription\":\"\"}',0,0,0,0,0,0,NULL,0,'','','','','','','',0,'',0,'',0,'',0,0,'','','','',0,'','','',0,'',0,0,'',NULL,'twocol','','',NULL,0,0,0,0,0,0,0,2,0,0,0,'default',0,'','',NULL,NULL,0,'','',0,'0','',1,0,NULL,0,'','','',0,0,'<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\" ?>\n<T3FlexForms>\n    <data>\n        <sheet index=\"sDEF\">\n            <language index=\"lDEF\">\n                <field index=\"boxed\">\n                    <value index=\"vDEF\">1</value>\n                </field>\n                <field index=\"marginPos\">\n                    <value index=\"vDEF\">y-</value>\n                </field>\n                <field index=\"bgcolor\">\n                    <value index=\"vDEF\">0</value>\n                </field>\n                <field index=\"disallowPadding\">\n                    <value index=\"vDEF\">0</value>\n                </field>\n                <field index=\"colGap\">\n                    <value index=\"vDEF\">0</value>\n                </field>\n                <field index=\"margin\">\n                    <value index=\"vDEF\">3</value>\n                </field>\n            </language>\n        </sheet>\n    </data>\n</T3FlexForms>','',0,'','','',NULL,124,0,0,0,0,0,0);
/*!40000 ALTER TABLE `tt_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_aisuite_domain_model_custom_prompt_template`
--

DROP TABLE IF EXISTS `tx_aisuite_domain_model_custom_prompt_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_aisuite_domain_model_custom_prompt_template` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `prompt` text COLLATE utf8mb4_unicode_ci,
  `scope` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_aisuite_domain_model_custom_prompt_template`
--

LOCK TABLES `tx_aisuite_domain_model_custom_prompt_template` WRITE;
/*!40000 ALTER TABLE `tx_aisuite_domain_model_custom_prompt_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_aisuite_domain_model_custom_prompt_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_aisuite_domain_model_server_prompt_template`
--

DROP TABLE IF EXISTS `tx_aisuite_domain_model_server_prompt_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_aisuite_domain_model_server_prompt_template` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `prompt` text COLLATE utf8mb4_unicode_ci,
  `scope` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_aisuite_domain_model_server_prompt_template`
--

LOCK TABLES `tx_aisuite_domain_model_server_prompt_template` WRITE;
/*!40000 ALTER TABLE `tx_aisuite_domain_model_server_prompt_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_aisuite_domain_model_server_prompt_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_extensionmanager_domain_model_extension`
--

DROP TABLE IF EXISTS `tx_extensionmanager_domain_model_extension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_extensionmanager_domain_model_extension` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `extension_key` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `repository` int NOT NULL DEFAULT '1',
  `remote` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ter',
  `version` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alldownloadcounter` int unsigned NOT NULL DEFAULT '0',
  `downloadcounter` int unsigned NOT NULL DEFAULT '0',
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `state` int NOT NULL DEFAULT '0',
  `review_state` int NOT NULL DEFAULT '0',
  `category` int NOT NULL DEFAULT '0',
  `serialized_dependencies` mediumtext COLLATE utf8mb4_unicode_ci,
  `author_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `author_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ownerusername` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `md5hash` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `update_comment` mediumtext COLLATE utf8mb4_unicode_ci,
  `authorcompany` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `integer_version` int NOT NULL DEFAULT '0',
  `current_version` int NOT NULL DEFAULT '0',
  `lastreviewedversion` int NOT NULL DEFAULT '0',
  `documentation_link` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distribution_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distribution_welcome_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_updated` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `versionextrepo` (`extension_key`,`version`,`remote`),
  KEY `index_extrepo` (`extension_key`,`remote`),
  KEY `index_versionrepo` (`integer_version`,`remote`,`extension_key`),
  KEY `index_currentversions` (`current_version`,`review_state`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_extensionmanager_domain_model_extension`
--

LOCK TABLES `tx_extensionmanager_domain_model_extension` WRITE;
/*!40000 ALTER TABLE `tx_extensionmanager_domain_model_extension` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_extensionmanager_domain_model_extension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_impexp_presets`
--

DROP TABLE IF EXISTS `tx_impexp_presets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_impexp_presets` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `public` smallint NOT NULL DEFAULT '0',
  `item_uid` int NOT NULL DEFAULT '0',
  `user_uid` int unsigned NOT NULL DEFAULT '0',
  `preset_data` blob,
  PRIMARY KEY (`uid`),
  KEY `lookup` (`item_uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_impexp_presets`
--

LOCK TABLES `tx_impexp_presets` WRITE;
/*!40000 ALTER TABLE `tx_impexp_presets` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_impexp_presets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_linkvalidator_link`
--

DROP TABLE IF EXISTS `tx_linkvalidator_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_linkvalidator_link` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `record_uid` int NOT NULL DEFAULT '0',
  `record_pid` int NOT NULL DEFAULT '0',
  `language` int NOT NULL DEFAULT '-1',
  `headline` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `element_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_title` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `url_response` text COLLATE utf8mb4_unicode_ci,
  `last_check` int NOT NULL DEFAULT '0',
  `link_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `needs_recheck` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_linkvalidator_link`
--

LOCK TABLES `tx_linkvalidator_link` WRITE;
/*!40000 ALTER TABLE `tx_linkvalidator_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_linkvalidator_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_news_domain_model_link`
--

DROP TABLE IF EXISTS `tx_news_domain_model_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_news_domain_model_link` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `pid` int NOT NULL DEFAULT '0',
  `tstamp` int NOT NULL DEFAULT '0',
  `crdate` int NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumtext COLLATE utf8mb4_unicode_ci,
  `l10n_source` int NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `deleted` smallint NOT NULL DEFAULT '0',
  `hidden` smallint NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `t3_origuid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `cruser_id` int NOT NULL DEFAULT '0',
  `parent` int NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8mb4_unicode_ci,
  `uri` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `news` (`parent`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_news_domain_model_link`
--

LOCK TABLES `tx_news_domain_model_link` WRITE;
/*!40000 ALTER TABLE `tx_news_domain_model_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_news_domain_model_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_news_domain_model_news`
--

DROP TABLE IF EXISTS `tx_news_domain_model_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_news_domain_model_news` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `pid` int NOT NULL DEFAULT '0',
  `tstamp` int NOT NULL DEFAULT '0',
  `crdate` int NOT NULL DEFAULT '0',
  `editlock` smallint NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumtext COLLATE utf8mb4_unicode_ci,
  `l10n_source` int NOT NULL DEFAULT '0',
  `deleted` smallint NOT NULL DEFAULT '0',
  `hidden` smallint NOT NULL DEFAULT '0',
  `starttime` int NOT NULL DEFAULT '0',
  `endtime` int NOT NULL DEFAULT '0',
  `fe_group` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `notes` text COLLATE utf8mb4_unicode_ci,
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `t3_origuid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `cruser_id` int NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `teaser` text COLLATE utf8mb4_unicode_ci,
  `bodytext` mediumtext COLLATE utf8mb4_unicode_ci,
  `datetime` bigint NOT NULL DEFAULT '0',
  `archive` bigint NOT NULL DEFAULT '0',
  `author` tinytext COLLATE utf8mb4_unicode_ci,
  `author_email` tinytext COLLATE utf8mb4_unicode_ci,
  `categories` int NOT NULL DEFAULT '0',
  `related` int NOT NULL DEFAULT '0',
  `related_from` int NOT NULL DEFAULT '0',
  `related_files` tinytext COLLATE utf8mb4_unicode_ci,
  `fal_related_files` int unsigned DEFAULT '0',
  `related_links` int NOT NULL DEFAULT '0',
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `tags` int NOT NULL DEFAULT '0',
  `media` text COLLATE utf8mb4_unicode_ci,
  `fal_media` int unsigned DEFAULT '0',
  `internalurl` text COLLATE utf8mb4_unicode_ci,
  `externalurl` text COLLATE utf8mb4_unicode_ci,
  `istopnews` int NOT NULL DEFAULT '0',
  `content_elements` int NOT NULL DEFAULT '0',
  `path_segment` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternative_title` tinytext COLLATE utf8mb4_unicode_ci,
  `sitemap_changefreq` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sitemap_priority` decimal(2,1) NOT NULL DEFAULT '0.5',
  `import_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `import_source` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `sys_language_uid_l10n_parent` (`sys_language_uid`,`l10n_parent`),
  KEY `path_segment` (`path_segment`(185),`uid`),
  KEY `import` (`import_id`,`import_source`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_news_domain_model_news`
--

LOCK TABLES `tx_news_domain_model_news` WRITE;
/*!40000 ALTER TABLE `tx_news_domain_model_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_news_domain_model_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_news_domain_model_news_related_mm`
--

DROP TABLE IF EXISTS `tx_news_domain_model_news_related_mm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_news_domain_model_news_related_mm` (
  `uid_local` int NOT NULL DEFAULT '0',
  `uid_foreign` int NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `sorting_foreign` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid_local`,`uid_foreign`),
  KEY `uid_local` (`uid_local`),
  KEY `uid_foreign` (`uid_foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_news_domain_model_news_related_mm`
--

LOCK TABLES `tx_news_domain_model_news_related_mm` WRITE;
/*!40000 ALTER TABLE `tx_news_domain_model_news_related_mm` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_news_domain_model_news_related_mm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_news_domain_model_news_tag_mm`
--

DROP TABLE IF EXISTS `tx_news_domain_model_news_tag_mm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_news_domain_model_news_tag_mm` (
  `uid_local` int NOT NULL DEFAULT '0',
  `uid_foreign` int NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `sorting_foreign` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid_local`,`uid_foreign`),
  KEY `uid_local` (`uid_local`),
  KEY `uid_foreign` (`uid_foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_news_domain_model_news_tag_mm`
--

LOCK TABLES `tx_news_domain_model_news_tag_mm` WRITE;
/*!40000 ALTER TABLE `tx_news_domain_model_news_tag_mm` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_news_domain_model_news_tag_mm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_news_domain_model_news_ttcontent_mm`
--

DROP TABLE IF EXISTS `tx_news_domain_model_news_ttcontent_mm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_news_domain_model_news_ttcontent_mm` (
  `uid_local` int NOT NULL DEFAULT '0',
  `uid_foreign` int NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  KEY `uid_local` (`uid_local`),
  KEY `uid_foreign` (`uid_foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_news_domain_model_news_ttcontent_mm`
--

LOCK TABLES `tx_news_domain_model_news_ttcontent_mm` WRITE;
/*!40000 ALTER TABLE `tx_news_domain_model_news_ttcontent_mm` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_news_domain_model_news_ttcontent_mm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_news_domain_model_tag`
--

DROP TABLE IF EXISTS `tx_news_domain_model_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_news_domain_model_tag` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `pid` int NOT NULL DEFAULT '0',
  `tstamp` int NOT NULL DEFAULT '0',
  `crdate` int NOT NULL DEFAULT '0',
  `deleted` smallint NOT NULL DEFAULT '0',
  `hidden` smallint NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumtext COLLATE utf8mb4_unicode_ci,
  `l10n_source` int NOT NULL DEFAULT '0',
  `notes` text COLLATE utf8mb4_unicode_ci,
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `cruser_id` int NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `title` tinytext COLLATE utf8mb4_unicode_ci,
  `slug` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_headline` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `seo_text` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_news_domain_model_tag`
--

LOCK TABLES `tx_news_domain_model_tag` WRITE;
/*!40000 ALTER TABLE `tx_news_domain_model_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_news_domain_model_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_scheduler_task`
--

DROP TABLE IF EXISTS `tx_scheduler_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_scheduler_task` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `disable` smallint unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `nextexecution` int unsigned NOT NULL DEFAULT '0',
  `lastexecution_time` int unsigned NOT NULL DEFAULT '0',
  `lastexecution_failure` text COLLATE utf8mb4_unicode_ci,
  `lastexecution_context` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `serialized_task_object` mediumblob,
  `serialized_executions` mediumblob,
  `task_group` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `index_nextexecution` (`nextexecution`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_scheduler_task`
--

LOCK TABLES `tx_scheduler_task` WRITE;
/*!40000 ALTER TABLE `tx_scheduler_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_scheduler_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_scheduler_task_group`
--

DROP TABLE IF EXISTS `tx_scheduler_task_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_scheduler_task_group` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `groupName` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_scheduler_task_group`
--

LOCK TABLES `tx_scheduler_task_group` WRITE;
/*!40000 ALTER TABLE `tx_scheduler_task_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_scheduler_task_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_solr_eventqueue_item`
--

DROP TABLE IF EXISTS `tx_solr_eventqueue_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_solr_eventqueue_item` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `tstamp` int NOT NULL DEFAULT '0',
  `event` longblob,
  `error` smallint unsigned NOT NULL DEFAULT '0',
  `error_message` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`uid`),
  KEY `tstamp` (`tstamp`),
  KEY `error` (`error`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_solr_eventqueue_item`
--

LOCK TABLES `tx_solr_eventqueue_item` WRITE;
/*!40000 ALTER TABLE `tx_solr_eventqueue_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_solr_eventqueue_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_solr_indexqueue_indexing_property`
--

DROP TABLE IF EXISTS `tx_solr_indexqueue_indexing_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_solr_indexqueue_indexing_property` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `root` int NOT NULL DEFAULT '0',
  `item_id` int NOT NULL DEFAULT '0',
  `property_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `property_value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_solr_indexqueue_indexing_property`
--

LOCK TABLES `tx_solr_indexqueue_indexing_property` WRITE;
/*!40000 ALTER TABLE `tx_solr_indexqueue_indexing_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_solr_indexqueue_indexing_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_solr_indexqueue_item`
--

DROP TABLE IF EXISTS `tx_solr_indexqueue_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_solr_indexqueue_item` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `root` int NOT NULL DEFAULT '0',
  `item_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `item_uid` int NOT NULL DEFAULT '0',
  `indexing_configuration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `has_indexing_properties` smallint NOT NULL DEFAULT '0',
  `indexing_priority` int NOT NULL DEFAULT '0',
  `changed` int NOT NULL DEFAULT '0',
  `indexed` int NOT NULL DEFAULT '0',
  `errors` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pages_mountidentifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `changed` (`changed`),
  KEY `root` (`root`),
  KEY `indexing_priority_changed` (`indexing_priority`,`changed`),
  KEY `item_id` (`item_type`(191),`item_uid`),
  KEY `site_statistics` (`root`,`indexing_configuration`),
  KEY `pages_mountpoint` (`item_type`(191),`item_uid`,`has_indexing_properties`,`pages_mountidentifier`(191))
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_solr_indexqueue_item`
--

LOCK TABLES `tx_solr_indexqueue_item` WRITE;
/*!40000 ALTER TABLE `tx_solr_indexqueue_item` DISABLE KEYS */;
INSERT INTO `tx_solr_indexqueue_item` VALUES (16,1,'pages',1,'pages',0,0,1710608286,0,'','');
/*!40000 ALTER TABLE `tx_solr_indexqueue_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_solr_last_searches`
--

DROP TABLE IF EXISTS `tx_solr_last_searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_solr_last_searches` (
  `sequence_id` smallint unsigned NOT NULL DEFAULT '0',
  `tstamp` int NOT NULL DEFAULT '0',
  `keywords` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`sequence_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_solr_last_searches`
--

LOCK TABLES `tx_solr_last_searches` WRITE;
/*!40000 ALTER TABLE `tx_solr_last_searches` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_solr_last_searches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_solr_statistics`
--

DROP TABLE IF EXISTS `tx_solr_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_solr_statistics` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `pid` int NOT NULL DEFAULT '0',
  `root_pid` int NOT NULL DEFAULT '0',
  `tstamp` int NOT NULL DEFAULT '0',
  `language` int NOT NULL DEFAULT '0',
  `num_found` int NOT NULL DEFAULT '0',
  `suggestions_shown` int NOT NULL DEFAULT '0',
  `time_total` int NOT NULL DEFAULT '0',
  `time_preparation` int NOT NULL DEFAULT '0',
  `time_processing` int NOT NULL DEFAULT '0',
  `feuser_id` int unsigned NOT NULL DEFAULT '0',
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `keywords` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `page` int unsigned NOT NULL DEFAULT '0',
  `filters` blob,
  `sorting` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `parameters` blob,
  PRIMARY KEY (`uid`),
  KEY `rootpid_keywords` (`root_pid`,`keywords`),
  KEY `rootpid_tstamp` (`root_pid`,`tstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_solr_statistics`
--

LOCK TABLES `tx_solr_statistics` WRITE;
/*!40000 ALTER TABLE `tx_solr_statistics` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_solr_statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_staticfilecache_queue`
--

DROP TABLE IF EXISTS `tx_staticfilecache_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_staticfilecache_queue` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `cache_url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cache_priority` int NOT NULL DEFAULT '0',
  `page_uid` int NOT NULL DEFAULT '0',
  `invalid_date` int NOT NULL DEFAULT '0',
  `call_date` int NOT NULL DEFAULT '0',
  `call_result` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `call_date` (`call_date`,`cache_url`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_staticfilecache_queue`
--

LOCK TABLES `tx_staticfilecache_queue` WRITE;
/*!40000 ALTER TABLE `tx_staticfilecache_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_staticfilecache_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_twohkickstarter_domain_model_contact`
--

DROP TABLE IF EXISTS `tx_twohkickstarter_domain_model_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_twohkickstarter_domain_model_contact` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sub_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `image` int NOT NULL DEFAULT '0',
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `company_name_appendix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `company_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `company_address_appendix` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `website_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_twohkickstarter_domain_model_contact`
--

LOCK TABLES `tx_twohkickstarter_domain_model_contact` WRITE;
/*!40000 ALTER TABLE `tx_twohkickstarter_domain_model_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_twohkickstarter_domain_model_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_twohkickstarter_domain_model_social`
--

DROP TABLE IF EXISTS `tx_twohkickstarter_domain_model_social`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_twohkickstarter_domain_model_social` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `pid` int NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `t3ver_oid` int NOT NULL DEFAULT '0',
  `t3ver_wsid` int NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cruser_id` int unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `t3ver_count` int NOT NULL DEFAULT '0',
  `t3ver_tstamp` int NOT NULL DEFAULT '0',
  `t3ver_move_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `language` (`l10n_parent`,`sys_language_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_twohkickstarter_domain_model_social`
--

LOCK TABLES `tx_twohkickstarter_domain_model_social` WRITE;
/*!40000 ALTER TABLE `tx_twohkickstarter_domain_model_social` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_twohkickstarter_domain_model_social` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_twohtinypng_domain_model_tiny`
--

DROP TABLE IF EXISTS `tx_twohtinypng_domain_model_tiny`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_twohtinypng_domain_model_tiny` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `pid` int NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `starttime` int unsigned NOT NULL DEFAULT '0',
  `endtime` int unsigned NOT NULL DEFAULT '0',
  `t3ver_oid` int NOT NULL DEFAULT '0',
  `t3ver_wsid` int NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `dimension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `width` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `height` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cruser_id` int unsigned NOT NULL DEFAULT '0',
  `t3ver_id` int NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `t3ver_count` int NOT NULL DEFAULT '0',
  `t3ver_tstamp` int NOT NULL DEFAULT '0',
  `t3ver_move_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `language` (`l10n_parent`,`sys_language_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_twohtinypng_domain_model_tiny`
--

LOCK TABLES `tx_twohtinypng_domain_model_tiny` WRITE;
/*!40000 ALTER TABLE `tx_twohtinypng_domain_model_tiny` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_twohtinypng_domain_model_tiny` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_yoastseo_prominent_word`
--

DROP TABLE IF EXISTS `tx_yoastseo_prominent_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_yoastseo_prominent_word` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `site` int NOT NULL DEFAULT '0',
  `stem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `weight` int NOT NULL DEFAULT '0',
  `uid_foreign` int NOT NULL DEFAULT '0',
  `tablenames` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_yoastseo_prominent_word`
--

LOCK TABLES `tx_yoastseo_prominent_word` WRITE;
/*!40000 ALTER TABLE `tx_yoastseo_prominent_word` DISABLE KEYS */;
INSERT INTO `tx_yoastseo_prominent_word` VALUES (1,1,0,1,'kickstarter',6,1,'pages'),(2,1,0,1,'twoh',5,1,'pages'),(3,1,0,1,'home',3,1,'pages'),(4,1,0,1,'project',3,1,'pages'),(5,1,0,1,'typo3',3,1,'pages'),(6,1,0,1,'de',2,1,'pages'),(7,1,0,1,'development',2,1,'pages'),(8,1,0,1,'en',2,1,'pages'),(9,1,0,1,'standorte',2,1,'pages');
/*!40000 ALTER TABLE `tx_yoastseo_prominent_word` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tx_yoastseo_related_focuskeyword`
--

DROP TABLE IF EXISTS `tx_yoastseo_related_focuskeyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tx_yoastseo_related_focuskeyword` (
  `uid` int unsigned NOT NULL AUTO_INCREMENT,
  `pid` int unsigned NOT NULL DEFAULT '0',
  `tstamp` int unsigned NOT NULL DEFAULT '0',
  `crdate` int unsigned NOT NULL DEFAULT '0',
  `deleted` smallint unsigned NOT NULL DEFAULT '0',
  `hidden` smallint unsigned NOT NULL DEFAULT '0',
  `sorting` int NOT NULL DEFAULT '0',
  `sys_language_uid` int NOT NULL DEFAULT '0',
  `l10n_parent` int unsigned NOT NULL DEFAULT '0',
  `l10n_source` int unsigned NOT NULL DEFAULT '0',
  `l10n_state` text COLLATE utf8mb4_unicode_ci,
  `t3_origuid` int unsigned NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `t3ver_oid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_wsid` int unsigned NOT NULL DEFAULT '0',
  `t3ver_state` smallint NOT NULL DEFAULT '0',
  `t3ver_stage` int NOT NULL DEFAULT '0',
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `synonyms` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `uid_foreign` int NOT NULL DEFAULT '0',
  `tablenames` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`,`deleted`,`hidden`),
  KEY `translation_source` (`l10n_source`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tx_yoastseo_related_focuskeyword`
--

LOCK TABLES `tx_yoastseo_related_focuskeyword` WRITE;
/*!40000 ALTER TABLE `tx_yoastseo_related_focuskeyword` DISABLE KEYS */;
/*!40000 ALTER TABLE `tx_yoastseo_related_focuskeyword` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-16 17:01:02
