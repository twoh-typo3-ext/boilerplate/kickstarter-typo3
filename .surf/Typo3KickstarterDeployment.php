<?php

/** @var \TYPO3\Surf\Domain\Model\Deployment $deployment */
$node = new \TYPO3\Surf\Domain\Model\Node(getenv('NODE'));
$node
    ->setHostname($node->getName())
    ->setOption('username', getenv('USERNAME'))
    ->setOption('password', getenv('PASSWORD'))
    ->setOption('phpBinaryPathAndFilename', '/usr/local/bin/php_cli');

$application = new \TYPO3\Surf\Application\TYPO3\CMS();
$application
    ->setContext(getenv('TYPO3_CONTEXT'))
    ->setDeploymentPath(getenv('DEPLOYMENT_PATH'))
    ->setOption('baseUrl', getenv('BASE_URL'))
    ->setOption('webDirectory', 'public')
    ->setOption('symlinkDataFolders', ['fileadmin', 'uploads', '_assets'])
    ->setOption('repositoryUrl', 'git@gitlab.com:twoh-typo3-ext/boilerplate/kickstarter-typo3.git')
    ->setOption('branch', getenv('BRANCH'))
    ->setOption('keepReleases', 3)
    ->setOption('composerCommandPath', 'composer')
    ->setOption('rsyncExcludes', [
        '.git*',
        '.surf',
        'public/fileadmin',
        'public/uploads',
    ])
    ->addNode($node);

$deployment
    ->addApplication($application)
    ->onInitialize(
        function () use ($deployment, $application) {
            $deployment->getWorkflow()
                ->defineTask('CopyEnvFileTask', \TYPO3\Surf\Task\ShellTask::class, [
                    'command' => [
                        'cp {releasePath}/.env~dist {releasePath}/.env'
                    ]
                ])
                ->defineTask('ComposerInstall', \TYPO3\Surf\Task\ShellTask::class, [
                    'command' => [
                        'cd {releasePath}',
                        'composer install'
                    ]
                ])
                ->defineTask('SetupTypo3', \TYPO3\Surf\Task\ShellTask::class, [
                    'command' => [
                        'cd {currentPath}',
                        'php vendor/bin/typo3 install:fixfolderstructure',
                        'php vendor/bin/typo3 database:updateschema',
                        'php vendor/bin/typo3 language:update',
                        'php vendor/bin/typo3 cache:flush'
                    ]
                ])
                ->afterStage('transfer', 'CopyEnvFileTask', $application)
                ->afterStage('transfer', 'ComposerInstall', $application)
                ->afterStage('finalize', 'SetupTypo3', $application)
                ->removeTask(\TYPO3\Surf\Task\TYPO3\CMS\CreatePackageStatesTask::class, $application)
                ->removeTask(\TYPO3\Surf\Task\TYPO3\CMS\CopyConfigurationTask::class, $application)
                ->removeTask(\TYPO3\Surf\Task\TYPO3\CMS\WebOpcacheResetCreateScriptTask::class, $application)
                ->removeTask(\TYPO3\Surf\Task\TYPO3\CMS\WebOpcacheResetExecuteTask::class, $application)
                ->removeTask(\TYPO3\Surf\Task\TYPO3\CMS\SetUpExtensionsTask::class, $application)
                ->removeTask(\TYPO3\Surf\Task\TYPO3\CMS\FlushCachesTask::class, $application);
        }
    );