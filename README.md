# Project: Kickstarter TYPO3

## Table of contents

- [Getting started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Setup](#setup)
- [Running tests](#running-tests)
  - [Unit tests](#unit-tests)
  - [PHPLint Tests](#phplint-tests)
  - [Coding style tests](#coding-style-tests)
- [Deployment](#deployment)
- [Workflow](#workflow)
- [Cronjob](#cronjob)
- [Deployment](#deployment)

## Getting started

This section should be a manual to get you a
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on different systems (e.g. test, stage and prod).

### Prerequisites

What things you need to install the software and how to install them.

- [Composer: 2](https://getcomposer.org/doc/00-intro.md)
- [Docker: 2.4.0](https://docs.docker.com/get-docker/)
- Docker Compose

### Setup

### Run

```shell
cp .env~dist .env
```

### copy .env file

- `TYPO3_CONTEXT` to your specified environment

```
Development
Testing
Production
```

- the following Variables also:

```
MYSQL_HOST
MYSQL_DATABASE
MYSQL_USER
MYSQL_PASSWORD
ICON_PATH='/fileadmin/Icons/'
```

### Execute the following steps to install TYPO3 and his dependencies

```shell
composer install
```

### Setup Docker

If you are on a unix based system and you have installed docker you can:

- add domains to your local /etc/hosts (like you need to for the project)

```
127.0.0.1 silbitz.de.local
```

### Start Docker

# Installation notes

Start the services:
`docker-compose up -d`

## Running tests

### PHPLint Tests

```
composer phplint-docker
```

### Unit tests

```
composer phpunit-docker
```

### Coding style tests

#### PHP Code Sniffer [PSR-12]

After Setup above, enable Code Sniffer and select coding standard.
[Take a look](https://medium.com/@sameernyaupane/how-to-setup-php-code-sniffer-in-phpstorm-d8ad7fc0cc08)

#### HTML Validation Tests

```
composer html-validator
```

or you can test only one file

```
html-validator PATH_TO_FILE
```

## Deployment

Add additional notes about how to deploy this on different systems (e.g. stage, live)

## Workflow

### Pipeline

- The branches Develop, Staging and Master each have a pipeline that tests the system and rolls it out on the server
- For develop purposes we also installed a gitlab-runner docker container. So you can test your .gitlab-ci.yaml is working without running it on a server.
  - `git config --global --add safe.directory '*'`
  - `docker exec -it -w /var/www twohdigital_gitlab-runner_1 gitlab-runner exec docker JOB_NAME`

### We work according to the following GIT workflow

- Branches for features and bugixes are always branched off from develop
- Please provide commit messages exclusively in the commitlint procedure
  - please run the tests listed here again locally before each commit and fix any problems that may have arisen
- After successfully pushing a branch, it is merged into the Develop system
- Once the change has been approved on Dev, the develop branch is merged into the staging branch and the customer can test
- After successful customer approval, the staging branch can be merged into the master branch

## Cronjob

- Since we have integrated various **scheduler tasks**, a TYPO3 **cron job** must be **set up** (server side).
- [Create TYPO3 Cronjob](https://docs.typo3.org/c/typo3/cms-scheduler/master/en-us//Installation/SetupCheck/Index.html)

```shell
*/15 * * * * /usr/local/bin/php /var/www/vhost/dev.gpticketmaster.de/httpdocs/vendor/bin/typo3 scheduler:run
```

## Deployment
We deploy CI to staging and production via Surf and GitLab.

## Certs
Add ``certs/localhost.crt`` to your local keychain.
[Windows](https://woshub.com/updating-trusted-root-certificates-in-windows-10/)