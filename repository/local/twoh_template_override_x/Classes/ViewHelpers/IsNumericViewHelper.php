<?php

declare(strict_types=1);

namespace TWOH\TwohTemplateOverrideX\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Return available icons names:
 *
 * Import Namespace:
 * xmlns:twoh="http://typo3.org/ns/TWOH/TwohTemplateOverrideX/ViewHelpers"
 * or
 * {namespace twoh="http://typo3.org/ns/TWOH/TwohTemplateOverrideX/ViewHelpers"}
 *
 * <twoh:isNumeric label={whatever} />
 */
class IsNumericViewHelper extends AbstractViewHelper
{
    public function initializeArguments(): void
    {
        $this->registerArgument('label', 'string', 'Label you want to test', true);
    }

    /**
     * @return bool
     */
    public function render()
    {
        if ($this->arguments['label']){
            if (is_numeric($this->arguments['label'])) {
                return true;
            }
        }
        return false;
    }
}