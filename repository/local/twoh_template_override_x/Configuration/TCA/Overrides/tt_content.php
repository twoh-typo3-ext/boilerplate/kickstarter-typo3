<?php

use B13\Container\Tca\ContainerConfiguration;
use B13\Container\Tca\Registry;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

defined('TYPO3') || die();

GeneralUtility::makeInstance(Registry::class)->configureContainer(
    (
    new ContainerConfiguration(
        'oneelevencol', // CType
        '1 zu 11 Spalten Element', // label
        '1 zu 11 Spalten Element', // description
        [
            [
                ['name' => 'Linker Bereich', 'colPos' => 101],
                ['name' => 'Rechter Bereich', 'colPos' => 102]
            ]
        ] // grid configuration
    )
    )
        // set an optional icon configuration
        ->setIcon('EXT:twoh_kickstarter/Resources/Public/Icons/Extension.svg')
);

GeneralUtility::makeInstance(Registry::class)->configureContainer(
    (
    new ContainerConfiguration(
        'twotencol', // CType
        '2 zu 10 Spalten Element', // label
        '2 zu 10 Spalten Element', // description
        [
            [
                ['name' => 'Linker Bereich', 'colPos' => 101],
                ['name' => 'Rechter Bereich', 'colPos' => 102]
            ]
        ] // grid configuration
    )
    )
        // set an optional icon configuration
        ->setIcon('EXT:twoh_kickstarter/Resources/Public/Icons/Extension.svg')
);

GeneralUtility::makeInstance(Registry::class)->configureContainer(
    (
    new ContainerConfiguration(
        'oneeightthreecol', // CType
        '1 / 8 / 3 Spalten Element', // label
        '1 / 8 / 3 Spalten Element', // description
        [
            [
                ['name' => 'Linker Bereich', 'colPos' => 101],
                ['name' => 'Mittlerer Bereich', 'colPos' => 102],
                ['name' => 'Rechter Bereich', 'colPos' => 103]
            ]
        ] // grid configuration
    )
    )
        // set an optional icon configuration
        ->setIcon('EXT:twoh_kickstarter/Resources/Public/Icons/Extension.svg')
);

GeneralUtility::makeInstance(Registry::class)->configureContainer(
    (
    new ContainerConfiguration(
        'topiccontainer', // CType
        'Iconcard Element', // label
        'Iconcard Element', // description
        [
            [
                [
                    'name' => 'Iconcard Element Bereich',
                    'colPos' => 101,
                    'allowed' => [
                        'CType' => 'twoh_iconcardelement, shortcut'
                    ]
                ]
            ]
        ] // grid configuration
    )
    )
        // set an optional icon configuration
        ->setIcon('EXT:twoh_kickstarter/Resources/Public/Icons/Extension.svg')
);

ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    'pi_flexform;LLL:EXT:twoh_kickstarter/Resources/Private/Language/locallang.xlf:gridelement.container.settings.sheetGeneral',
    'oneelevencol',
    'after:header'
);

ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:twoh_kickstarter/Configuration/FlexForms/Gridelements/Col.xml',
    'oneelevencol'
);

ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    'pi_flexform;LLL:EXT:twoh_kickstarter/Resources/Private/Language/locallang.xlf:gridelement.container.settings.sheetGeneral',
    'twotencol',
    'after:header'
);

ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:twoh_kickstarter/Configuration/FlexForms/Gridelements/Col.xml',
    'twotencol'
);

ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    'pi_flexform;LLL:EXT:twoh_kickstarter/Resources/Private/Language/locallang.xlf:gridelement.container.settings.sheetGeneral',
    'oneeightthreecol',
    'after:header'
);

ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:twoh_kickstarter/Configuration/FlexForms/Gridelements/Col.xml',
    'oneeightthreecol'
);

ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:twoh_template_override_x/Configuration/FlexForms/Gridelements/Col.xml',
    'onecol'
);

ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    'pi_flexform;LLL:EXT:twoh_kickstarter/Resources/Private/Language/locallang.xlf:gridelement.container.settings.sheetGeneral',
    'topiccontainer',
    'after:header'
);

ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:twoh_kickstarter/Configuration/FlexForms/Gridelements/Col.xml',
    'topiccontainer'
);

ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:twoh_template_override_x/Configuration/FlexForms/Gridelements/Col.xml',
    'twocol'
);