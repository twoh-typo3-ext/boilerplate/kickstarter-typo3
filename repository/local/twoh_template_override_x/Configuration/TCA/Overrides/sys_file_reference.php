<?php

$GLOBALS['TCA']['tt_content']['types']['twoh_sliderimageelement']['columnsOverrides']['media']['config']['overrideChildTca']['columns']['crop']['config'] = [
    'cropVariants' => [
        'desktop' => [
            'disabled' => false,
        ],
        'tablet' => [
            'disabled' => true,
        ],
        'mobile' => [
            'disabled' => true,
        ],
        'slider-home' => [
            'disabled' => true,
        ],
        'slider-subpage' => [
            'disabled' => true,
        ],
    ],
];
