<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3') or die();

ExtensionManagementUtility::addStaticFile(
    'twoh_template_override_x',
    'Configuration/TypoScript',
    '[TypoScript] TWOH Template Override X'
);
