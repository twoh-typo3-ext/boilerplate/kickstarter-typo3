# TWOH Template Override X

This extension overrides templates from other extensions.

## Minimum requirements

* **PHP** 8
* **composer** ^2
* **TYPO3** 12

## Setup

##### Install

* install Extension via Composer or FTP
* include Extension in TypoScript **ROOT Template**